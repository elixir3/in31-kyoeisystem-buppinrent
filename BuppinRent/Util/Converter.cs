﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuppinRent.Util
{
    class Converter
    {

        public static string GetMulitiLineText(Object o)
        {
            StringBuilder result = new StringBuilder();
            bool hasDone = false;
            System.IO.StringReader reader;
            TextBox tb = o as TextBox;
            RichTextBox rtb = o as RichTextBox;
            if (o is TextBox)
            {
                reader = new System.IO.StringReader(tb.Text);
            }
            else if(o is RichTextBox)
            {
                reader = new System.IO.StringReader(rtb.Text);
            }
            else
            {
                return "";
            }

            while (reader.Peek() > -1)
            {

                if (hasDone)
                {
                    result.Append(" \\n");
                }
                else
                {
                    hasDone = true;
                }

                result.Append(reader.ReadLine());
    
            }
            return result.ToString();
        }
    }
}
