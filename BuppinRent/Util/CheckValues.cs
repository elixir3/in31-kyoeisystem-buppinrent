﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuppinRent.Util

/*
*  Created By HAL OSAKA Katsuya Akamatsu on 2017/10/4 WED
*  使いまわしメソッド　入力チェック編
*/

{
    class CheckValues
    {

        public static bool IsEmpty(string s)
        {
            if (s is null || s is "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsEmpty(DataSet dataSet, string table)
        {
            if (dataSet.Tables[table].Rows.Count is 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsEmpty(DataTable dataTable)
        {
            if (dataTable.Rows.Count is 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsNumber(KeyPressEventArgs e)
        {
            return Int32.TryParse(e.KeyChar.ToString(), out int i);
        }

        public static bool IsNumber(string s)
        {
            return Int32.TryParse(s, out int i);
        }

        public static bool IsM001Kind(string s)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(s, @"[12]"))
            {
                return true;
            }

            return false;
        }

        public static bool IsM001Kind(KeyPressEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(e.KeyChar.ToString(), @"[12]"))
            {
                return true;
            }

            return false;
        }

        public static bool IsM001Group(string s)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(s, @"[0123456]"))
            {
                return true;
            }

            return false;
        }

        public static bool IsM001Group(KeyPressEventArgs e)
        {
            var pattern = System.Text.RegularExpressions.Regex.IsMatch(e.KeyChar.ToString(), @"[0123456]");

            if (pattern)
            {
                return true;
            }

            return false;
        }

        public static bool IsM001Flag(string s)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(s, @"[09]"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsM001Flag(KeyPressEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(e.KeyChar.ToString(), @"[09]"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static DateTime? IsDateTime(string y, string m, string d)
        {
            DateTime date;
            if (y.Length is 3)
            {
                y = "200" + y.Remove(0, 2);
            }

            if (m.Length is 1)
            {
                m = "0"+ m;
            }

            if (d.Length is 1)
            {
                d = "0" + d;
            }

            if (DateTime.TryParse(y + "/" + m + "/" + d, out date))
            {
                return date;
            }
            else
            {
                return null;
            }
        }
    }
}
