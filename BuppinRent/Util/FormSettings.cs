﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuppinRent.Util
{
    /*
*  Created By HAL OSAKA Katsuya Akamatsu on 2017/10/4 WED
*  使いまわしメソッド　フォーム設定編
*/

    class FormSettings
    {

        /*
         * trueでファンクションキーをアクティブにする
         */
        public static void EnabledFunction(Button button, bool mode)
        {
            button.Enabled = mode;
            button.TabStop = mode;

            if (mode)
            {
                button.BackColor = SystemColors.Control;
            }
            else
            {
                button.BackColor = SystemColors.ControlDarkDark;
            }

        }

        /*
         * trueでファンクションキーをアクティブにする、ファンクションキーのテキストも変更できる
         */
        public static void EnabledFunction(Button button, bool mode, string name)
        {
            button.Enabled = mode;
            button.TabStop = mode;
            button.Text = name + button.Text;

            if (mode)
            {
                button.BackColor = SystemColors.Control;
            }
            else
            {
                button.BackColor = SystemColors.ControlDarkDark;
            }
        }

        /*
         * falseですべてのオブジェクトに対してTab遷移を禁止する、trueで元に戻す
         */
        public static void TabStopTrigger(Control control, bool mode)
        {
            foreach (Control target in control.Controls)
            {
                if (target.HasChildren)
                {
                    TabStopTrigger(target, mode);
                }

                if (target is TextBox || target is Button)
                {
                    target.TabStop = mode;
                }
            }
        }

        /*
         * フォームを初期状態に戻す
         */
        public static void ModeCancel (Control control, Color defaultColor)
        {
            foreach (Control target in control.Controls)
            {
                if (target.HasChildren)
                {
                    ModeCancel(target, defaultColor);
                }

                if (target is TextBox)
                {
                    target.TabStop = true;
                    target.BackColor = defaultColor;
                    target.Text = "";
                }
            }
        }

        /*
         * DataGridViewのデザインを変更し２つのみヘッダセルの結合を行う
         * @param
         * int start 結合するヘッダセルの列のインデックス、結合されるヘッダセルは消滅する
         */
        public static void PaintHeaderCells (object sender, DataGridViewCellPaintingEventArgs e, DataGridView dgv, int start, string[] headerText)
        {
            //ヘッダの以外は変更しない
            if (e.RowIndex > -1)
            {
                return; //終了
            }

            Rectangle rectangle; //セルのサイズを保存するオブジェクト
            SolidBrush brush; //rectangleで指定した範囲を任意の色で塗りつぶすためのオブジェクト
            Pen pen; //rectangleで指定した範囲を線で囲むためのオブジェクト
            string text = "";

            //ヘッダ外及び、結合されるセル以外のデザインを変更する
            if (e.ColumnIndex != start+1 && e.ColumnIndex < headerText.Length)
            {
                //サイズ計測
                rectangle = e.CellBounds;

                //結合するセルと結合されるセルの横幅を足しておく
                if(e.ColumnIndex == start) {
                    rectangle.Width += dgv.Columns[start + 1].Width;
                }

                rectangle.X += -1; //調整
                rectangle.Y += -2; //調整

                brush = new SolidBrush(SystemColors.Control);//塗りつぶす色の設定
                pen = new Pen(dgv.GridColor); //線の色の設定

                e.Graphics.FillRectangle(brush, rectangle);　//色塗り
                e.Graphics.DrawRectangle(pen, rectangle); //線引き

                rectangle.Y += 2; //調整

                //ヘッダテキストを変数に格納、   Index -1 は空白である
                if (e.ColumnIndex > -1)
                {
                    text = headerText[e.ColumnIndex];
                }

                //ヘッダテキストを設定
                TextRenderer.DrawText(
                    e.Graphics, //描写してくれるオブジェクト
                    text, //ヘッダテキスト
                    e.CellStyle.Font, //書式
                    rectangle, //設定する範囲
                    e.CellStyle.ForeColor, //フォント色
                    TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter //縦横中央揃え
               );
            }
            else if(e.ColumnIndex >= headerText.Length)
            {
                e.Paint(e.ClipBounds, e.PaintParts); //ヘッダ外はそのまま
            }

            e.Handled = true; //このメソッドはイベントハンドラにより作動
        }

        //結合がない用
        public static void PaintHeaderCells(object sender, DataGridViewCellPaintingEventArgs e, DataGridView dgv, string[] headerText)
        {
            if (e.RowIndex > -1)
            {
                return;
            }

            Rectangle rectangle;
            SolidBrush brush;
            Pen pen;
            string text = "";

            if (e.ColumnIndex < headerText.Length)
            {
                // e.Paint(e.ClipBounds, e.PaintParts);

                rectangle = e.CellBounds;
                rectangle.X += -1;
                rectangle.Y += -2;
                brush = new SolidBrush(SystemColors.Control);
                pen = new Pen(dgv.GridColor);
                e.Graphics.FillRectangle(brush, rectangle);
                e.Graphics.DrawRectangle(pen, rectangle);

                if (e.ColumnIndex > -1)
                {
                    text = headerText[e.ColumnIndex];
                }

                rectangle.Y += 2;
                TextRenderer.DrawText(
                    e.Graphics,
                    text,
                    e.CellStyle.Font,
                    rectangle,
                    e.CellStyle.ForeColor,
                    TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter
                    );
            }
            else
            {
                e.Paint(e.ClipBounds, e.PaintParts);
            }

        }
        }
}
