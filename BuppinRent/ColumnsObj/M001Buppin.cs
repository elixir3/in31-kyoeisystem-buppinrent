﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuppinRent.ColumnsObj

/*
 *  Created By HAL OSAKA Katsuya Akamatsu on 2017/10/2 MON
 *  物品マスタテーブルの値を格納するオブジェクト
 */
{
    class M001Buppin
    {

        //m001 TABLE  Columns
        private int m001BupnCd = 0; //コード PK
        //private string m001BupnSbCd; //サブコード
        private string m001BupnNm = ""; //物品名称
        private int m001BupnKnd = 0; //物品種別
        private int m001BupnGrp = 0; //分類コード
        private string m001BupnCnd = ""; //状態・付属品
        private string m001BupnLcat1 = ""; //基本保管場所
        private string m001BupnLcat2 = ""; // 最終保管場所
        private int m001BupnRent = 0; //貸出フラグ
        private int m001BupnDel = 0; //削除フラグ
        private string m001BupnRrsn = ""; //削除理由
        private DateTime? m001InsDate; //登録日　登録時のみのスタンプ
        private DateTime? m001InsTime; //登録時間　登録時のみのスタンプ
        private DateTime? m001UpdDate; //更新日　更新都度スタンプ
        private DateTime? m001UpdTime; //更新時間　更新都度スタンプ

        //Geter Setter

        public int M001BupnCd
        {
            get
            {
                return m001BupnCd;
            }
            set
            {
                m001BupnCd = value;
            }
        }

        /*public string M001BupnSbCd
        {
            get
            {
                return m001BupnSbCd;
            }
            set
            {
                M001BupnSbCd = m001BupnSbCd;
            }
        }
        */

        public string M001BupnNm
        {
            get
            {
                return m001BupnNm;
            }
            set
            {
                m001BupnNm = value;
            }
        }

        public int M001BupnKnd
        {
            get
            {
                return m001BupnKnd;
            }
            set
            {
                m001BupnKnd = value;
            }
        }

        public int M001BupnGrp
        {
            get
            {
                return m001BupnGrp;
            }
            set
            {
                m001BupnGrp = value;
            }
        }

        public string M001BupnCnd
        {
            get
            {
                return m001BupnCnd;
            }
            set
            {
                m001BupnCnd = value;
            }
        }

        public string M001BupnLcat1
        {
            get
            {
                return m001BupnLcat1;
            }
            set
            {
                m001BupnLcat1 = value;
            }
        }

        public string M001BupnLcat2
        {
            get
            {
                return m001BupnLcat2;
            }
            set
            {
                m001BupnLcat2 = value;
            }
        }

        public int M001BupnRent
        {
            get
            {
                return m001BupnRent;
            }
            set
            {
                m001BupnRent = value;
            }
        }

        public int M001BupnDel
        {
            get
            {
                return m001BupnDel;
            }
            set
            {
                m001BupnDel = value;
            }
        }

        public string M001BupnRrsn
        {
            get
            {
                return m001BupnRrsn;
            }
            set
            {
                m001BupnRrsn = value;
            }
        }

        public DateTime? M001InsDate
        {
            get
            {
                return m001InsDate;
            }
            set
            {
                m001InsDate = value;
            }
        }

        public DateTime? M001InsTime
        {
            get
            {
                return m001InsTime;
            }
            set
            {
                m001InsTime = value;
            }

        }

        public DateTime? M001UpdDate
        {
            get
            {
                return m001UpdDate;
            }
            set
            {
                m001UpdDate = value;
            }

        }

        public DateTime? M001UpdTime
        {
            get
            {
                return m001UpdTime;
            }
            set
            {
                m001UpdTime = value;
            }

        }

    }

}
