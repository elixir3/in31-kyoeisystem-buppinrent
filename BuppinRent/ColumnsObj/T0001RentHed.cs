﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuppinRent.ColumnsObj

/*
*  Created By HAL OSAKA Katsuya Akamatsu on 2017/10/2 MON
*  貸出記録ヘッダテーブルの値を格納するオブジェクト
*/

{
    class T0001RentHed
    {
        private int t001RentNo; //貸出No 
        private int t001bupnKnd; //物品種別
        private DateTime t001RentDate; //貸出日
        private string t001RentCont; //借受者名
        private string t001RentUser; //利用者名
        private string t001RentCmnt; //備考
        private DateTime t001InsDate; //登録日　登録時のみのスタンプ
        private DateTime t001InsTime; //登録時間　登録時のみのスタンプ
        private DateTime t001UpdDate; //更新日　更新都度スタンプ
        private DateTime t001UpdTime; //更新時間　更新都度スタンプ

        private string t001RentNoStr;

        //getter setter

        public string T001RentNoStr
        {
            get
            {
                return t001RentNoStr;
            }
            set
            {
                t001RentNoStr = value;
            }
        }

        public int T001RentNo
        {
            get
            {
                return T001RentNo;
            }
            set
            {
                t001RentNo = value;
            }
        }

        public int T001bupnKnd
        {
            get
            {
                return t001bupnKnd;
            }
            set
            {
                t001bupnKnd = value;
            }
        }

        public DateTime T001RentDate
        {
            get
            {
                return t001RentDate;
            }
            set
            {
                t001RentDate = value;
            }
        }

        public string T001RentCont
        {
            get
            {
                return t001RentCont;
            }
            set
            {
                t001RentCont = value;
            }
        }

        public string T001RentUser
        {
            get
            {
                return t001RentUser;
            }
            set
            {
                t001RentUser = value;
            }
        }

        public string T001RentCmnt
        {
            get
            {
                return t001RentCmnt;
            }
            set
            {
                t001RentCmnt = value;
            }
        }

        public DateTime T001InsDate
        {
            get
            {
                return t001InsDate;
            }
            set
            {
                t001InsDate = value;
            }
        }

        public DateTime T001InsTime
        {
            get
            {
                return t001InsTime;
            }
            set
            {
                t001InsTime = value;
            }

        }

        public DateTime T001UpdDate
        {
            get
            {
                return t001UpdDate;
            }
            set
            {
                t001UpdDate = value;
            }

        }

        public DateTime T001UpdTime
        {
            get
            {
                return t001UpdTime;
            }
            set
            {
                t001UpdTime = value;
            }

        }
    }
}
