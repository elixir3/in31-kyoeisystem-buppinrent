﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuppinRent.ColumnsObj

/*
*  Created By HAL OSAKA Katsuya Akamatsu on 2017/10/2 MON
*  貸出記録明細テーブルの値を格納するオブジェクト
*/

{
    class T0002RentDtl
    {
        private int t002RentNo; //貸出No PK
        private int t002RentSeq; //sEQ Sequence PK
        private int t002BupnCd; //物品コード
        private string t002BupnCnd; //状態・付属品
        private DateTime t002RetrnPlan; //返却予定日
        private string t002RentLcat1; //保管場所・出
        private string t002RetrnDate; //返却日
        private string t002RetrnNm; //返却者
        private string t002RetrnCmnt; //返却備考
        private string t002RentLcat2; //保管場所・入
        private DateTime t002InsDate; //登録日　登録時のみのスタンプ
        private DateTime t002InsTime; //登録時間　登録時のみのスタンプ
        private DateTime t002UpdDate; //更新日　更新都度スタンプ
        private DateTime t002UpdTime; //更新時間　更新都度スタンプ 

        //getter setter

        public int T002RentNo
        {
            get
            {
                return T002RentNo;
            }
            set
            {
                t002RentNo = value;
            }
        }

        public int T002RentSeq
        {
            get
            {
                return t002RentSeq;
            }
            set
            {
                t002RentSeq = value;
            }
        }

        public int T002BupnCd
        {
            get
            {
                return t002BupnCd;
            }
            set
            {
                t002BupnCd = value;
            }
        }

        public string T002BupnCnd
        {
            get
            {
                return t002BupnCnd;
            }
            set
            {
                t002BupnCnd = value;
            }
        }

        public DateTime T002RetrnPlan
        {
            get
            {
                return t002RetrnPlan;
            }
            set
            {
                t002RetrnPlan = value;
            }
        }

        public string T002RentLcat1
        {
            get
            {
                return t002RentLcat1;
            }
            set
            {
                t002RentLcat1 = value;
            }
        }

        public string T002RetrnDate
        {
            get
            {
                return t002RetrnDate;
            }
            set
            {
                t002RetrnDate = value;
            }
        }

        public string T002RetrnNm
        {
            get
            {
                return t002RetrnNm;
            }
            set
            {
                t002RetrnNm = value;
            }
        }

        public string T002RetrnCmnt
        {
            get
            {
                return t002RetrnCmnt;
            }
            set
            {
                t002RetrnCmnt = value;
            }
        }

        public string T002RentLcat2
        {
            get
            {
                return t002RentLcat2;
            }
            set
            {
                t002RentLcat2 = value;
            }
        }

        public DateTime T002InsDate
        {
            get
            {
                return t002InsDate;
            }
            set
            {
                t002InsDate = value;
            }
        }

        public DateTime T002InsTime
        {
            get
            {
                return t002InsTime;
            }
            set
            {
                t002InsTime = value;
            }

        }

        public DateTime T002UpdDate
        {
            get
            {
                return t002UpdDate;
            }
            set
            {
                t002UpdDate = value;
            }

        }

        public DateTime T002UpdTime
        {
            get
            {
                return t002UpdTime;
            }
            set
            {
                t002UpdTime = value;
            }

        }
    }
}
