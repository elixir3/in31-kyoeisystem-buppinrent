﻿namespace BuppinRent
{
    partial class ByCategoryOnLoanInquiryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbBuppinGroup = new System.Windows.Forms.TextBox();
            this.oldMonth = new System.Windows.Forms.TextBox();
            this.oldYear = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.oldDay = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.newYear = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.newMonth = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.newDay = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(179, 186);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 28);
            this.label2.TabIndex = 22;
            this.label2.Text = "日付";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(179, 242);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 28);
            this.label3.TabIndex = 22;
            this.label3.Text = "分類";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.label5.Location = new System.Drawing.Point(266, 242);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(587, 28);
            this.label5.TabIndex = 22;
            this.label5.Text = "0:全て 1:本体 2:モニタ 3:プリンター 4:モデム 5:ルーター 6:ハブ";
            // 
            // tbBuppinGroup
            // 
            this.tbBuppinGroup.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbBuppinGroup.Location = new System.Drawing.Point(234, 239);
            this.tbBuppinGroup.MaxLength = 1;
            this.tbBuppinGroup.Name = "tbBuppinGroup";
            this.tbBuppinGroup.Size = new System.Drawing.Size(26, 36);
            this.tbBuppinGroup.TabIndex = 8;
            this.tbBuppinGroup.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // oldMonth
            // 
            this.oldMonth.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.oldMonth.Location = new System.Drawing.Point(316, 182);
            this.oldMonth.MaxLength = 2;
            this.oldMonth.Name = "oldMonth";
            this.oldMonth.Size = new System.Drawing.Size(37, 36);
            this.oldMonth.TabIndex = 3;
            this.oldMonth.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // oldYear
            // 
            this.oldYear.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.oldYear.Location = new System.Drawing.Point(262, 182);
            this.oldYear.MaxLength = 2;
            this.oldYear.Name = "oldYear";
            this.oldYear.Size = new System.Drawing.Size(37, 36);
            this.oldYear.TabIndex = 2;
            this.oldYear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(407, 186);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 31);
            this.label6.TabIndex = 22;
            this.label6.Text = "～";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(228, 186);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 28);
            this.label4.TabIndex = 100000;
            this.label4.Text = "20";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(297, 186);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 28);
            this.label7.TabIndex = 22;
            this.label7.Text = "/";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(350, 186);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(23, 28);
            this.label8.TabIndex = 22;
            this.label8.Text = "/";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // oldDay
            // 
            this.oldDay.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.oldDay.Location = new System.Drawing.Point(369, 182);
            this.oldDay.MaxLength = 2;
            this.oldDay.Name = "oldDay";
            this.oldDay.Size = new System.Drawing.Size(37, 36);
            this.oldDay.TabIndex = 4;
            this.oldDay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(432, 186);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 28);
            this.label10.TabIndex = 100000;
            this.label10.Text = "20";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // newYear
            // 
            this.newYear.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.newYear.Location = new System.Drawing.Point(467, 182);
            this.newYear.MaxLength = 2;
            this.newYear.Name = "newYear";
            this.newYear.Size = new System.Drawing.Size(37, 36);
            this.newYear.TabIndex = 5;
            this.newYear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(504, 186);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(23, 28);
            this.label11.TabIndex = 22;
            this.label11.Text = "/";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // newMonth
            // 
            this.newMonth.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.newMonth.Location = new System.Drawing.Point(523, 182);
            this.newMonth.MaxLength = 2;
            this.newMonth.Name = "newMonth";
            this.newMonth.Size = new System.Drawing.Size(37, 36);
            this.newMonth.TabIndex = 6;
            this.newMonth.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(557, 186);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(23, 28);
            this.label12.TabIndex = 22;
            this.label12.Text = "/";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // newDay
            // 
            this.newDay.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.newDay.Location = new System.Drawing.Point(577, 182);
            this.newDay.MaxLength = 2;
            this.newDay.Name = "newDay";
            this.newDay.Size = new System.Drawing.Size(37, 36);
            this.newDay.TabIndex = 7;
            this.newDay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // ByCategoryOnLoanInquiryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 1024);
            this.Controls.Add(this.newMonth);
            this.Controls.Add(this.newYear);
            this.Controls.Add(this.oldYear);
            this.Controls.Add(this.tbBuppinGroup);
            this.Controls.Add(this.newDay);
            this.Controls.Add(this.oldDay);
            this.Controls.Add(this.oldMonth);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Name = "ByCategoryOnLoanInquiryForm";
            this.Text = "ByCategoryOnLoanInquiryForm";
            this.Controls.SetChildIndex(this.title, 0);
            this.Controls.SetChildIndex(this.message, 0);
            this.Controls.SetChildIndex(this.f11, 0);
            this.Controls.SetChildIndex(this.f1, 0);
            this.Controls.SetChildIndex(this.f2, 0);
            this.Controls.SetChildIndex(this.f3, 0);
            this.Controls.SetChildIndex(this.f4, 0);
            this.Controls.SetChildIndex(this.f5, 0);
            this.Controls.SetChildIndex(this.f6, 0);
            this.Controls.SetChildIndex(this.f7, 0);
            this.Controls.SetChildIndex(this.f8, 0);
            this.Controls.SetChildIndex(this.f9, 0);
            this.Controls.SetChildIndex(this.f10, 0);
            this.Controls.SetChildIndex(this.f12, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.label12, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.label11, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.label10, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.oldMonth, 0);
            this.Controls.SetChildIndex(this.oldDay, 0);
            this.Controls.SetChildIndex(this.newDay, 0);
            this.Controls.SetChildIndex(this.tbBuppinGroup, 0);
            this.Controls.SetChildIndex(this.oldYear, 0);
            this.Controls.SetChildIndex(this.newYear, 0);
            this.Controls.SetChildIndex(this.newMonth, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbBuppinGroup;
        private System.Windows.Forms.TextBox oldMonth;
        private System.Windows.Forms.TextBox oldYear;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox oldDay;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox newYear;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox newMonth;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox newDay;
    }
}