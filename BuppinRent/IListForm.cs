﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BuppinRent.ColumnsObj;
using BuppinRent.Util;
using BuppinRent.Database;

namespace BuppinRent
{

    /**
     * 保管場所別分類別物品照会　2017/10/05
     * 荻野　翔大
     * 
     * Update By HAL OSAKA Katsuya Akamatsu on 2017/10/6 FRI
     * データグリッドビューのセルのデザイン、ヘッダセルの結合、またヘッダテキストを設定するときの仕様変更
     * 印刷ジョブの追加
     * 
     * */
    public partial class IListForm :IFormDesign
    {
        protected string defaultTitle = "";
        protected string defaultMessageHeader = "[メッセージ]  ";
        protected string defaultMessage = "保管場所別の一覧を確認できます";
        protected string[] headerText; // Add By HAL OSAKA Katsuya Akamatsu on 2017/10/6 FRI
        protected string[] keywords;

        protected DataGridView list;
        protected TextBox tbKind;

        protected DataSet dataSet;

        public IListForm()
        {
            InitializeComponent();
         }

        protected virtual void OnCreate()
        {
            dataSet = null;
            keywords = new string[4];
            list = articleInquiryList;
            tbKind = articleType;
            title.Text = defaultTitle;
            message.Text = defaultMessageHeader + defaultMessage;

            FormSettings.EnabledFunction(f3, true, "前の行");
            FormSettings.EnabledFunction(f4, true, "次の行");
            FormSettings.EnabledFunction(f8, true, "表示");
            FormSettings.EnabledFunction(f9, true, "印刷");
            FormSettings.EnabledFunction(f10, true, "クリア");
        }

        protected override void FoundKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F3:
                    MoveSelectRow(Keys.F3);
                    return;
                case Keys.F4:
                    MoveSelectRow(Keys.F4);
                    return;
                case Keys.F8:
                    Display_Click(null, null);
                    break;
                case Keys.F9:
                    BtnF9_Click(null, null);
                    break;
                case Keys.F10:
                    BtnF10_Click(null, null);
                    break;
            }

            base.FoundKeyDown(sender, e);

        }

        protected override void BtnF8_Click(object sender, EventArgs e)
        {
            Display_Click(null, null);
        }

        protected override void BtnF9_Click(object sender, EventArgs e)
        {
            ShowPrintPreview();
        }

        protected override void BtnF10_Click(object sender, EventArgs e)
        {
            FormSettings.ModeCancel(this, DefaultColor);
            message.Text = defaultMessageHeader + defaultMessage;
            list.DataSource = null;
        }

        protected virtual void FoundKeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox box = (TextBox)sender;

            if (e.KeyChar != (Char) Keys.Back && e.KeyChar != (Char) Keys.Enter && e.KeyChar != (Char) Keys.Escape)
            { 
               CheckTypeValues(box, e);
            }

        }

        protected virtual void CheckTypeValues(TextBox box, KeyPressEventArgs e)
        {

            switch (box.Name)
            {
               
                case "articleType":
                    if (CheckValues.IsM001Kind(e) || e.KeyChar.ToString() is "0")
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, "種別は0から２のみ入力可能です");
                    }
                    break;

            }

        }

        protected virtual void SetColumnProperty()
        {

        }

        protected virtual void SetCellValues(string table)
        {

        }

        protected virtual void SetDefaultTheme(TextBox box)
        {
            box.BackColor = DefaultColor;
            message.Text = defaultMessageHeader + defaultMessage;
            FormSettings.TabStopTrigger(this, true);
        }

        protected virtual void SetErrorTheme(TextBox box, KeyPressEventArgs e, String errorMessage)
        {
            e.Handled = true;
            box.BackColor = ErrorColor;
            message.Text = defaultMessageHeader + errorMessage;
            FormSettings.TabStopTrigger(this, false);
        }

        protected virtual void Display_Click(object sender, EventArgs e)
            {

            }

        protected virtual void PaintHeaderCells(object sender, DataGridViewCellPaintingEventArgs e)
        {
            
        }

        protected bool IsSearchRent(string s)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(s, @"[123]"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected bool IsSearchDel(string s)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(s, @"[12]"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //End by By HAL OSAKA Katsuya Akamatsu on 2017/10/6 FRI

        protected virtual void ShowPrintPreview()
        {
            if(dataSet is null)
            {
                MessageBox.Show(
                    "印刷するには先に一覧の表示を行ってください", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (list.Rows.Count == 0)
            {
                MessageBox.Show(
                    "印刷するには一覧に一件以上必要です", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            using (ModalStyle modal = new ModalStyle(this, dataSet, ModalStyle.Mode.ModePrint, keywords))
            {
                modal.Owner = this;
                modal.ShowDialog();
            }

        }

        private void MoveSelectRow(Keys key)
        {
            if (list.Rows.Count is 0)
            {
                return;
            }

            int rowIndex = 0;
            try
            {
                rowIndex = list.CurrentRow.Index;

            }
            catch
            {
                list.Rows[rowIndex].Selected = true;
                list.CurrentCell = list[0, rowIndex];
            }

            switch (key)
            {
                case Keys.F3:
                    if (rowIndex != 0)
                    {
                        list.Rows[rowIndex - 1].Selected = true;
                        list.Rows[rowIndex].Selected = false;
                        list.CurrentCell = list[0, rowIndex - 1];
                    }
                    return;
                case Keys.F4:
                    if (rowIndex + 1 != list.Rows.Count)
                    {
                        list.Rows[rowIndex].Selected = false;
                        list.Rows[rowIndex + 1].Selected = true;
                        list.CurrentCell = list[0, rowIndex + 1];
                    }
                    return;
            }
        }

        protected virtual void ArticleInquiryList_Sorted(object sender, EventArgs e)
        {

        }
    }
}
