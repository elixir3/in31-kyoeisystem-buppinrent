﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using MySql.Data;
using System.Data;
using MySql.Data.MySqlClient;

namespace BuppinRent.Database

/*
*  Created By HAL OSAKA Katsuya Akamatsu on 2017/10/2 MON
*  データベースに接続するためのクラス
*  
*  Update by By HAL OSAKA Katsuya Akamatsu on 2017/10/5 THU
*/

{
    class DatabaseConnector
    {

        private DataSet dataSet; //戻り値に使用するためのオブジェクト

        /*
         * app.configに記述されているデータベースに接続するための情報を取得する 
         */
        private string GetKey()
        {
            return ConfigurationManager.AppSettings["DbConKey"];
        }

        /*
         * データベースに接続しアダプターに値を格納する
         */
        private void Connect(string dataTable, string sql, string table)
        {
            dataSet = new DataSet(dataTable);
            using (MySqlConnection conn = new MySqlConnection(GetKey()))
            {
                conn.Open();
                using (MySqlDataAdapter adapter = new MySqlDataAdapter(sql, conn))
                {
                    adapter.Fill(dataSet, table);
                }
                    
                conn.Close();
            }

                
        }

        /*
         * public　アダプターを利用者に返す
         */
        public DataSet Result(string dataTable, string sql, string table)
        {
            dataSet = null; //初期化
            Connect(dataTable, sql, table);
            return dataSet;
        }

        // Start to Add By HAL OSAKA Katsuya Akamatsu on 2017/10/5 THU

        /*
         * Adapterが必要ない場合のConnectメソッド
         */
        private void Connect(string sql)
        {
            using (MySqlConnection conn = new MySqlConnection(GetKey()))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand(sql, conn))
                {
                    cmd.ExecuteNonQuery();
                }

                conn.Close();
            }
        }

        /*
         * SQLを実行するためのメソッド、成功するとtrueを返す
         */ 
        public void ExecuteNonQuery(string sql)
        {
            Connect(sql);
        }

        // End to Add By HAL OSAKA Katsuya Akamatsu on 2017/10/5 THU

    }
}
