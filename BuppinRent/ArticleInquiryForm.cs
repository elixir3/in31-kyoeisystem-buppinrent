﻿using BuppinRent.Database;
using BuppinRent.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuppinRent
{
    public partial class ArticleInquiryForm : IListForm
    {

        public ArticleInquiryForm()
        {
            InitializeComponent();
            OnCreate();
        }

        protected override void OnCreate()
        {
            defaultTitle = "物品照会";
            defaultMessageHeader = "[メッセージ]  ";
            defaultMessage = "保管場所別の一覧を確認できます";
            dataSet = null;
            base.OnCreate();
        }


        protected override void FoundKeyDown(object sender, KeyEventArgs e)
        {
            base.FoundKeyDown(sender, e);
        }


        protected override void FoundKeyPress(object sender, KeyPressEventArgs e)
        {
            base.FoundKeyPress(sender, e);
        }

        protected override void CheckTypeValues(TextBox box, KeyPressEventArgs e)
        {

            switch (box.Name)
            {
                case "tbLoan":
                    if (IsSearchRent(e.KeyChar.ToString()))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, "貸出は１から３のみ入力可能です");
                    }
                    break;
                case "tbDel":
                    if (IsSearchDel(e.KeyChar.ToString()))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, "削除は１もしくは２のみ入力可能です");
                    }
                    break;
                default:
                    base.CheckTypeValues(box, e);
                    break;

            }
        }

        protected void ValueCheck(TextBox box, KeyPressEventArgs e, string message, string mode)
        {
            switch (mode)
            {

                case "kind":

                    if (CheckValues.IsM001Kind(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;

                case "tbLoan":

                    if (IsSearchRent(e.KeyChar.ToString()))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;

                case "tbDel":

                    if (IsSearchDel(e.KeyChar.ToString()))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;
                default:
                    break;

            }
            e.Handled = true;
        }

        protected override void SetDefaultTheme(TextBox box)
        {
            box.BackColor = DefaultColor;
            message.Text = defaultMessageHeader + defaultMessage;
            FormSettings.TabStopTrigger(this, true);
        }

        protected override void SetErrorTheme(TextBox box, KeyPressEventArgs e, String errorMessage)
        {
            e.Handled = true;
            box.BackColor = ErrorColor;
            message.Text = defaultMessageHeader + errorMessage;
            FormSettings.TabStopTrigger(this, false);
        }




        protected override void Display_Click(object sender, EventArgs e)
        {
            var sql = new StringBuilder();
            sql.Append("SELECT M001_BUPNLCAT2, M001_BUPNLCAT1,T002_RETRNNM, cast(M001_BUPNGRP as CHAR) as M001_BUPNGRP , ");
            sql.Append("M001_BUPNCD,M001_BUPNNM, cast(REPLACE (REPLACE (M001_BUPNRENT,'0','未') ,'9','貸') as ChAR) as M001_BUPNRENT, ");
            sql.Append(" REPLACE(M001_BUPNCND,null,' ') as M001_BUPNCND,cast( REPLACE (REPLACE (M001_BUPNDEL,'0',''),'9','削除') as CHAR) as M001_BUPNDEL ");
            sql.Append(" from M001_BUPPIN ");
            sql.Append(" LEFT OUTER JOIN t002_rent_dtl ON T002_BUPNCD = M001_BUPNCD WHERE 1 ");

            string stAricleType = tbKind.Text;

            if (stAricleType == "1")
            {
                sql.Append(" AnD M001_BUPNKND = 1");
            }
            else if(stAricleType is "2")
            {
                sql.Append(" AnD M001_BUPNKND = 2");
            }
            else
            {
                tbKind.Text = "0";
            }

            string stloan = tbLoan.Text;
            if (stloan == "2")
            {
                sql.Append(" and M001_BUPNRENT = 9");
            }
            else if (stloan == "3")
            {
                sql.Append(" and M001_BUPNRENT = 0");
            }
            else
            {
                tbLoan.Text = "1";
            }

            string stDelete = tbDel.Text;
            if (stDelete == "1")
            {
                sql.Append(" and M001_BUPNDEL = 0");
            }
            else
            {
                tbDel.Text = "2";
            }



            sql.Append(" ORDER BY M001_BUPNLCAT2 ASC ;");
            string table = "M001_BUPPIN";

            DatabaseConnector conn = new DatabaseConnector();

            dataSet = conn.Result(table, sql.ToString(), table);

            keywords[0] = tbKind.Text;
            keywords[1] = tbLoan.Text;
            keywords[2] = tbDel.Text;

            if (dataSet.Tables[table].Rows.Count != 0)
            {
                SetCellValues(table);
               
            }

            list.DataSource = dataSet.Tables[table];

            if (list.Rows.Count != 0)
            {
                list.Rows[0].Selected = true;
                SetRowColor();
            }
                
            SetColumnProperty();

           

            //Start to Add By HAL OSAKA Katsuya Akamatsu on 2017/10/6 FRI

            headerText = new string[] { "現在保管場所","基本保管場所","最終返却者", "類", "物品名", "", "貸出", "状態(付属品)", "削除" };

            for (int count = 0; count < headerText.Length; count++)
            {
                list.Columns[count].HeaderText = headerText[count];
            }


        }

        /*
         * ヘッダーセルのスタイル変更
         */
        protected override void PaintHeaderCells(object sender, DataGridViewCellPaintingEventArgs e)
        {
            FormSettings.PaintHeaderCells(sender, e, list, 4, headerText);
        }

        //End By HAL OSAKA Katsuya Akamatsu on 2017/10/6 FRI


        //Start to Add By HAL OSAKA Katsuya Akamatsu on 2017/10/18 WED

        /*
        *列の幅調整
        */
        protected override void SetColumnProperty()
        {
            double width = list.Size.Width;
            int scrollBar = 17;
            if (list.Rows.Count > 25)
            {
                width = width - scrollBar;
            }

            int small = 50;
            int midle = 80;
            int big = 180;

            var center = DataGridViewContentAlignment.MiddleCenter; //中央揃え
            var right = DataGridViewContentAlignment.MiddleRight; //右揃え

            //RowHeder
            list.RowHeadersWidth = small;

            //Column0 As Location2
            list.Columns[0].Width = midle;
            list.Columns[0].HeaderCell.Style.Alignment = center;

            //Column1 As Location1
            list.Columns[1].Width = midle;
            list.Columns[1].HeaderCell.Style.Alignment = center;

            //Column2 As ReturnUser
            list.Columns[2].Width = midle;
            list.Columns[2].HeaderCell.Style.Alignment = center;

            //Column3 As Group
            list.Columns[3].Width = midle;
            list.Columns[3].HeaderCell.Style.Alignment = center;

            //Column4 As Code
            list.Columns[4].Width = small;
            list.Columns[4].HeaderCell.Style.Alignment = center;
            list.Columns[4].DefaultCellStyle.Alignment = right;

            //Column5 As Name
            list.Columns[5].Width = big;
            list.Columns[5].HeaderCell.Style.Alignment = center;

            //Column6 As Rent
            list.Columns[6].Width = small;
            list.Columns[6].HeaderCell.Style.Alignment = center;
            list.Columns[6].DefaultCellStyle.Alignment = center;

            //Column7 As Condition
            list.Columns[7].Width = (int)width - (small * 4 + midle * 4 + big * 1) - 2;
            list.Columns[7].HeaderCell.Style.Alignment = center;

            //Column6 As Delete
            list.Columns[8].Width = small;
            list.Columns[8].HeaderCell.Style.Alignment = center;
            list.Columns[8].DefaultCellStyle.Alignment = center;
        }

        /*
         * SQLだとREPLACEが複雑になるのでC#でレコードの内容を変更します
         */
        protected override void SetCellValues(string table)
        {
            string cell; //セルの内容を格納する

            //DataGridViewの内容を全件調べる
            for (int rowIndex = 0; rowIndex < dataSet.Tables[table].Rows.Count; rowIndex++)
            {
                //２列目、分類コードを可視性向上のため文字に置き換え開始

                cell = dataSet.Tables[table].Rows[rowIndex][3].ToString(); //二列目 rowindex行目の文字を格納

                switch (cell)
                {
                    case "1":
                        cell = "本体";
                        break;
                    case "2":
                        cell = "モニター";
                        break;
                    case "3":
                        cell = "プリンタ";
                        break;
                    case "4":
                        cell = "モデム";
                        break;
                    case "5":
                        cell = "ルータ";
                        break;
                    case "6":
                        cell = "ハブ";
                        break;
                   default:
                        cell = "書籍";
                        break;
                }

                dataSet.Tables[table].Rows[rowIndex][3] = cell; //変換した文字をセルに設定する、置き換え終了

            }
        }

        private void SetRowColor()
        {
            for (int rowIndex = 0; rowIndex < list.Rows.Count; rowIndex++)
            {
                if (list.Rows[rowIndex].Cells[8].Value.ToString().Equals("削除"))
                {
                    list.Rows[rowIndex].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                }
            }
        }

        protected override void ArticleInquiryList_Sorted(object sender, EventArgs e)
        {
            if (list.Rows.Count != 0)
            {
                SetRowColor();
            }
        }
    }
}
