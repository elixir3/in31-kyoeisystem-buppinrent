﻿using BuppinRent.ColumnsObj;
using BuppinRent.Database;
using BuppinRent.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuppinRent
{
    public partial class RentReceiveForm : IFormDesign
    {
        private string DefaultTitle = "物品返却";
        private string DefaultMessageHeader = "[メッセージ]  ";
        private string defaultMessage = "物品返却";

        private string rentNo = "0";
        private string[] headerText;
        private int index = -1;
        private List<T0001RentHed> list = null;
        private DatabaseConnector conn;
        private StringBuilder sql;
        private int Exist = 0;//存在するときに使う定数。
        private int NotExist = 1;//存在しない時に使う定数。
        private int NoSpecification = 2; //明細が存在しない時に使う定数。

        public RentReceiveForm()
        {
            InitializeComponent();
            //画面をフルスクリーンにする。
            OnCreate();
        }

        protected void OnCreate()
        {
            title.Text = DefaultTitle;
            message.Text = DefaultMessageHeader + defaultMessage;
            FormSettings.EnabledFunction(f1, true, "検索");
            FormSettings.EnabledFunction(f3, true, "前の行");
            FormSettings.EnabledFunction(f4, true, "次の行");
            FormSettings.EnabledFunction(f5, true, "前の\n貸出No");
            FormSettings.EnabledFunction(f6, true, "次の\n貸出No");
            FormSettings.EnabledFunction(f7, true, "返却");
            FormSettings.EnabledFunction(f8, true, "表示");
            FormSettings.EnabledFunction(f9, true, "印刷");
            FormSettings.EnabledFunction(f10, true, "クリア");
            FormSettings.EnabledFunction(f12, true, "削除");
            display.BackColor = SystemColors.Control;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    BtnF1_Click(null, null);
                    break;
                case Keys.F3:
                    MoveSelectRow(Keys.F3);
                    return;
                case Keys.F4:
                    MoveSelectRow(Keys.F4);
                    return;
                case Keys.F5:
                    NextData(false);
                    BtnF8_Click(null, null);
                    return;
                case Keys.F6:
                    NextData(true);
                    BtnF8_Click(null, null);
                    return;
                case Keys.F7:
                    BtnF7_Click(null, null);
                    return;
                case Keys.F8:
                    BtnF8_Click(null, null);
                    return;
                case Keys.F9:
                    BtnF9_Click(null, null);
                    return;
                case Keys.F10:
                    BtnF10_Click(null, null);
                    return;
                case Keys.F12:
                    ForcedReceive();
                    return;
                case Keys.Enter:
                    int currentRow = 0;
                    try
                    {
                        currentRow = dataGridView.CurrentRow.Index;
                    }
                    catch (Exception)
                    {
                        return;
                    }

                    DataGridViewCellEventArgs args = new DataGridViewCellEventArgs(0, currentRow);
                    dataGridView_CellDoubleClick(null, args);

                    return;

            }

            base.OnKeyDown(e);
        }
        protected override void BtnF1_Click(object sender, EventArgs e)
        {

            using (ModalStyle modal = new ModalStyle(this, null, ModalStyle.Mode.ModeSearch1))
            {
                modal.Owner = this;
                modal.ShowDialog();
            }

        }

        protected override void BtnF3_Click(object sender, EventArgs e)
        {
            MoveSelectRow(Keys.F3);
        }

        protected override void BtnF4_Click(object sender, EventArgs e)
        {
            MoveSelectRow(Keys.F4);
        }

        protected void BtnF7_Click(object sender, EventArgs e)
        {
            T0002RentDtl dtl = GetRentDtl();

            if (dtl is null)
            {
                MessageBox.Show(
                      "入力されていない箇所があります", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                message.Text = DefaultMessageHeader + "入力されていない箇所があります";
                return;
            }
            UpdateReturnData(dtl);
            SetDataGridView();

        }
        protected override void BtnF8_Click(object sender, EventArgs e)
        {
            if (CheckValues.IsEmpty(tbRentNo.Text))
            {
                MessageBox.Show(
                "貸出Noが入力されていません", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                message.Text = DefaultMessageHeader + "貸出Noが入力されていません";
                return;
            }

            switch (SetRentData())
            {
                case 0: //Exist
                    message.Text = DefaultMessageHeader + defaultMessage;
                    return;
                case 1: //NotExist
                    MessageBox.Show(
                    "その貸出Noは存在しません", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    message.Text = DefaultMessageHeader + "その貸出Noは存在しません";
                    return;
                case 2: //NoSpecification
                    tbBuppinCode.Text = string.Empty;
                    tbBuppinName.Text = string.Empty;
                    SetDataGridView();
                    message.Text = DefaultMessageHeader + "その貸出Noは全て返却済みです。";
                    return;


            }
        }

        protected override void BtnF10_Click(object sender, EventArgs e)
        {
            DialogResult result;
            Color glay = SystemColors.Control;
            if (CheckValues.IsEmpty(tbBuppinCode.Text) && !CheckValues.IsEmpty(tbRentNo.Text))
            {
                result = MessageBox.Show(
                      "フォームを初期化します、よろしいですか？", "メッセージ", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                if (result is DialogResult.OK)
                {
                    FormSettings.ModeCancel(this, SystemColors.ControlLightLight);
                    dataGridView.DataSource = null;
                    message.Text = DefaultMessageHeader + defaultMessage;
                    tbBuppinKind.BackColor = glay;
                    tbRentCont.BackColor = glay;
                    tbRentUser.BackColor = glay;
                    tbRentDate.BackColor = glay;
                    RentcMnt.BackColor = glay;
                    tbRentNo.ReadOnly = false;

                    return;
                }

            }

            lbRentSeq.Text = string.Empty;
            tbBuppinCode.Text = string.Empty;
            tbBuppinName.Text = string.Empty;
            tbYear.Text = string.Empty;
            tbMonth.Text = string.Empty;
            tbDay.Text = string.Empty;
            tbRetrnNm.Text = string.Empty;
            tbLcat2.Text = string.Empty;
            tbRentcMnt.Text = string.Empty;

            dataGridView.TabStop = false;

        }

        private void BtnLeft_Click(object sender, EventArgs e)
        {
            NextData(false);
            BtnF1_Click(null, null);
        }

        private void BtnRight_Click(object sender, EventArgs e)
        {
            NextData(true);
            BtnF1_Click(null, null);
        }

        private void SetDataGridView()
        {
            dataGridView.DataSource = GetDtlFromDB();

            dataGridView.Columns[7].SortMode = 0;
            //ヘッダーのカラムの幅
            dataGridView.Columns[0].Width = 39;//明細番号
            dataGridView.Columns[0].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView.Columns[1].Width = 89;//物品コード
            dataGridView.Columns[1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView.Columns[2].Width = 172;//物品名
            dataGridView.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView.Columns[3].Width = 180;//状態(付属品)
            dataGridView.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView.Columns[4].Width = 90;//返却予定日
            dataGridView.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView.Columns[5].Width = 100;//保管場所(出)
            dataGridView.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView.Columns[6].Width = 130;//返却者
            dataGridView.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView.Columns[7].Width = 90;//返却日
            dataGridView.Columns[7].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView.Columns[8].Width = 196;//返却備考
            dataGridView.Columns[8].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView.Columns[9].Width = 100;//保管場所(入)
            dataGridView.Columns[9].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            headerText = new string[] { "No", "物品コード", "物品名", "状態(付属品)", "返却予定日", "保管場所(出)", "返却者", "返却日", "返却備考", "保管場所(入)" };

            for (int count = 0; count < headerText.Length; count++)
            {
                dataGridView.Columns[count].HeaderText = headerText[count];
            }

            if (dataGridView.Rows.Count != 0)
            {
                dataGridView.Rows[0].Selected = true;
                SetRowColor();
            }
        }

        private void SetRowColor()
        {
            for (int rowIndex = 0; rowIndex < dataGridView.Rows.Count; rowIndex++)
            {
                if (!CheckValues.IsEmpty(dataGridView.Rows[rowIndex].Cells[7].Value.ToString()))
                {
                    dataGridView.Rows[rowIndex].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                }
            }
        }

        private int SetRentData()
        {
            sql = new StringBuilder();
            string hedTable = "t001_rent_hed";
            sql.Append("SELECT `T001_RENTNO`,`T001_BUPNKND`,`T001_RENTDATE`, `T001_RENTCONT`,`T001_RENTUSER`,REPLACE(T001_RENTCMNT, '\\n', '@ENTER') as `T001_RENTCMNT` ");
            sql.Append("FROM " + hedTable + " WHERE ");
            sql.Append("T001_RENTNO = " + tbRentNo.Text);

            conn = new DatabaseConnector();
            DataTable hedData = conn.Result(hedTable, sql.ToString(), hedTable).Tables[hedTable];
            if (CheckValues.IsEmpty(hedData))
            {
                return NotExist;
            }

            rentNo = hedData.Rows[0][0].ToString();
            tbBuppinKind.Text = hedData.Rows[0][1].ToString();
            string rentDate = hedData.Rows[0][2].ToString();
            tbRentCont.Text = hedData.Rows[0][3].ToString();
            tbRentUser.Text = hedData.Rows[0][4].ToString();
            RentcMnt.Text = hedData.Rows[0][5].ToString().Replace("@ENTER", Environment.NewLine);
            tbRentDate.Text = rentDate.Substring(0, rentDate.IndexOf(' '));
            tbRentNo.ReadOnly = true;

            SetDataGridView();

            sql.Clear();
            string t002 = "t002_rent_dtl";
            sql.Append("select M001_BUPNCD from " + t002 + " INNER JOIN m001_buppin ON ");
            sql.Append(" M001_BUPNCD = T002_BUPNCD where M001_BUPNCD = 9 AND T002_RENTNO = " + tbRentNo.Text);
            DataTable check = conn.Result(t002, sql.ToString(), t002).Tables[t002];

            if (check.Rows.Count == 0)
            {
                return NoSpecification;
            }

            SetRentDtl(dataGridView.Rows[0]);

            sql.Clear();

            return Exist;

        }

        private DataTable GetDtlFromDB()
        {
            sql = new StringBuilder();
            string dtlTable = "t002_rent_dtl";
            sql.Append("SELECT `T002_RENTSEQ`,`T002_BUPNCD`, M001_BUPNNM, `T002_BUPNCND`, `T002_RETRNPLAN`, `T002_RENTLCAT1`,`T002_RETRNNM`,REPLACE(T002_RETRNDATE, \"1987/10/05\", \"\"),`T002_RENTCMNT`,  `T002_RENTLCAT2` ");
            sql.Append(" FROM " + dtlTable + " INNER JOIN `m001_buppin` ON m001_buppin.m001_bupncd = T002_BUPNCD  ");
            sql.Append(" WHERE T002_RENTNO = " + rentNo);

            sql.Append(" ORDER BY T002_RENTSEQ ASC");

            conn = new DatabaseConnector();
            DataSet dtlData = conn.Result(dtlTable, sql.ToString(), dtlTable);
            sql.Clear();

            dataGridView.DataSource = dtlData.Tables[dtlTable];


            return dtlData.Tables[dtlTable];

        }

        private void SetRentDtl(DataGridViewRow row)
        {
            lbRentSeq.Text = row.Cells[0].Value.ToString();
            tbBuppinCode.Text = row.Cells[1].Value.ToString();
            tbBuppinName.Text = row.Cells[2].Value.ToString();

        }

        private T0002RentDtl GetRentDtl()
        {
            T0002RentDtl dtl = new T0002RentDtl();

            if (!CheckValues.IsNumber(tbRentNo.Text))
            {
                return null;
            }

            if (CheckValues.IsNumber(lbRentSeq.Text))
            {
                dtl.T002RentSeq = Convert.ToInt32(lbRentSeq.Text);
            }
            else
            {
                return null;
            }

            if (CheckValues.IsNumber(tbBuppinCode.Text))
            {
                dtl.T002BupnCd = Convert.ToInt32(tbBuppinCode.Text);
            }
            else
            {
                return null;
            }


            DateTime? check = CheckValues.IsDateTime("20" + tbYear.Text, tbMonth.Text, tbDay.Text);
            DateTime date;

            if (check != null)
            {
                date = (DateTime)check;
                dtl.T002RetrnDate = date.Date.ToString("yyyy/MM/dd");
            }
            else
            {
                return null;
            }

            if (!CheckValues.IsEmpty(tbRentUser.Text))
            {
                dtl.T002RetrnNm = tbRetrnNm.Text;
            }
            else
            {
                return null;
            }

            if (!CheckValues.IsEmpty(tbLcat2.Text))
            {
                dtl.T002RentLcat2 = tbLcat2.Text;
            }
            else
            {
                return null;
            }

            return dtl;
        }

        private bool UpdateReturnData(T0002RentDtl dtl)
        {



            if (Convert.ToDateTime(tbRentDate.Text) >= Convert.ToDateTime(dtl.T002RetrnDate))
            {
                MessageBox.Show(
                   "貸出日より前の返却日は登録できません。", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                message.Text = DefaultMessageHeader + "正しい日付の入力お願いします。";
                tbYear.BackColor = ErrorColor;
                tbMonth.BackColor = ErrorColor;
                tbDay.BackColor = ErrorColor;
                return false;

            }

            sql = new StringBuilder();
            string m001 = "m001_buppin";
            string code = tbBuppinCode.Text;
            if (!CheckValues.IsEmpty(dtl.T002BupnCd.ToString()))
            {
                code = dtl.T002BupnCd.ToString();
            }
            sql.Append("select M001_BUPNCD from m001_buppin where M001_BUPNCD = " + code);
            sql.Append(" and M001_BUPNRENT = 9");
            conn = new DatabaseConnector();
            DataTable dt = conn.Result(m001, sql.ToString(), m001).Tables[m001];

            if (dt.Rows.Count == 0)
            {
                MessageBox.Show(
                      "その貸出Noは返却済みです", "メッセージ", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

                return false;
            }

            sql.Clear();

            string dtlTable = "t002_rent_dtl";
            sql.Append("UPDATE " + dtlTable + " SET");
            sql.Append(" T002_RETRNDATE  = \"" + dtl.T002RetrnDate + "\"");
            sql.Append(" ,T002_RETRNNM = \"" + dtl.T002RetrnNm + "\"");
            string comment = "";
            if (!CheckValues.IsEmpty(tbRentcMnt.Text))
            {
                comment = Converter.GetMulitiLineText(tbRentcMnt);
            }
            else if(!CheckValues.IsEmpty(dtl.T002RetrnCmnt))
            {
                comment = dtl.T002RetrnCmnt;
            }

            string seq = lbRentSeq.Text;
            if (!CheckValues.IsEmpty(dtl.T002RentSeq.ToString()))
            {
                seq = dtl.T002RentSeq.ToString();
            }

            sql.Append(" ,T002_RENTCMNT = \"" + comment + "\"");
            sql.Append(" ,T002_RENTLCAT2 = \"" + dtl.T002RentLcat2 + "\"");
            sql.Append(" ,T002_UPDDATE = now(), T002_UPDTIME = now() ");
            sql.Append(" WHERE T002_RENTNO = " + tbRentNo.Text);
            sql.Append(" AnD T002_RENTSEQ = " + seq);

            conn.ExecuteNonQuery(sql.ToString());
            sql.Clear();

            UpdateReturnHedData();
            ReturnBuppin(dtl);

            return true;
        }

        private void UpdateReturnHedData()
        {
            sql = new StringBuilder();
            string hedTable = "t001_rent_hed";
            sql.Append("UPDATE " + hedTable + " SET");
            sql.Append(" T001_UPDDATE = now(),  	T001_UPDTIME = now()");

            conn = new DatabaseConnector();
            conn.ExecuteNonQuery(sql.ToString());
            sql.Clear();
        }

        private void ReturnBuppin(T0002RentDtl dtl)
        {
            sql = new StringBuilder();
            string hedTable = "m001_buppin";
            sql.Append("UPDATE " + hedTable + " SET");
            sql.Append(" M001_BUPNRENT = 0 , M001_BUPNLCAT2 = \"" + dtl.T002RentLcat2 + "\" WHERE M001_BUPNCD = " + dtl.T002BupnCd);

            conn = new DatabaseConnector();
            conn.ExecuteNonQuery(sql.ToString());
            sql.Clear();
        }

        private void dataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex is -1)
            {
                return;
            }


            DataGridViewRow row = dataGridView.Rows[e.RowIndex];

            if (!CheckValues.IsEmpty(row.Cells[7].Value.ToString()))
            {
                MessageBox.Show("すでに返却済みの物品です", "メッセージ", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
            }

            lbRentSeq.Text = row.Cells[0].Value.ToString();
            tbBuppinName.Text = row.Cells[2].Value.ToString();
            tbBuppinCode.Text = row.Cells[1].Value.ToString();
        }


        private void FoundKeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox box = (TextBox)sender;
            if (e.KeyChar != (Char)Keys.Back && e.KeyChar != (Char)Keys.Enter && e.KeyChar != (Char)Keys.Escape)
            {
                CheckTypeValues(box, e);
            }

        }

        private void CheckTypeValues(TextBox box, KeyPressEventArgs e)
        {

            switch (box.Name)
            {

                case "tbRentNo":
                    ValueCheck(box, e, "貸出Noは数値のみ入力可能です。", "rentNo");
                    break;
                case "tbBuppinCode":
                    ValueCheck(box, e, "物品コードは数値のみ入力可能です。", "code");
                    break;
                case "tbYear":
                    ValueCheck(box, e, "日付は数値で入力してください。", "year");
                    break;
                case "tbMonth":
                    ValueCheck(box, e, "日付は数値で入力してください。", "month");
                    break;
                case "tbDay":
                    ValueCheck(box, e, "日付は数値で入力してください。", "day");
                    break;

                default:

                    break;
            }
        }

        private void ValueCheck(TextBox box, KeyPressEventArgs e, string message, string mode)
        {
            switch (mode)
            {
                //貸出Noに文字が入力されたらエラーを表示
                case "rentNo":

                    if (CheckValues.IsNumber(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;
                //貸出Noに文字が入力されたらエラーを表示
                case "code":
                    if (CheckValues.IsNumber(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;
                //貸出Noに文字が入力されたらエラーを表示
                case "year":
                    if (CheckValues.IsNumber(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;
                //貸出Noに文字が入力されたらエラーを表示
                case "month":
                    if (CheckValues.IsNumber(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;
                //貸出Noに文字が入力されたらエラーを表示
                case "day":
                    if (CheckValues.IsNumber(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;



                default:
                    if (CheckValues.IsNumber(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;

            }
        }


        private void SetDefaultTheme(TextBox box)
        {
            box.BackColor = DefaultColor;
            message.Text = DefaultMessageHeader + defaultMessage;
            FormSettings.TabStopTrigger(this, true);
        }

        private void SetErrorTheme(TextBox box, KeyPressEventArgs e, String errorMessage)
        {
            e.Handled = true;
            box.BackColor = ErrorColor;
            message.Text = DefaultMessageHeader + errorMessage;
            FormSettings.TabStopTrigger(this, false);
        }

        private void NextData(bool mode)
        {
            DialogResult result = DialogResult.Cancel;

            if (list == null)
            {
                result = MessageBox.Show(
                   "貸出Noリストを取得します\n現在入力されているデータは破棄されますがよろしいですか？", "確認", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                if (result is DialogResult.OK)
                {
                    list = SetDataList();
                    index = -1;
                    FormSettings.ModeCancel(this, DefaultColor);
                    tbRentNo.ReadOnly = true;
                    tbRentNo.BackColor = SystemColors.Control;
                    tbRentDate.BackColor = SystemColors.Control;
                    tbBuppinKind.BackColor = SystemColors.Control;
                    tbRentCont.BackColor = SystemColors.Control;
                    tbRentUser.BackColor = SystemColors.Control;
                    RentcMnt.BackColor = SystemColors.Control;
                }
                else
                {
                    return;
                }

            }


            if (mode)
            {
                index++;
            }
            else
            {
                index--;
            }


            if (index <= -1)
            {
                index = list.Count - 1;
            }
            else if (index >= list.Count)
            {
                index = 0;
            }

            T0001RentHed rentHed = GetData(index);

            tbRentNo.Text = rentHed.T001RentNoStr;
            tbBuppinKind.Text = rentHed.T001bupnKnd.ToString();
            tbRentCont.Text = rentHed.T001RentCont.ToString();
            tbRentUser.Text = rentHed.T001RentUser.ToString();
            tbRentDate.Text = rentHed.T001RentDate.Date.ToString("yyyy/MM/dd");
            RentcMnt.Text = rentHed.T001RentCmnt.ToString();
        }

        private T0001RentHed GetData(int index)
        {
            return list[index];
        }

        private List<T0001RentHed> SetDataList()
        {
            list = new List<T0001RentHed>();
            index = 0;

            var conn = new DatabaseConnector();
            string table = "T001_RENT_HED";
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT `T001_RENTNO`, `T001_BUPNKND`, `T001_RENTCONT`, `T001_RENTUSER`, `T001_RENTDATE`,");
            sql.Append("`T001_RENTCMNT` FROM " + table);

            conn = new DatabaseConnector();
            DataSet dataSet = conn.Result(table, sql.ToString(), table);

            foreach (DataRow row in dataSet.Tables["T001_RENT_HED"].Rows)
            {
                list.Add(SetDataRow(row));
            }

            return list;
        }

        private T0001RentHed SetDataRow(DataRow row)
        {
            T0001RentHed rentHed = new T0001RentHed();

            rentHed.T001RentNoStr = row[0].ToString();
            rentHed.T001bupnKnd = Convert.ToInt32(row[1].ToString());
            rentHed.T001RentCont = row[2].ToString();
            rentHed.T001RentUser = row[3].ToString();
            rentHed.T001RentDate = System.Convert.ToDateTime(row[4].ToString());
            rentHed.T001RentCmnt = row[5].ToString();

            return rentHed;
        }

        private void MoveSelectRow(Keys key)
        {
            if (dataGridView.Rows.Count is 0)
            {
                return;
            }

            int rowIndex = 0;
            try
            {
                rowIndex = dataGridView.CurrentRow.Index;

            }
            catch
            {
                dataGridView.Rows[rowIndex].Selected = true;
                dataGridView.CurrentCell = dataGridView[0, rowIndex];
            }

            switch (key)
            {
                case Keys.F3:
                    if (rowIndex != 0)
                    {
                        dataGridView.Rows[rowIndex - 1].Selected = true;
                        dataGridView.Rows[rowIndex].Selected = false;
                        dataGridView.CurrentCell = dataGridView[0, rowIndex - 1];
                    }
                    return;
                case Keys.F4:
                    if (rowIndex + 1 != dataGridView.Rows.Count)
                    {
                        dataGridView.Rows[rowIndex].Selected = false;
                        dataGridView.Rows[rowIndex + 1].Selected = true;
                        dataGridView.CurrentCell = dataGridView[0, rowIndex + 1];
                    }
                    return;
            }
        }

        private void ForcedReceive()
        {
            if (CheckValues.IsEmpty(tbRentNo.Text) || dataGridView.Rows.Count == 0)
            {
                return;
            }
            
            T0002RentDtl dtl = new T0002RentDtl();
            dtl.T002RetrnNm = "システム";
            dtl.T002RetrnDate = DateTime.Today.ToString();
            dtl.T002RetrnCmnt = "この物品はシステムによって強制的に返却されました。";
            dtl.T002RentNo = Convert.ToInt32(rentNo);

            DatabaseConnector conn = new DatabaseConnector();
            string table = "m001_buppin";
            string sql = "select M001_BUPNLCAT1 from " + table + " where M001_BUPNCD = ";
            DataSet dataSet = null;

            bool hasReceive = false;

            for (int rowIndex = 0; rowIndex < dataGridView.Rows.Count; rowIndex++)
            {   
                dtl.T002RentSeq = (int)dataGridView.Rows[rowIndex].Cells[0].Value;
                dtl.T002BupnCd = (int)dataGridView.Rows[rowIndex].Cells[1].Value;
                dataSet = conn.Result(table, sql + dtl.T002BupnCd, table);
                dtl.T002RentLcat2 = dataSet.Tables[table].Rows[0][0].ToString();

                if (UpdateReturnData(dtl)){

                    hasReceive = true;
                }

                SetDataGridView();
            }
            if (hasReceive)
            {
                MessageBox.Show("返却が完了しました", "メッセージ", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

            }
        }

        private void dataGridView_Sorted(object sender, EventArgs e)
        {
            if (dataGridView.Rows.Count != 0)
            {
                SetRowColor();
            }
        }
    }
}
