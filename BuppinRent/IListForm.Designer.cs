﻿namespace BuppinRent
{
    partial class IListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.articleType = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.display = new System.Windows.Forms.Button();
            this.articleInquiryList = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.articleInquiryList)).BeginInit();
            this.SuspendLayout();
            // 
            // articleType
            // 
            this.articleType.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.articleType.Location = new System.Drawing.Point(238, 129);
            this.articleType.MaxLength = 1;
            this.articleType.Name = "articleType";
            this.articleType.Size = new System.Drawing.Size(22, 36);
            this.articleType.TabIndex = 1;
            this.articleType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label1.Location = new System.Drawing.Point(141, 132);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 28);
            this.label1.TabIndex = 12;
            this.label1.Text = "物品種別";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label2.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.label2.Location = new System.Drawing.Point(286, 132);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(198, 28);
            this.label2.TabIndex = 12;
            this.label2.Text = "0:全て 1:書籍 2:機器";
            // 
            // display
            // 
            this.display.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.display.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.display.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.display.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.display.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.display.Location = new System.Drawing.Point(987, 159);
            this.display.Name = "display";
            this.display.Size = new System.Drawing.Size(124, 80);
            this.display.TabIndex = 15;
            this.display.Text = "表示";
            this.display.UseVisualStyleBackColor = false;
            this.display.Click += new System.EventHandler(this.Display_Click);
            // 
            // articleInquiryList
            // 
            this.articleInquiryList.AllowUserToAddRows = false;
            this.articleInquiryList.AllowUserToDeleteRows = false;
            this.articleInquiryList.AllowUserToResizeColumns = false;
            this.articleInquiryList.AllowUserToResizeRows = false;
            this.articleInquiryList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.articleInquiryList.Location = new System.Drawing.Point(146, 305);
            this.articleInquiryList.Name = "articleInquiryList";
            this.articleInquiryList.ReadOnly = true;
            this.articleInquiryList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.articleInquiryList.RowTemplate.Height = 21;
            this.articleInquiryList.Size = new System.Drawing.Size(965, 553);
            this.articleInquiryList.TabIndex = 14;
            this.articleInquiryList.TabStop = false;
            this.articleInquiryList.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.PaintHeaderCells);
            this.articleInquiryList.Sorted += new System.EventHandler(this.ArticleInquiryList_Sorted);
            // 
            // IListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 1024);
            this.Controls.Add(this.articleInquiryList);
            this.Controls.Add(this.display);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.articleType);
            this.Name = "IListForm";
            this.Text = "FormArticleInquiry";
            this.Controls.SetChildIndex(this.title, 0);
            this.Controls.SetChildIndex(this.message, 0);
            this.Controls.SetChildIndex(this.f11, 0);
            this.Controls.SetChildIndex(this.f1, 0);
            this.Controls.SetChildIndex(this.f2, 0);
            this.Controls.SetChildIndex(this.f3, 0);
            this.Controls.SetChildIndex(this.f4, 0);
            this.Controls.SetChildIndex(this.f5, 0);
            this.Controls.SetChildIndex(this.f6, 0);
            this.Controls.SetChildIndex(this.f7, 0);
            this.Controls.SetChildIndex(this.f8, 0);
            this.Controls.SetChildIndex(this.f9, 0);
            this.Controls.SetChildIndex(this.f10, 0);
            this.Controls.SetChildIndex(this.f12, 0);
            this.Controls.SetChildIndex(this.articleType, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.display, 0);
            this.Controls.SetChildIndex(this.articleInquiryList, 0);
            ((System.ComponentModel.ISupportInitialize)(this.articleInquiryList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox articleType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button display;
        private System.Windows.Forms.DataGridView articleInquiryList;
    }
}