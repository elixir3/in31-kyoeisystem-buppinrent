﻿using BuppinRent.CrystalReports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuppinRent
{ 
    public partial class ModalStyle : Form
    {
        private Form parent;
        private Object param = null;
        private  Object instance;
        private int mode = 0;
        private string[] keywords;

        public ModalStyle(params object[] param) //0 Form 1 KeyObject 2 JumpTo 3 string[] search keywords
        {
            parent = param[0] as Form;
            if (parent is null)
            {          
                Close();
                Dispose();
            }
            this.param = param[1];
            mode = (int )param[2];
            if (param.Length == 4)
            {
                keywords = (string[])param[3];
            }

            InitializeComponent();
        }

        private void Setting()
        {

            FormBorderStyle = FormBorderStyle.None;
            StartPosition = FormStartPosition.CenterScreen;
            Size = parent.Size;
            Location = parent.Location;
            BackColor = Color.Red;
            Opacity = 0.7;
        }

        private void ModalStyle_Activated(object sender, EventArgs e)
        {
            if (instance is Form)
            {
                this.Close();
            }
        }

        private void ModalStyle_Load(object sender, EventArgs e)
        {
            Setting();

            switch (mode)
            {
                case 1:
                    using (SearchForm form = new SearchForm(parent, (String)param))
                    {
                        
                        form.Owner = parent;
                        form.StartPosition = FormStartPosition.CenterScreen;
                        form.ShowDialog();
                        instance = form;
                    }
                    break;

                case 2:
                    ShowPrintForm();
                    break;
                case 3:
                    using (SearchForm1 form1 = new SearchForm1(parent))
                    {
                        form1.Owner = parent;
                        form1.StartPosition = FormStartPosition.CenterScreen;
                        form1.ShowDialog();
                        instance = form1;
                        
                    }
                    break;
              
            }


        }

        private void ShowPrintForm()
        {
            switch (parent.Name)
            {
                case "ArticleInquiryForm":

                    using (ArticleInquiryPrint form = new ArticleInquiryPrint((DataSet)param, keywords))
                    {
                        instance = form;
                        form.Owner = parent;
                        form.StartPosition = FormStartPosition.CenterScreen;
                        form.ShowDialog();
                    }

                    break;

                case "ByCategoryOnLoanInquiryForm":

                    using (ByCatOnLoanInqPrint form = new ByCatOnLoanInqPrint((DataSet)param, keywords))
                    {
                        instance = form;
                        form.Owner = parent;
                        form.StartPosition = FormStartPosition.CenterScreen;
                        form.ShowDialog();
                    }

                    break;

                case "RentRegisterForm":

                    using (RentRegisterPrint form = new RentRegisterPrint((DataSet)param, keywords))
                    {
                        instance = form;
                        form.Owner = parent;
                        form.StartPosition = FormStartPosition.CenterScreen;
                        form.ShowDialog();
                    }

                    break;

                case "NonReceivedListForm":

                    using (NonReceivedListPrint form = new NonReceivedListPrint((DataSet)param, keywords))
                    {
                        instance = form;
                        form.Owner = parent;
                        form.StartPosition = FormStartPosition.CenterScreen;
                        form.ShowDialog();
                    }

                    break;

            }

        }

        public class Mode
        {
            public static int ModeSearch = 1;
            public static int ModePrint = 2;
            public static int ModeSearch1 = 3;
        }

        private void ModalStyle_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F11)
            {
                Dispose();
                Close();
            }
        }
    }
}
