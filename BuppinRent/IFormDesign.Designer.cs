﻿namespace BuppinRent
{
    partial class IFormDesign
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbTitle = new System.Windows.Forms.Label();
            this.lbMessage = new System.Windows.Forms.Label();
            this.btnF11 = new System.Windows.Forms.Button();
            this.btnF1 = new System.Windows.Forms.Button();
            this.btnF2 = new System.Windows.Forms.Button();
            this.btnF3 = new System.Windows.Forms.Button();
            this.btnF4 = new System.Windows.Forms.Button();
            this.btnF5 = new System.Windows.Forms.Button();
            this.btnF6 = new System.Windows.Forms.Button();
            this.btnF7 = new System.Windows.Forms.Button();
            this.btnF8 = new System.Windows.Forms.Button();
            this.btnF9 = new System.Windows.Forms.Button();
            this.btnF10 = new System.Windows.Forms.Button();
            this.btnF12 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbTitle
            // 
            this.lbTitle.BackColor = System.Drawing.Color.Gainsboro;
            this.lbTitle.CausesValidation = false;
            this.lbTitle.Font = new System.Drawing.Font("メイリオ", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbTitle.ForeColor = System.Drawing.Color.Navy;
            this.lbTitle.Location = new System.Drawing.Point(0, 0);
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lbTitle.Size = new System.Drawing.Size(1280, 40);
            this.lbTitle.TabIndex = 4;
            this.lbTitle.Text = "物品貸出しシステム";
            this.lbTitle.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lbMessage
            // 
            this.lbMessage.BackColor = System.Drawing.SystemColors.Control;
            this.lbMessage.CausesValidation = false;
            this.lbMessage.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbMessage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.lbMessage.Location = new System.Drawing.Point(0, 60);
            this.lbMessage.Name = "lbMessage";
            this.lbMessage.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.lbMessage.Size = new System.Drawing.Size(1280, 30);
            this.lbMessage.TabIndex = 5;
            this.lbMessage.Text = "[メッセージ] 物品貸出しシステム";
            this.lbMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnF11
            // 
            this.btnF11.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnF11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.btnF11.Location = new System.Drawing.Point(1061, 925);
            this.btnF11.Name = "btnF11";
            this.btnF11.Size = new System.Drawing.Size(96, 96);
            this.btnF11.TabIndex = 10;
            this.btnF11.UseVisualStyleBackColor = true;
            this.btnF11.Click += new System.EventHandler(this.BtnF11_Click);
            // 
            // btnF1
            // 
            this.btnF1.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnF1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.btnF1.Location = new System.Drawing.Point(21, 925);
            this.btnF1.Name = "btnF1";
            this.btnF1.Size = new System.Drawing.Size(96, 96);
            this.btnF1.TabIndex = 11;
            this.btnF1.UseVisualStyleBackColor = true;
            this.btnF1.Click += new System.EventHandler(this.BtnF1_Click);
            // 
            // btnF2
            // 
            this.btnF2.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnF2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.btnF2.Location = new System.Drawing.Point(122, 925);
            this.btnF2.Name = "btnF2";
            this.btnF2.Size = new System.Drawing.Size(96, 96);
            this.btnF2.TabIndex = 12;
            this.btnF2.UseVisualStyleBackColor = true;
            this.btnF2.Click += new System.EventHandler(this.BtnF2_Click);
            // 
            // btnF3
            // 
            this.btnF3.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnF3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.btnF3.Location = new System.Drawing.Point(223, 925);
            this.btnF3.Name = "btnF3";
            this.btnF3.Size = new System.Drawing.Size(96, 96);
            this.btnF3.TabIndex = 13;
            this.btnF3.UseVisualStyleBackColor = true;
            this.btnF3.Click += new System.EventHandler(this.BtnF3_Click);
            // 
            // btnF4
            // 
            this.btnF4.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnF4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.btnF4.Location = new System.Drawing.Point(324, 925);
            this.btnF4.Name = "btnF4";
            this.btnF4.Size = new System.Drawing.Size(96, 96);
            this.btnF4.TabIndex = 14;
            this.btnF4.UseVisualStyleBackColor = true;
            this.btnF4.Click += new System.EventHandler(this.BtnF4_Click);
            // 
            // btnF5
            // 
            this.btnF5.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnF5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.btnF5.Location = new System.Drawing.Point(438, 925);
            this.btnF5.Name = "btnF5";
            this.btnF5.Size = new System.Drawing.Size(96, 96);
            this.btnF5.TabIndex = 15;
            this.btnF5.UseVisualStyleBackColor = true;
            this.btnF5.Click += new System.EventHandler(this.BtnF5_Click);
            // 
            // btnF6
            // 
            this.btnF6.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnF6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.btnF6.Location = new System.Drawing.Point(539, 925);
            this.btnF6.Name = "btnF6";
            this.btnF6.Size = new System.Drawing.Size(96, 96);
            this.btnF6.TabIndex = 16;
            this.btnF6.UseVisualStyleBackColor = true;
            this.btnF6.Click += new System.EventHandler(this.BtnF6_Click);
            // 
            // btnF7
            // 
            this.btnF7.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnF7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.btnF7.Location = new System.Drawing.Point(640, 925);
            this.btnF7.Name = "btnF7";
            this.btnF7.Size = new System.Drawing.Size(96, 96);
            this.btnF7.TabIndex = 17;
            this.btnF7.UseVisualStyleBackColor = true;
            // 
            // btnF8
            // 
            this.btnF8.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnF8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.btnF8.Location = new System.Drawing.Point(741, 925);
            this.btnF8.Name = "btnF8";
            this.btnF8.Size = new System.Drawing.Size(96, 96);
            this.btnF8.TabIndex = 18;
            this.btnF8.UseVisualStyleBackColor = true;
            this.btnF8.Click += new System.EventHandler(this.BtnF8_Click);
            // 
            // btnF9
            // 
            this.btnF9.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnF9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.btnF9.Location = new System.Drawing.Point(859, 925);
            this.btnF9.Name = "btnF9";
            this.btnF9.Size = new System.Drawing.Size(96, 96);
            this.btnF9.TabIndex = 19;
            this.btnF9.UseVisualStyleBackColor = true;
            this.btnF9.Click += new System.EventHandler(this.BtnF9_Click);
            // 
            // btnF10
            // 
            this.btnF10.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnF10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.btnF10.Location = new System.Drawing.Point(960, 925);
            this.btnF10.Name = "btnF10";
            this.btnF10.Size = new System.Drawing.Size(96, 96);
            this.btnF10.TabIndex = 20;
            this.btnF10.UseVisualStyleBackColor = true;
            this.btnF10.Click += new System.EventHandler(this.BtnF10_Click);
            // 
            // btnF12
            // 
            this.btnF12.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnF12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.btnF12.Location = new System.Drawing.Point(1162, 925);
            this.btnF12.Name = "btnF12";
            this.btnF12.Size = new System.Drawing.Size(96, 96);
            this.btnF12.TabIndex = 21;
            this.btnF12.UseVisualStyleBackColor = true;
            this.btnF12.Click += new System.EventHandler(this.BtnF12_Click);
            // 
            // IFormDesign
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 1024);
            this.Controls.Add(this.btnF12);
            this.Controls.Add(this.btnF10);
            this.Controls.Add(this.btnF9);
            this.Controls.Add(this.btnF8);
            this.Controls.Add(this.btnF7);
            this.Controls.Add(this.btnF6);
            this.Controls.Add(this.btnF5);
            this.Controls.Add(this.btnF4);
            this.Controls.Add(this.btnF3);
            this.Controls.Add(this.btnF2);
            this.Controls.Add(this.btnF1);
            this.Controls.Add(this.btnF11);
            this.Controls.Add(this.lbMessage);
            this.Controls.Add(this.lbTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "IFormDesign";
            this.Text = "FormInsterface";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FoundKeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbTitle;
        private System.Windows.Forms.Label lbMessage;
        private System.Windows.Forms.Button btnF11;
        private System.Windows.Forms.Button btnF1;
        private System.Windows.Forms.Button btnF2;
        private System.Windows.Forms.Button btnF3;
        private System.Windows.Forms.Button btnF4;
        private System.Windows.Forms.Button btnF5;
        private System.Windows.Forms.Button btnF6;
        private System.Windows.Forms.Button btnF7;
        private System.Windows.Forms.Button btnF8;
        private System.Windows.Forms.Button btnF9;
        private System.Windows.Forms.Button btnF10;
        private System.Windows.Forms.Button btnF12;
    }
}