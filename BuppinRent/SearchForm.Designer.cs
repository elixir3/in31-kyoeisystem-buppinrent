﻿namespace BuppinRent
{
    partial class SearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbBuppinName = new System.Windows.Forms.TextBox();
            this.tbBuppinKind = new System.Windows.Forms.TextBox();
            this.tbBuppinGroup = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.searchResultList = new System.Windows.Forms.DataGridView();
            this.display = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.searchResultList)).BeginInit();
            this.SuspendLayout();
            // 
            // f1
            // 
            this.f1.Location = new System.Drawing.Point(12, 925);
            this.f1.Size = new System.Drawing.Size(76, 96);
            // 
            // f2
            // 
            this.f2.Location = new System.Drawing.Point(94, 925);
            this.f2.Size = new System.Drawing.Size(76, 96);
            // 
            // f3
            // 
            this.f3.Location = new System.Drawing.Point(178, 925);
            this.f3.Size = new System.Drawing.Size(76, 96);
            // 
            // f4
            // 
            this.f4.Location = new System.Drawing.Point(260, 925);
            this.f4.Size = new System.Drawing.Size(76, 96);
            // 
            // f5
            // 
            this.f5.Location = new System.Drawing.Point(342, 925);
            this.f5.Size = new System.Drawing.Size(76, 96);
            // 
            // f6
            // 
            this.f6.Location = new System.Drawing.Point(424, 925);
            this.f6.Size = new System.Drawing.Size(76, 96);
            // 
            // f7
            // 
            this.f7.Location = new System.Drawing.Point(506, 925);
            this.f7.Size = new System.Drawing.Size(76, 96);
            // 
            // f8
            // 
            this.f8.Location = new System.Drawing.Point(588, 925);
            this.f8.Size = new System.Drawing.Size(76, 96);
            // 
            // f9
            // 
            this.f9.Location = new System.Drawing.Point(670, 925);
            this.f9.Size = new System.Drawing.Size(76, 96);
            this.f9.TabIndex = 5;
            // 
            // f10
            // 
            this.f10.Location = new System.Drawing.Point(752, 925);
            this.f10.Size = new System.Drawing.Size(76, 96);
            this.f10.TabIndex = 6;
            // 
            // f11
            // 
            this.f11.Location = new System.Drawing.Point(834, 925);
            this.f11.Size = new System.Drawing.Size(76, 96);
            this.f11.TabIndex = 7;
            // 
            // f12
            // 
            this.f12.Location = new System.Drawing.Point(916, 925);
            this.f12.Size = new System.Drawing.Size(76, 96);
            this.f12.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label1.Location = new System.Drawing.Point(164, 132);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 28);
            this.label1.TabIndex = 22;
            this.label1.Text = "物品名称";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label3.Location = new System.Drawing.Point(164, 174);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 56);
            this.label3.TabIndex = 22;
            this.label3.Text = "物品種別";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label4.Location = new System.Drawing.Point(143, 232);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 56);
            this.label4.TabIndex = 22;
            this.label4.Text = "分類コード";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbBuppinName
            // 
            this.tbBuppinName.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbBuppinName.Location = new System.Drawing.Point(281, 129);
            this.tbBuppinName.Multiline = true;
            this.tbBuppinName.Name = "tbBuppinName";
            this.tbBuppinName.Size = new System.Drawing.Size(353, 36);
            this.tbBuppinName.TabIndex = 1;
            // 
            // tbBuppinKind
            // 
            this.tbBuppinKind.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbBuppinKind.Location = new System.Drawing.Point(282, 185);
            this.tbBuppinKind.MaxLength = 1;
            this.tbBuppinKind.Multiline = true;
            this.tbBuppinKind.Name = "tbBuppinKind";
            this.tbBuppinKind.Size = new System.Drawing.Size(48, 36);
            this.tbBuppinKind.TabIndex = 2;
            this.tbBuppinKind.Text = "0";
            this.tbBuppinKind.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbBuppinKind.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // tbBuppinGroup
            // 
            this.tbBuppinGroup.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbBuppinGroup.Location = new System.Drawing.Point(281, 241);
            this.tbBuppinGroup.MaxLength = 1;
            this.tbBuppinGroup.Multiline = true;
            this.tbBuppinGroup.Name = "tbBuppinGroup";
            this.tbBuppinGroup.Size = new System.Drawing.Size(48, 36);
            this.tbBuppinGroup.TabIndex = 3;
            this.tbBuppinGroup.Text = "0";
            this.tbBuppinGroup.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbBuppinGroup.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.label10.Location = new System.Drawing.Point(336, 188);
            this.label10.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(134, 28);
            this.label10.TabIndex = 29;
            this.label10.Text = "1:書籍 2:機器";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.label11.Location = new System.Drawing.Point(336, 244);
            this.label11.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(522, 28);
            this.label11.TabIndex = 30;
            this.label11.Text = "1:本体 2:モニタ 3:プリンター 4:モデム 5:ルータ 6:ハブ   ";
            // 
            // searchResultList
            // 
            this.searchResultList.AllowUserToAddRows = false;
            this.searchResultList.AllowUserToDeleteRows = false;
            this.searchResultList.AllowUserToResizeColumns = false;
            this.searchResultList.AllowUserToResizeRows = false;
            this.searchResultList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.searchResultList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.searchResultList.Location = new System.Drawing.Point(25, 305);
            this.searchResultList.Name = "searchResultList";
            this.searchResultList.ReadOnly = true;
            this.searchResultList.RowTemplate.Height = 21;
            this.searchResultList.Size = new System.Drawing.Size(950, 487);
            this.searchResultList.TabIndex = 31;
            this.searchResultList.TabStop = false;
            this.searchResultList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.SearchResultList_CellDoubleClick);
            this.searchResultList.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.PaintHeaderCells);
            // 
            // display
            // 
            this.display.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.display.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.display.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.display.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.display.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.display.Location = new System.Drawing.Point(851, 162);
            this.display.Name = "display";
            this.display.Size = new System.Drawing.Size(124, 80);
            this.display.TabIndex = 4;
            this.display.Text = "表示";
            this.display.UseVisualStyleBackColor = false;
            this.display.Click += new System.EventHandler(this.Display_Click);
            // 
            // SearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 1024);
            this.Controls.Add(this.display);
            this.Controls.Add(this.searchResultList);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tbBuppinGroup);
            this.Controls.Add(this.tbBuppinKind);
            this.Controls.Add(this.tbBuppinName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Name = "SearchForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ArticleSearchForm";
            this.Controls.SetChildIndex(this.title, 0);
            this.Controls.SetChildIndex(this.message, 0);
            this.Controls.SetChildIndex(this.f11, 0);
            this.Controls.SetChildIndex(this.f1, 0);
            this.Controls.SetChildIndex(this.f2, 0);
            this.Controls.SetChildIndex(this.f3, 0);
            this.Controls.SetChildIndex(this.f4, 0);
            this.Controls.SetChildIndex(this.f5, 0);
            this.Controls.SetChildIndex(this.f6, 0);
            this.Controls.SetChildIndex(this.f7, 0);
            this.Controls.SetChildIndex(this.f8, 0);
            this.Controls.SetChildIndex(this.f9, 0);
            this.Controls.SetChildIndex(this.f10, 0);
            this.Controls.SetChildIndex(this.f12, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.tbBuppinName, 0);
            this.Controls.SetChildIndex(this.tbBuppinKind, 0);
            this.Controls.SetChildIndex(this.tbBuppinGroup, 0);
            this.Controls.SetChildIndex(this.label10, 0);
            this.Controls.SetChildIndex(this.label11, 0);
            this.Controls.SetChildIndex(this.searchResultList, 0);
            this.Controls.SetChildIndex(this.display, 0);
            ((System.ComponentModel.ISupportInitialize)(this.searchResultList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbBuppinName;
        private System.Windows.Forms.TextBox tbBuppinKind;
        private System.Windows.Forms.TextBox tbBuppinGroup;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridView searchResultList;
        private System.Windows.Forms.Button display;
    }
}