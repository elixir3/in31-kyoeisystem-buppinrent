﻿namespace BuppinRent
{
    partial class RentRegisterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbBuppinKind = new System.Windows.Forms.TextBox();
            this.tbRentCont = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbRentUser = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbBuppinCode = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.rentList = new System.Windows.Forms.DataGridView();
            this.bg = new System.Windows.Forms.Label();
            this.lbBuppinName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbYear = new System.Windows.Forms.TextBox();
            this.tbMonth = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.tbDay = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.tbPlanDay = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.tbPlanMonth = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tbCnd = new System.Windows.Forms.TextBox();
            this.lbNo = new System.Windows.Forms.Label();
            this.tbLcat2 = new System.Windows.Forms.TextBox();
            this.tbLcat1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbPlanYear = new System.Windows.Forms.TextBox();
            this.RentcMnt = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.rentList)).BeginInit();
            this.SuspendLayout();
            // 
            // title
            // 
            this.title.Text = "物品貸出";
            // 
            // message
            // 
            this.message.Text = "[メッセージ] 物品貸出";
            // 
            // f11
            // 
            this.f11.TabIndex = 15;
            // 
            // tbBuppinKind
            // 
            this.tbBuppinKind.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbBuppinKind.Location = new System.Drawing.Point(230, 109);
            this.tbBuppinKind.MaxLength = 1;
            this.tbBuppinKind.Name = "tbBuppinKind";
            this.tbBuppinKind.Size = new System.Drawing.Size(26, 36);
            this.tbBuppinKind.TabIndex = 1;
            this.tbBuppinKind.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // tbRentCont
            // 
            this.tbRentCont.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbRentCont.Location = new System.Drawing.Point(229, 165);
            this.tbRentCont.MaxLength = 20;
            this.tbRentCont.Name = "tbRentCont";
            this.tbRentCont.Size = new System.Drawing.Size(204, 36);
            this.tbRentCont.TabIndex = 2;
            this.tbRentCont.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label2.Location = new System.Drawing.Point(136, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 28);
            this.label2.TabIndex = 23;
            this.label2.Text = "物品種別";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label3.Location = new System.Drawing.Point(135, 168);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 28);
            this.label3.TabIndex = 23;
            this.label3.Text = "借受者名";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.label6.Location = new System.Drawing.Point(266, 112);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(134, 28);
            this.label6.TabIndex = 23;
            this.label6.Text = "1:書籍 2:機器";
            // 
            // tbRentUser
            // 
            this.tbRentUser.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbRentUser.Location = new System.Drawing.Point(229, 221);
            this.tbRentUser.MaxLength = 20;
            this.tbRentUser.Name = "tbRentUser";
            this.tbRentUser.Size = new System.Drawing.Size(204, 36);
            this.tbRentUser.TabIndex = 3;
            this.tbRentUser.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label11.Location = new System.Drawing.Point(135, 224);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 28);
            this.label11.TabIndex = 23;
            this.label11.Text = "利用者名";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label5.Location = new System.Drawing.Point(840, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 28);
            this.label5.TabIndex = 23;
            this.label5.Text = "貸出日";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label4.Location = new System.Drawing.Point(859, 168);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 28);
            this.label4.TabIndex = 23;
            this.label4.Text = "備考";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label7.Location = new System.Drawing.Point(32, 302);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 28);
            this.label7.TabIndex = 23;
            this.label7.Text = "No.";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label8.Location = new System.Drawing.Point(116, 302);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(107, 28);
            this.label8.TabIndex = 23;
            this.label8.Text = "物品コード";
            // 
            // tbBuppinCode
            // 
            this.tbBuppinCode.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbBuppinCode.Location = new System.Drawing.Point(229, 299);
            this.tbBuppinCode.MaxLength = 5;
            this.tbBuppinCode.Name = "tbBuppinCode";
            this.tbBuppinCode.Size = new System.Drawing.Size(112, 36);
            this.tbBuppinCode.TabIndex = 8;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label9.Location = new System.Drawing.Point(118, 358);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(107, 28);
            this.label9.TabIndex = 23;
            this.label9.Text = "返却予定日";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label12.Location = new System.Drawing.Point(99, 416);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(126, 28);
            this.label12.TabIndex = 23;
            this.label12.Text = "現在保管場所";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label13.Location = new System.Drawing.Point(859, 300);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(50, 28);
            this.label13.TabIndex = 23;
            this.label13.Text = "状態";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label14.Location = new System.Drawing.Point(822, 328);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(87, 28);
            this.label14.TabIndex = 23;
            this.label14.Text = "(付属品)";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rentList
            // 
            this.rentList.AllowUserToAddRows = false;
            this.rentList.AllowUserToDeleteRows = false;
            this.rentList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rentList.Location = new System.Drawing.Point(37, 468);
            this.rentList.Name = "rentList";
            this.rentList.ReadOnly = true;
            this.rentList.RowTemplate.Height = 21;
            this.rentList.Size = new System.Drawing.Size(1213, 410);
            this.rentList.TabIndex = 25;
            this.rentList.TabStop = false;
            // 
            // bg
            // 
            this.bg.BackColor = System.Drawing.SystemColors.ControlDark;
            this.bg.Location = new System.Drawing.Point(30, 280);
            this.bg.Name = "bg";
            this.bg.Size = new System.Drawing.Size(1220, 1);
            this.bg.TabIndex = 26;
            this.bg.Text = " ";
            this.bg.UseMnemonic = false;
            // 
            // lbBuppinName
            // 
            this.lbBuppinName.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.lbBuppinName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.lbBuppinName.Location = new System.Drawing.Point(347, 305);
            this.lbBuppinName.Name = "lbBuppinName";
            this.lbBuppinName.Size = new System.Drawing.Size(444, 38);
            this.lbBuppinName.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label1.Location = new System.Drawing.Point(1009, 112);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 28);
            this.label1.TabIndex = 28;
            this.label1.Text = "/";
            // 
            // tbYear
            // 
            this.tbYear.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbYear.Location = new System.Drawing.Point(951, 109);
            this.tbYear.MaxLength = 2;
            this.tbYear.Name = "tbYear";
            this.tbYear.Size = new System.Drawing.Size(52, 36);
            this.tbYear.TabIndex = 4;
            this.tbYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbYear.TextChanged += new System.EventHandler(this.TbPlanYear_TextChanged);
            this.tbYear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // tbMonth
            // 
            this.tbMonth.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbMonth.Location = new System.Drawing.Point(1038, 109);
            this.tbMonth.MaxLength = 2;
            this.tbMonth.Name = "tbMonth";
            this.tbMonth.Size = new System.Drawing.Size(52, 36);
            this.tbMonth.TabIndex = 5;
            this.tbMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbMonth.TextChanged += new System.EventHandler(this.TbPlanYear_TextChanged);
            this.tbMonth.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label19.Location = new System.Drawing.Point(1096, 112);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(23, 28);
            this.label19.TabIndex = 31;
            this.label19.Text = "/";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label20.Location = new System.Drawing.Point(1211, 112);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(0, 28);
            this.label20.TabIndex = 33;
            // 
            // tbDay
            // 
            this.tbDay.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbDay.Location = new System.Drawing.Point(1125, 109);
            this.tbDay.MaxLength = 2;
            this.tbDay.Name = "tbDay";
            this.tbDay.Size = new System.Drawing.Size(52, 36);
            this.tbDay.TabIndex = 6;
            this.tbDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbDay.TextChanged += new System.EventHandler(this.TbPlanYear_TextChanged);
            this.tbDay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label21.Location = new System.Drawing.Point(910, 112);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(38, 28);
            this.label21.TabIndex = 34;
            this.label21.Text = "20";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label22.Location = new System.Drawing.Point(231, 358);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(38, 28);
            this.label22.TabIndex = 41;
            this.label22.Text = "20";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label23.Location = new System.Drawing.Point(532, 358);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(0, 28);
            this.label23.TabIndex = 40;
            // 
            // tbPlanDay
            // 
            this.tbPlanDay.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbPlanDay.Location = new System.Drawing.Point(446, 355);
            this.tbPlanDay.MaxLength = 2;
            this.tbPlanDay.Name = "tbPlanDay";
            this.tbPlanDay.Size = new System.Drawing.Size(52, 36);
            this.tbPlanDay.TabIndex = 11;
            this.tbPlanDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbPlanDay.TextChanged += new System.EventHandler(this.TbPlanYear_TextChanged);
            this.tbPlanDay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label24.Location = new System.Drawing.Point(417, 358);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(23, 28);
            this.label24.TabIndex = 38;
            this.label24.Text = "/";
            // 
            // tbPlanMonth
            // 
            this.tbPlanMonth.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbPlanMonth.Location = new System.Drawing.Point(359, 355);
            this.tbPlanMonth.MaxLength = 2;
            this.tbPlanMonth.Name = "tbPlanMonth";
            this.tbPlanMonth.Size = new System.Drawing.Size(52, 36);
            this.tbPlanMonth.TabIndex = 10;
            this.tbPlanMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbPlanMonth.TextChanged += new System.EventHandler(this.TbPlanYear_TextChanged);
            this.tbPlanMonth.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label25.Location = new System.Drawing.Point(330, 358);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(23, 28);
            this.label25.TabIndex = 35;
            this.label25.Text = "/";
            // 
            // tbCnd
            // 
            this.tbCnd.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbCnd.Location = new System.Drawing.Point(912, 299);
            this.tbCnd.Multiline = true;
            this.tbCnd.Name = "tbCnd";
            this.tbCnd.ReadOnly = true;
            this.tbCnd.Size = new System.Drawing.Size(335, 114);
            this.tbCnd.TabIndex = 43;
            // 
            // lbNo
            // 
            this.lbNo.AutoSize = true;
            this.lbNo.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.lbNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.lbNo.Location = new System.Drawing.Point(71, 302);
            this.lbNo.Name = "lbNo";
            this.lbNo.Size = new System.Drawing.Size(25, 28);
            this.lbNo.TabIndex = 44;
            this.lbNo.Text = "1";
            // 
            // tbLcat2
            // 
            this.tbLcat2.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbLcat2.Location = new System.Drawing.Point(236, 413);
            this.tbLcat2.MaxLength = 20;
            this.tbLcat2.Name = "tbLcat2";
            this.tbLcat2.ReadOnly = true;
            this.tbLcat2.Size = new System.Drawing.Size(204, 36);
            this.tbLcat2.TabIndex = 45;
            // 
            // tbLcat1
            // 
            this.tbLcat1.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbLcat1.Location = new System.Drawing.Point(916, 413);
            this.tbLcat1.MaxLength = 20;
            this.tbLcat1.Name = "tbLcat1";
            this.tbLcat1.ReadOnly = true;
            this.tbLcat1.Size = new System.Drawing.Size(204, 36);
            this.tbLcat1.TabIndex = 48;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label10.Location = new System.Drawing.Point(822, 416);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 28);
            this.label10.TabIndex = 47;
            this.label10.Text = "返却場所";
            // 
            // tbPlanYear
            // 
            this.tbPlanYear.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbPlanYear.Location = new System.Drawing.Point(272, 355);
            this.tbPlanYear.MaxLength = 2;
            this.tbPlanYear.Name = "tbPlanYear";
            this.tbPlanYear.Size = new System.Drawing.Size(52, 36);
            this.tbPlanYear.TabIndex = 9;
            this.tbPlanYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbPlanYear.TextChanged += new System.EventHandler(this.TbPlanYear_TextChanged);
            this.tbPlanYear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // RentcMnt
            // 
            this.RentcMnt.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.RentcMnt.Location = new System.Drawing.Point(915, 165);
            this.RentcMnt.MaxLength = 50;
            this.RentcMnt.Name = "RentcMnt";
            this.RentcMnt.Size = new System.Drawing.Size(262, 92);
            this.RentcMnt.TabIndex = 49;
            this.RentcMnt.Text = "";
            // 
            // RentRegisterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 1024);
            this.Controls.Add(this.RentcMnt);
            this.Controls.Add(this.tbLcat1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tbLcat2);
            this.Controls.Add(this.lbNo);
            this.Controls.Add(this.tbCnd);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.tbPlanDay);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.tbPlanMonth);
            this.Controls.Add(this.tbPlanYear);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.tbDay);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.tbMonth);
            this.Controls.Add(this.tbYear);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bg);
            this.Controls.Add(this.rentList);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lbBuppinName);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbBuppinKind);
            this.Controls.Add(this.tbRentUser);
            this.Controls.Add(this.tbRentCont);
            this.Controls.Add(this.tbBuppinCode);
            this.Name = "RentRegisterForm";
            this.Text = "LendingRecordRegistrationForm";
            this.Controls.SetChildIndex(this.tbBuppinCode, 0);
            this.Controls.SetChildIndex(this.tbRentCont, 0);
            this.Controls.SetChildIndex(this.tbRentUser, 0);
            this.Controls.SetChildIndex(this.tbBuppinKind, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.lbBuppinName, 0);
            this.Controls.SetChildIndex(this.label9, 0);
            this.Controls.SetChildIndex(this.label12, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.label13, 0);
            this.Controls.SetChildIndex(this.label14, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.label11, 0);
            this.Controls.SetChildIndex(this.rentList, 0);
            this.Controls.SetChildIndex(this.bg, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.tbYear, 0);
            this.Controls.SetChildIndex(this.tbMonth, 0);
            this.Controls.SetChildIndex(this.label19, 0);
            this.Controls.SetChildIndex(this.tbDay, 0);
            this.Controls.SetChildIndex(this.label20, 0);
            this.Controls.SetChildIndex(this.label21, 0);
            this.Controls.SetChildIndex(this.label25, 0);
            this.Controls.SetChildIndex(this.tbPlanYear, 0);
            this.Controls.SetChildIndex(this.tbPlanMonth, 0);
            this.Controls.SetChildIndex(this.label24, 0);
            this.Controls.SetChildIndex(this.tbPlanDay, 0);
            this.Controls.SetChildIndex(this.label23, 0);
            this.Controls.SetChildIndex(this.label22, 0);
            this.Controls.SetChildIndex(this.tbCnd, 0);
            this.Controls.SetChildIndex(this.lbNo, 0);
            this.Controls.SetChildIndex(this.tbLcat2, 0);
            this.Controls.SetChildIndex(this.label10, 0);
            this.Controls.SetChildIndex(this.tbLcat1, 0);
            this.Controls.SetChildIndex(this.title, 0);
            this.Controls.SetChildIndex(this.message, 0);
            this.Controls.SetChildIndex(this.f11, 0);
            this.Controls.SetChildIndex(this.f1, 0);
            this.Controls.SetChildIndex(this.f2, 0);
            this.Controls.SetChildIndex(this.f3, 0);
            this.Controls.SetChildIndex(this.f4, 0);
            this.Controls.SetChildIndex(this.f5, 0);
            this.Controls.SetChildIndex(this.f6, 0);
            this.Controls.SetChildIndex(this.f7, 0);
            this.Controls.SetChildIndex(this.f8, 0);
            this.Controls.SetChildIndex(this.f9, 0);
            this.Controls.SetChildIndex(this.f10, 0);
            this.Controls.SetChildIndex(this.f12, 0);
            this.Controls.SetChildIndex(this.RentcMnt, 0);
            ((System.ComponentModel.ISupportInitialize)(this.rentList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox tbBuppinKind;
        private System.Windows.Forms.TextBox tbRentCont;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbRentUser;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbBuppinCode;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridView rentList;
        private System.Windows.Forms.Label bg;
        private System.Windows.Forms.Label lbBuppinName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbYear;
        private System.Windows.Forms.TextBox tbMonth;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tbDay;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox tbPlanDay;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox tbPlanMonth;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox tbCnd;
        private System.Windows.Forms.Label lbNo;
        private System.Windows.Forms.TextBox tbLcat2;
        private System.Windows.Forms.TextBox tbLcat1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbPlanYear;
        private System.Windows.Forms.RichTextBox RentcMnt;
    }
}