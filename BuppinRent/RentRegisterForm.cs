﻿using BuppinRent.ColumnsObj;
using BuppinRent.Database;
using BuppinRent.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuppinRent
{

    /*  
*  Created By HAL OSAKA Katsuya Akamatsu on 2017/10/10 TUE
*  F001_BUPPIN_MNT 貸し出す物品を登録変更削除するためのフォーム
*/
    public partial class RentRegisterForm : IFormDesign
    {
        private string DefaultTitle = "物品貸出";
        private string DefaultMessageHeader = "[メッセージ]  ";
        private string defaultMessage = "物品マスタの登録・変更及び削除が行えます";

        private int rentNo = 0;
        private int dtlNo = 0;
        private string[] headerText;
        //貸出フラグ
        private int bupnrent = 9;

        private bool hasRent;
        private bool errorDate;

        public RentRegisterForm()
        { 
            OnCreate();
            InitializeComponent();
        }

        protected void OnCreate()
        {
            title.Text = DefaultTitle;
            message.Text = DefaultMessageHeader + defaultMessage;

            FormSettings.EnabledFunction(f1, true, "検索");
            FormSettings.EnabledFunction(f3, true, "前の行");
            FormSettings.EnabledFunction(f4, true, "次の行");
            FormSettings.EnabledFunction(f8, true, "登録");
            //FormSettings.EnabledFunction(f9, true, "印刷");
            FormSettings.EnabledFunction(f10, true, "クリア");
           // FormSettings.EnabledFunction(f12, true, "削除");
        }

        protected override void FoundKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    BtnF1_Click(null, null);
                    return;
                case Keys.F3:
                    MoveSelectRow(Keys.F3);
                    return;
                case Keys.F4:
                    MoveSelectRow(Keys.F4);
                    return;
                case Keys.F8:
                    BtnF8_Click(null, null);
                    return;
                case Keys.F9:
                    BtnF9_Click(null, null);
                    return;
                case Keys.F10:
                    BtnF10_Click(null, null);
                    return;
                case Keys.F12:
                    BtnF12_Click(null, null);
                    return;
            }

            base.FoundKeyDown(sender, e);
        }

        protected override void BtnF1_Click(object sender, EventArgs e)
        {
            string kind = tbBuppinKind.Text;
            if (CheckValues.IsEmpty(kind))
            {
                MessageBox.Show(
      "先に物品種別を入力してください", "メッセージ", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

                return;
            }

            using (ModalStyle modal = new ModalStyle(this, kind, ModalStyle.Mode.ModeSearch))
            {
                modal.Owner = this;
                modal.ShowDialog();
            }

        }

        protected override void BtnF8_Click(object sender, EventArgs e)
        {
            Submit();
        }

        protected override void BtnF10_Click(object sender, EventArgs e)
        {
            Color white = Color.White;
            Color glay = SystemColors.Control;
            if (!hasRent)
            {
                FormSettings.ModeCancel(this, white);
                tbLcat1.BackColor = glay;
                tbLcat2.BackColor = glay;
                tbCnd.BackColor = glay;
                tbBuppinKind.ReadOnly = false;
            }

            tbBuppinCode.Text = "";
            tbPlanYear.Text = "";
            tbPlanMonth.Text = "";
            tbPlanDay.Text = "";
            tbPlanYear.BackColor = white;
            tbPlanMonth.BackColor = white;
            tbPlanDay.BackColor = white;
            tbLcat1.Text = "";
            tbLcat2.Text = "";
            tbCnd.Text = "";
            lbBuppinName.Text = "";

            FormSettings.TabStopTrigger(tbBuppinCode, true);
            FormSettings.TabStopTrigger(tbPlanYear, true);
            FormSettings.TabStopTrigger(tbPlanMonth, true);
            FormSettings.TabStopTrigger(tbPlanDay, true);
            FormSettings.EnabledFunction(f8, true);
            FormSettings.EnabledFunction(f10, true);
            FormSettings.EnabledFunction(f11, true);
            FormSettings.EnabledFunction(f12, true);

            rentList.TabStop = false;
        }

        protected override void BtnF11_Click(object sender, EventArgs e)
        {
            DataSet dataSet;
            DatabaseConnector conn;
            StringBuilder sql;
            string table;
            string[] keywords;
            if (hasRent)
            {
                keywords = new string[5];
                keywords[0] = rentNo.ToString();
                keywords[1] = tbYear.Text + "/" + tbMonth.Text + "/" + tbDay.Text;
                keywords[2] = tbRentCont.Text;
                keywords[3] = tbRentUser.Text;
                keywords[4] = RentcMnt.Text;

                table = "register_view";
                sql = new StringBuilder();
                sql.Append("SELECT `RENTSEQ`, `BUPNCD`, `BUPNNM`, `BUPNCND`, `RETRNPLAN`, `LCAT2`, `LCAT1`  ");
                sql.Append(" FROM `" + table +"` where T002_RENTNO = " + rentNo);
                conn = new DatabaseConnector();
                dataSet = conn.Result(table, sql.ToString(), table);
                using (ModalStyle modal = new ModalStyle(this, dataSet, ModalStyle.Mode.ModePrint, keywords))
                {
                    modal.ShowDialog();
                    

                }
            }

            Dispose();
        }

        protected override void BtnF12_Click(object sender, EventArgs e)
        {

        }

        //Setter データ受け取りに使用する
        public void SetTbBuppinCode(string buppinCode)
        {
            tbBuppinCode.Text = buppinCode;
        }

        //Setter データ受け取りに使用する
        public void SetTbBuppinLcat(string buppinLcat)
        {
            tbLcat2.Text = buppinLcat;
        }

        //Setter データ受け取りに使用する
        public void SetTbBuppinCnd(string buppinCnd)
        {
            tbCnd.Text = buppinCnd;
        }

        //更新ボタン(F8)が押された時の処理
        private void Submit()
        {
            T0001RentHed hed = GetRentHed();
            T0002RentDtl dtl = GetDtl();

            if (hed is null || dtl is null)
            {
                MessageBox.Show(
      "入力されていない箇所があります", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                message.Text = DefaultMessageHeader + "入力されていない箇所があります";
                return;
            }


            if (hed.T001RentDate > dtl.T002RetrnPlan)
            {
                MessageBox.Show(
                   "正しい日付を入力してください。", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                message.Text = DefaultMessageHeader + "正しい日付を入力してください";
                tbPlanYear.BackColor = ErrorColor;
                tbPlanMonth.BackColor = ErrorColor;
                tbPlanDay.BackColor = ErrorColor;
                errorDate = true;
                return;

            }


            if (rentNo == 0)
            {
                InsertHed(hed);
                
            }
            if (InsertDtl(dtl))
            {
                lbNo.Text = (Convert.ToInt32(lbNo.Text) + 1).ToString();

                tbBuppinKind.ReadOnly = true;
                tbBuppinKind.BackColor = SystemColors.Control;
                tbYear.ReadOnly = true;
                tbMonth.ReadOnly = true;
                tbDay.ReadOnly = true;
                tbYear.BackColor = SystemColors.Control;
                tbMonth.BackColor = SystemColors.Control;
                tbDay.BackColor = SystemColors.Control;
                RentcMnt.BackColor = SystemColors.Control;
                RentcMnt.ReadOnly = true;

                RentReady();
            }

        }

        private void RentReady()
        {
            tbBuppinKind.ReadOnly = true;
            tbRentCont.ReadOnly = true;
            tbRentUser.ReadOnly = true;
            tbYear.ReadOnly = true;
            tbMonth.ReadOnly = true;
            tbDay.ReadOnly = true;
            tbRentCont.ReadOnly = true;
            tbBuppinCode.Text = "";
            lbBuppinName.Text = "";
            tbPlanYear.Text = "";
            tbPlanMonth.Text = "";
            tbPlanDay.Text = "";
            defaultMessage = "貸出Noは" + rentNo + "です、返却時に必要です。";
            message.Text = DefaultMessageHeader + defaultMessage;
        }

        private void InsertHed(T0001RentHed hed)
        {
            StringBuilder sql;
            
            string table = "t001_rent_hed";
            sql = new StringBuilder();
            sql.Append("SELECT `T001_RENTNO` FROM "+ table + " ORDER BY `T001_RENTNO` DESC LIMIT 1");
            DatabaseConnector conn = new DatabaseConnector();
            DataTable dataTable = conn.Result(table, sql.ToString(), table).Tables[table];
            if (!CheckValues.IsEmpty(dataTable))
            {
                rentNo = Convert.ToInt32(dataTable.Rows[0][0]) + 1;
            }
            else
            {
                rentNo = 1;
            }
            sql = new StringBuilder();
            sql.Append("INSERT INTO `t001_rent_hed`(`T001_BUPNKND`, `T001_RENTDATE`, `T001_RENTCONT`, `T001_RENTUSER`, `T001_RENTCMNT`, `T001_INSDATE`, `T001_INSTIME`) VALUES (");
            sql.Append("" + hed.T001bupnKnd + ", ");
            sql.Append("" + hed.T001RentDate.Date.ToString("yyyyMMdd") + ", ");
            sql.Append("'" + hed.T001RentCont + "', ");
            sql.Append("'" + hed.T001RentUser + "', ");
            string comment = "";
            if (!CheckValues.IsEmpty(RentcMnt.Text))
            {
                comment = Converter.GetMulitiLineText(RentcMnt);
            }
            sql.Append("\"" + comment + "\", ");
            sql.Append(" now() , now() ");
            sql.Append(")");

            conn.ExecuteNonQuery(sql.ToString());

        }


       
        private bool InsertDtl(T0002RentDtl dtl)
        {

            StringBuilder sql;
            string table = "t002_rent_dtl";
            sql = new StringBuilder();
            sql.Append("SELECT `T002_RENTSEQ` FROM " + table + " WHERE T002_RENTNO = " +rentNo+ " ORDER BY `T002_RENTSEQ` DESC LIMIT 1");
            DatabaseConnector conn = new DatabaseConnector();
            DataTable dataTable = conn.Result(table, sql.ToString(), table).Tables[table];
            if (dataTable.Rows.Count is 1)
            {
                dtlNo = Convert.ToInt32(dataTable.Rows[0][0]) + 1;
            }
            //1回で100件の明細を登録するとエラー表示
            else if(dataTable.Rows.Count is 99)
            {
                //MAX
                MessageBox.Show(
                    "一度に貸出可能な物品は99件までです", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                message.Text = DefaultMessageHeader + "一度に貸出可能な物品は99件までです。";
                return false;
            }
            else
            {
                dtlNo = 1;
            }
            //sqlの結果が０か１
            sql = new StringBuilder();
            sql.Append("SELECT `T002_BUPNCD` FROM `t002_rent_dtl` WHERE `T002_BUPNCD` =" + dtl.T002BupnCd + " and T002_RENTNO =" +rentNo.ToString());
            DataSet dataSet = conn.Result(table, sql.ToString(), table);
            //SELECT結果が０件なら登録できる。
            if (CheckValues.IsEmpty(dataSet, table))
            {
                sql = new StringBuilder();
                sql.Append("INSERT INTO `t002_rent_dtl`(`T002_RENTNO`, `T002_RENTSEQ`, `T002_BUPNCD`, `T002_BUPNCND`, `T002_RETRNPLAN`, `T002_RENTLCAT1`,  `T002_INSDATE`, `T002_INSTIME`) VALUES ( ");
                sql.Append("" + rentNo + ", ");
                sql.Append("" + dtlNo + ", ");
                sql.Append("" + dtl.T002BupnCd + ", ");
                sql.Append("\"" + dtl.T002BupnCnd + " \", ");
                sql.Append("" + dtl.T002RetrnPlan.Date.ToString("yyyyMMdd") + ", ");
                sql.Append("\"" + dtl.T002RentLcat2 + "\", ");
                sql.Append(" now() , now() ");
                sql.Append(")");

                conn.ExecuteNonQuery(sql.ToString());

                sql = new StringBuilder();
                sql.Append("UPDATE M001_BUPPIN SET ");
                sql.Append("M001_BUPNRENT = " + bupnrent );
                sql.Append(" WHERE M001_BUPNCD = " + dtl.T002BupnCd);
                conn.ExecuteNonQuery(sql.ToString());

                SetDtlToDgv(conn, table);
            }
            else
            {
                MessageBox.Show(
                    "この物品は貸出中です。", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                message.Text = DefaultMessageHeader + "この物品は貸出中です。";
                return false;
            }

            hasRent = true;
            return true;
        }

        private void SetDtlToDgv(DatabaseConnector conn, string table)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT t002_rent_dtl.T002_RENTSEQ,t002_rent_dtl.T002_BUPNCD,m001_buppin.M001_BUPNNM,");
            sql.Append("t002_rent_dtl.T002_BUPNCND,t001_rent_hed.T001_RENTCONT,t001_rent_hed.T001_RENTUSER,t001_rent_hed.T001_RENTDATE,t002_rent_dtl.T002_RETRNPLAN");
            sql.Append(",m001_buppin.M001_BUPNLCAT2, m001_buppin.m001_BUPNLCAT1,T001_RENT_HED.T001_RENTCMNT FROM " + table);
            sql.Append(" INNER JOIN `t001_rent_hed` ON `t002_rent_dtl`.`T002_RENTNO` = `t001_rent_hed`.`T001_RENTNO`");
            sql.Append(" INNER JOIN `m001_buppin` ON `t002_rent_dtl`.`T002_BUPNCD` = `m001_buppin`.`M001_BUPNCD`");
            sql.Append(" WHERE t002_rent_dtl.T002_RENTNO = " + rentNo);
            sql.Append(" ORDER BY t002_rent_dtl.T002_RENTSEQ ASC");

            DataSet dataSet = conn.Result(table, sql.ToString(), table);


            rentList.DataSource = dataSet.Tables[table];

            if (rentList.Rows.Count != 0)
            {
                rentList.Rows[0].Selected = true;

            }

            //ヘッダーのカラムの幅
            rentList.Columns[0].Width = 40;//明細番号
            rentList.Columns[0].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            rentList.Columns[1].Width = 90;//物品コード
            rentList.Columns[1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            rentList.Columns[2].Width = 160;//物品名
            rentList.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            rentList.Columns[3].Width = 160;//状態(付属品)
            rentList.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            rentList.Columns[4].Width = 150;//借受者名
            rentList.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            rentList.Columns[5].Width = 150;//利用者名
            rentList.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            rentList.Columns[6].Width = 80;//貸出日
            rentList.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            rentList.Columns[7].Width = 80;//返却予定日
            rentList.Columns[7].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            rentList.Columns[8].Width = 130;//保管場所(出)
            rentList.Columns[8].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            rentList.Columns[9].Width = 200;//備考
            rentList.Columns[9].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            headerText = new string[] { "No", "物品コード","物品名", "状態(付属品)","借受者","利用者","貸出日","返却予定日","現在保管場所", "返却場所","備考"};

            for (int count = 0; count < headerText.Length; count++)
            {
                rentList.Columns[count].HeaderText = headerText[count];
            }
            
           // RemoveDataSetColumns(dataSet, table);
        }


        private T0001RentHed GetRentHed()
        {
            T0001RentHed hed = new T0001RentHed();

            //物品種別の空白チェック兼
            if (CheckValues.IsM001Kind(tbBuppinKind.Text))
            {
                hed.T001bupnKnd = System.Convert.ToInt32(tbBuppinKind.Text);
            }
            else
            {
                return null;
            }

            if (!CheckValues.IsEmpty(tbRentCont.Text))
            {
                hed.T001RentCont = tbRentCont.Text;
            }
            else
            {
                return null;
            }

            if (!CheckValues.IsEmpty(tbRentUser.Text))
            {
                hed.T001RentUser = tbRentUser.Text;
            }
            else
            {
                return null;
            }

            DateTime? date = CheckValues.IsDateTime(tbYear.Text, tbMonth.Text, tbDay.Text);
            if (date != null)
            {
                hed.T001RentDate = (DateTime) date;
            }
            else
            {
                return null;
            }

            return hed;
        }

        private T0002RentDtl GetDtl()
        {
            T0002RentDtl dtl = new T0002RentDtl();

            if (CheckValues.IsNumber(tbBuppinCode.Text))
            {
                dtl.T002BupnCd = Convert.ToInt32(tbBuppinCode.Text);
            }
            else
            {
                return null;
            }

            DateTime? date = CheckValues.IsDateTime(tbPlanYear.Text, tbPlanMonth.Text, tbPlanDay.Text);

            if (date != null)
            {
                dtl.T002RetrnPlan = (DateTime)date;
            }
            else
            {
                return null;
            }


            if (!CheckValues.IsEmpty(tbLcat2.Text))
            {
                dtl.T002RentLcat2 = tbLcat2.Text;
            }
            else
            {
                return null;
            }

            return dtl;
        }

        public void SetKindReadOnly()
        {
            tbBuppinKind.ReadOnly = true;
            tbBuppinKind.BackColor = SystemColors.Control;
        }

        public void SetBuppinCode(string code)
        {
            tbBuppinCode.Text = code;
        }

        public void SetBuppinName(string name)
        {
            lbBuppinName.Text = name;
        }

        public void SetBuppinLcat1(string lcat)
        { 
            tbLcat1.Text = lcat;
        }

        public void SetBuppinLcat2(string lcat)
        {
            tbLcat2.Text = lcat;
        }

        public void SetBuppinCond(string cond)
        {
            tbCnd.Text = cond;
        }

        private void FoundKeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox box = (TextBox)sender;
            if (e.KeyChar != (Char)Keys.Back && e.KeyChar != (Char)Keys.Enter && e.KeyChar != (Char)Keys.Escape)
            {
                CheckTypeValues(box, e);
            }

        }

        private void CheckTypeValues(TextBox box, KeyPressEventArgs e)
        {

            switch (box.Name)
            {
                case "tbBuppinKind":
                    ValueCheck(box, e, "物品種別は１または２のみ入力可能です。", "kind");
                    break;
                case "tbBuppinCode":
                    ValueCheck(box, e, "物品コードは数値のみ入力可能です。", "code");
                    break;
                case "tbYear":
                    ValueCheck(box, e, "日付は数値のみ入力可能です。", "year");
                    break;
                case "tbMonth":
                    ValueCheck(box, e, "日付は数値のみ入力可能です。", "month");
                    break;
                case "tbDay":
                    ValueCheck(box, e, "日付は数値のみ入力可能です。", "day");
                    break;
                case "tbPlanYear":
                    ValueCheck(box, e, "日付は数値のみ入力可能です。", "planYear");
                    break;
                case "tbPlanMonth":
                    ValueCheck(box, e, "日付は数値のみ入力可能です。", "planMonth");
                    break;
                case "tbPlanDay":
                    ValueCheck(box, e, "日付は数値のみ入力可能です。", "planDay");
                    break;

                default:
                    break;
            }
        }

        private void ValueCheck(TextBox box, KeyPressEventArgs e, string message, string mode)
        {
            switch (mode)
            {
                case "kind":

                    if (CheckValues.IsM001Kind(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }       
                    break;

                case "code":
                    if (CheckValues.IsNumber(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;

                case "year":
                    if (CheckValues.IsNumber(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;

                case "month":
                    if (CheckValues.IsNumber(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;

                case "day":
                    if (CheckValues.IsNumber(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;

                case "planYear":
                    if (CheckValues.IsNumber(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;

                case "planMonth":
                    if (CheckValues.IsNumber(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;

                case "planDay":
                    if (CheckValues.IsNumber(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;



                default:
                    break;

            }
        }

       
        private void SetDefaultTheme(TextBox box)
        {
            box.BackColor = DefaultColor;
            message.Text = DefaultMessageHeader + defaultMessage;
            FormSettings.TabStopTrigger(this, true);
        }

        private void SetErrorTheme(TextBox box, KeyPressEventArgs e, String errorMessage)
        {
            e.Handled = true;
            box.BackColor = ErrorColor;
            message.Text = DefaultMessageHeader + errorMessage;
            FormSettings.TabStopTrigger(this, false);
        }

        private void TbPlanYear_TextChanged(object sender, EventArgs e)
        {
            if (!errorDate)
            {
                return;
            }

            string year = tbYear.Text;
            if (year is "")
            {
                return;
            }
            string month = tbMonth.Text;
            if (month is "")
            {
                return;
            }
            string day = tbMonth.Text;
            if (day is "")
            {
                return;
            }

            DateTime date = new DateTime();
            if(CheckValues.IsDateTime(year, month, day) is DateTime)
            {
                date = (DateTime)CheckValues.IsDateTime(year, month, day);
            }
            else
            {
                return;
            }

            string newYear = tbPlanYear.Text;
            if (newYear is "")
            {
                return;
            }
            string newMonth = tbPlanMonth.Text;
            if (newMonth is "")
            {
                return;
            }
            string newDay = tbPlanDay.Text;
            if (newDay is "")
            {
                return;
            }

            DateTime newDate = new DateTime();
            if (CheckValues.IsDateTime(year, month, day) is DateTime)
            {
                newDate = (DateTime)CheckValues.IsDateTime(newYear, newMonth, newDay);
            }
            else

            {
                return;
            }

            Color color = Color.White;
            if (date.CompareTo(newDate) is -1)
            {
                tbYear.BackColor = color;
                tbMonth.BackColor = color;
                tbDay.BackColor = color;
                tbPlanYear.BackColor = color;
                tbPlanMonth.BackColor = color;
                tbPlanDay.BackColor = color;

                errorDate = false;
            }

        }

        private void MoveSelectRow(Keys key)
        {
            if (rentList.Rows.Count is 0)
            {
                return;
            }

            int rowIndex = 0;
            try
            {
                rowIndex = rentList.CurrentRow.Index;

            }
            catch
            {
                rentList.Rows[rowIndex].Selected = true;
                rentList.CurrentCell = rentList[0, rowIndex];
            }

            switch (key)
            {
                case Keys.F3:
                    if (rowIndex != 0)
                    {
                        rentList.Rows[rowIndex - 1].Selected = true;
                        rentList.Rows[rowIndex].Selected = false;
                        rentList.CurrentCell = rentList[0, rowIndex - 1];
                    }
                    return;
                case Keys.F4:
                    if (rowIndex + 1 != rentList.Rows.Count)
                    {
                        rentList.Rows[rowIndex].Selected = false;
                        rentList.Rows[rowIndex + 1].Selected = true;
                        rentList.CurrentCell = rentList[0, rowIndex + 1];
                    }
                    return;
            }
        }
    }
}
