﻿using BuppinRent.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuppinRent

/*
*  Created By HAL OSAKA Katsuya Akamatsu on 2017/10/3 TUE
*  フォームレイアウト
*/

{
    public partial class IFormDesign : Form
    {
        protected Label title;
        protected Label message;

        public Color DefaultColor = Color.White;
        public Color ErrorColor = Color.LightPink;
        public Color DefaltFontColor = Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
        public Color ErrorFontColor = Color.Red;

        protected Button f1;
        protected Button f2;
        protected Button f3;
        protected Button f4;
        protected Button f5;
        protected Button f6;
        protected Button f7;
        protected Button f8;
        protected Button f9;
        protected Button f10;
        protected Button f11;
        protected Button f12;

        public IFormDesign()
        {
            InitializeComponent();
            OnCreate();
        }

        protected virtual void BtnF1_Click(object sender, EventArgs e)
        {
            return;
        }

        protected virtual void BtnF2_Click(object sender, EventArgs e)
        {
            return;
        }

        protected virtual void BtnF3_Click(object sender, EventArgs e)
        {
            return;
        }

        protected virtual void BtnF4_Click(object sender, EventArgs e)
        {
            return;
        }

        protected virtual void BtnF5_Click(object sender, EventArgs e)
        {
            return;
        }

        protected virtual void BtnF6_Click(object sender, EventArgs e)
        {
            return;
        }

        protected virtual void BtnF8_Click(object sender, EventArgs e)
        {
            return;
        }

        protected virtual void BtnF9_Click(object sender, EventArgs e)
        {
            return;
        }

        protected virtual void BtnF10_Click(object sender, EventArgs e)
        {
            return;
        }

        /*
         * 終了ボタンをクリックすると閉じる
         */
        protected virtual void BtnF11_Click(object sender, EventArgs e)
        {
            this.Dispose();
            this.Close();
        }

        protected virtual void BtnF12_Click(object sender, EventArgs e)
        {

        }

        /*
         * キー入力された時の処理
         */
        protected virtual void FoundKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F11)
            {
                BtnF11_Click(null, null);
            }
        }

        private void OnCreate()
        {
            SetText();
            SetFunctionList();
            SetFunctionText();
            SetFunctionOptions();
        }

        private void SetText()
        {
            title = lbTitle;
            message = lbMessage;
        }
        private void SetFunctionList()
        {
            f1 = btnF1;
            f2 = btnF2;
            f3 = btnF3;
            f4 = btnF4;
            f5 = btnF5;
            f6 = btnF6;
            f7 = btnF7;
            f8 = btnF8;
            f9 = btnF9;
            f10 = btnF10;
            f11 = btnF11;
            f11.BackColor = SystemColors.Control;
            f12 = btnF12;
        }

        private void SetFunctionText()
        {
            f1.Text = "\n[F1]";
            f2.Text = "\n[F2]";
            f3.Text = "\n[F3]";
            f4.Text = "\n[F4]";
            f5.Text = "\n[F5]";
            f6.Text = "\n[F6]";
            f7.Text = "\n[F7]";
            f8.Text = "\n[F8]";
            f9.Text = "\n[F9]";
            f10.Text = "\n[F10]";
            f11.Text = "終了\n[F11]";
            f12.Text = "\n[F12]";
        }

        private void SetFunctionOptions()
        {
            FormSettings.EnabledFunction(f1, false);
            FormSettings.EnabledFunction(f2, false);
            FormSettings.EnabledFunction(f3, false);
            FormSettings.EnabledFunction(f4, false);
            FormSettings.EnabledFunction(f5, false);
            FormSettings.EnabledFunction(f6, false);
            FormSettings.EnabledFunction(f7, false);
            FormSettings.EnabledFunction(f8, false);
            FormSettings.EnabledFunction(f9, false);
            FormSettings.EnabledFunction(f10, false);
            FormSettings.EnabledFunction(f12, false);
        }
    }
}
