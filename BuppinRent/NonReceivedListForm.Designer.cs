﻿namespace BuppinRent
{
    partial class NonReceivedListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbBuppinGroup = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbYear = new System.Windows.Forms.TextBox();
            this.tbMonth = new System.Windows.Forms.TextBox();
            this.tbDay = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label3.Location = new System.Drawing.Point(122, 186);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 28);
            this.label3.TabIndex = 22;
            this.label3.Text = "返却予定日";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label4.Location = new System.Drawing.Point(179, 242);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 28);
            this.label4.TabIndex = 26;
            this.label4.Text = "分類";
            // 
            // tbBuppinGroup
            // 
            this.tbBuppinGroup.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbBuppinGroup.Location = new System.Drawing.Point(238, 239);
            this.tbBuppinGroup.MaxLength = 1;
            this.tbBuppinGroup.Name = "tbBuppinGroup";
            this.tbBuppinGroup.Size = new System.Drawing.Size(26, 36);
            this.tbBuppinGroup.TabIndex = 4;
            this.tbBuppinGroup.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label5.Location = new System.Drawing.Point(386, 185);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 28);
            this.label5.TabIndex = 28;
            this.label5.Text = "/";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.label6.Location = new System.Drawing.Point(267, 242);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(587, 28);
            this.label6.TabIndex = 30;
            this.label6.Text = "0:全て 1:本体 2:モニタ 3:プリンター 4:モデム 5:ルーター 6:ハブ";
            // 
            // tbYear
            // 
            this.tbYear.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbYear.Location = new System.Drawing.Point(271, 182);
            this.tbYear.MaxLength = 2;
            this.tbYear.Name = "tbYear";
            this.tbYear.Size = new System.Drawing.Size(52, 36);
            this.tbYear.TabIndex = 31;
            this.tbYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbYear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // tbMonth
            // 
            this.tbMonth.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbMonth.Location = new System.Drawing.Point(338, 182);
            this.tbMonth.MaxLength = 2;
            this.tbMonth.Name = "tbMonth";
            this.tbMonth.Size = new System.Drawing.Size(52, 36);
            this.tbMonth.TabIndex = 32;
            this.tbMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbMonth.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // tbDay
            // 
            this.tbDay.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbDay.Location = new System.Drawing.Point(405, 182);
            this.tbDay.MaxLength = 2;
            this.tbDay.Name = "tbDay";
            this.tbDay.Size = new System.Drawing.Size(52, 36);
            this.tbDay.TabIndex = 33;
            this.tbDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label13.Location = new System.Drawing.Point(485, 60);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 28);
            this.label13.TabIndex = 35;
            this.label13.Text = "～";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label7.Location = new System.Drawing.Point(320, 186);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 28);
            this.label7.TabIndex = 28;
            this.label7.Text = "/";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label8.Location = new System.Drawing.Point(238, 186);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 28);
            this.label8.TabIndex = 28;
            this.label8.Text = "20";
            // 
            // NonReceivedListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 1024);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.tbDay);
            this.Controls.Add(this.tbMonth);
            this.Controls.Add(this.tbYear);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbBuppinGroup);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Name = "NonReceivedListForm";
            this.Text = "NonReceivedListForm";
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.tbBuppinGroup, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.tbYear, 0);
            this.Controls.SetChildIndex(this.tbMonth, 0);
            this.Controls.SetChildIndex(this.tbDay, 0);
            this.Controls.SetChildIndex(this.label13, 0);
            this.Controls.SetChildIndex(this.title, 0);
            this.Controls.SetChildIndex(this.message, 0);
            this.Controls.SetChildIndex(this.f11, 0);
            this.Controls.SetChildIndex(this.f1, 0);
            this.Controls.SetChildIndex(this.f2, 0);
            this.Controls.SetChildIndex(this.f3, 0);
            this.Controls.SetChildIndex(this.f4, 0);
            this.Controls.SetChildIndex(this.f5, 0);
            this.Controls.SetChildIndex(this.f6, 0);
            this.Controls.SetChildIndex(this.f7, 0);
            this.Controls.SetChildIndex(this.f8, 0);
            this.Controls.SetChildIndex(this.f9, 0);
            this.Controls.SetChildIndex(this.f10, 0);
            this.Controls.SetChildIndex(this.f12, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbBuppinGroup;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbYear;
        private System.Windows.Forms.TextBox tbMonth;
        private System.Windows.Forms.TextBox tbDay;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}