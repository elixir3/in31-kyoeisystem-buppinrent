﻿namespace BuppinRent.CrystalReports
{
    partial class NonReceivedListPrint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.crystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.NonReceivedListReport1 = new BuppinRent.CrystalReports.NonReceivedListReports();
            this.SuspendLayout();
            // 
            // title
            // 
            this.title.Enabled = false;
            this.title.Visible = false;
            // 
            // message
            // 
            this.message.Enabled = false;
            this.message.Visible = false;
            // 
            // f1
            // 
            this.f1.Location = new System.Drawing.Point(12, 925);
            this.f1.Size = new System.Drawing.Size(76, 96);
            // 
            // f2
            // 
            this.f2.Location = new System.Drawing.Point(94, 925);
            this.f2.Size = new System.Drawing.Size(76, 96);
            // 
            // f3
            // 
            this.f3.Location = new System.Drawing.Point(178, 925);
            this.f3.Size = new System.Drawing.Size(76, 96);
            // 
            // f4
            // 
            this.f4.Location = new System.Drawing.Point(260, 925);
            this.f4.Size = new System.Drawing.Size(76, 96);
            // 
            // f5
            // 
            this.f5.Location = new System.Drawing.Point(342, 925);
            this.f5.Size = new System.Drawing.Size(76, 96);
            // 
            // f6
            // 
            this.f6.Location = new System.Drawing.Point(424, 925);
            this.f6.Size = new System.Drawing.Size(76, 96);
            // 
            // f7
            // 
            this.f7.Location = new System.Drawing.Point(506, 925);
            this.f7.Size = new System.Drawing.Size(76, 96);
            // 
            // f8
            // 
            this.f8.Location = new System.Drawing.Point(588, 925);
            this.f8.Size = new System.Drawing.Size(76, 96);
            // 
            // f9
            // 
            this.f9.Location = new System.Drawing.Point(670, 925);
            this.f9.Size = new System.Drawing.Size(76, 96);
            this.f9.TabIndex = 5;
            // 
            // f10
            // 
            this.f10.Location = new System.Drawing.Point(752, 925);
            this.f10.Size = new System.Drawing.Size(76, 96);
            this.f10.TabIndex = 6;
            // 
            // f11
            // 
            this.f11.Location = new System.Drawing.Point(834, 925);
            this.f11.Size = new System.Drawing.Size(76, 96);
            this.f11.TabIndex = 7;
            // 
            // f12
            // 
            this.f12.Location = new System.Drawing.Point(916, 925);
            this.f12.Size = new System.Drawing.Size(76, 96);
            this.f12.TabIndex = 8;
            // 
            // crystalReportViewer1
            // 
            this.crystalReportViewer1.ActiveViewIndex = 0;
            this.crystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default;
            this.crystalReportViewer1.Location = new System.Drawing.Point(0, 0);
            this.crystalReportViewer1.Name = "crystalReportViewer1";
            this.crystalReportViewer1.ReportSource = this.NonReceivedListReport1;
            this.crystalReportViewer1.Size = new System.Drawing.Size(1000, 922);
            this.crystalReportViewer1.TabIndex = 0;
            this.crystalReportViewer1.Load += new System.EventHandler(this.CrystalReportViewer1_Load);
            // 
            // NonReceivedListPrint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 1024);
            this.Controls.Add(this.crystalReportViewer1);
            this.Name = "NonReceivedListPrint";
            this.Text = "NonReceivedListPrint";
            this.Controls.SetChildIndex(this.title, 0);
            this.Controls.SetChildIndex(this.message, 0);
            this.Controls.SetChildIndex(this.f11, 0);
            this.Controls.SetChildIndex(this.f1, 0);
            this.Controls.SetChildIndex(this.f2, 0);
            this.Controls.SetChildIndex(this.f3, 0);
            this.Controls.SetChildIndex(this.f4, 0);
            this.Controls.SetChildIndex(this.f5, 0);
            this.Controls.SetChildIndex(this.f6, 0);
            this.Controls.SetChildIndex(this.f7, 0);
            this.Controls.SetChildIndex(this.f8, 0);
            this.Controls.SetChildIndex(this.f9, 0);
            this.Controls.SetChildIndex(this.f10, 0);
            this.Controls.SetChildIndex(this.f12, 0);
            this.Controls.SetChildIndex(this.crystalReportViewer1, 0);
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer1;
        private NonReceivedListReports NonReceivedListReport1;
    }
}