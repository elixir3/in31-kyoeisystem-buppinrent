﻿using BuppinRent.Util;
using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuppinRent.CrystalReports
{
    public partial class NonReceivedListPrint : IFormDesign
    {
        private DataSet param;
        private string[] keywords;
        private NonReceivedListReports reports;


        public NonReceivedListPrint(DataSet param, string[] keywords)
        {
            if (param is DataSet)
            {
                this.param = param;
                this.keywords = keywords;
            }

            InitializeComponent();

            FormSettings.EnabledFunction(f9, true, "印刷");
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    BtnF9_Click(null, null);
                    break;
            }
            base.OnKeyDown(e);
        }

        protected override void BtnF9_Click(object sender, EventArgs e)
        {
            crystalReportViewer1.PrintReport();
        }
        private void CrystalReportViewer1_Load(object sender, EventArgs e)
        {
            reports = new NonReceivedListReports();
            reports.SetDataSource(param);
            SetText();
            crystalReportViewer1.ReportSource = reports;
            crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
        }

        private void SetText()
        {
            TextObject textKind = (TextObject)reports.ReportDefinition.ReportObjects["tbKind"];
            switch (keywords[0])
            {
                case "1":
                    textKind.Text = "書籍";
                    break;
                case "2":
                    textKind.Text = "機器";
                    break;
                default:
                    textKind.Text = "すべて";
                    break;
            }

            TextObject textDate = (TextObject)reports.ReportDefinition.ReportObjects["txDate"];
            textDate.Text = keywords[1] + " ～ " + keywords[2];

            TextObject textGroup = (TextObject)reports.ReportDefinition.ReportObjects["tbGroup"];
            switch (keywords[3])
            {
                case "1":
                    textGroup.Text = "本体";
                    break;
                case "2":
                    textGroup.Text = "モニター";
                    break;
                case "3":
                    textGroup.Text = "プリンター";
                    break;
                case "4":
                    textGroup.Text = "モデム";
                    break;
                case "5":
                    textGroup.Text = "ルータ";
                    break;
                case "6":
                    textGroup.Text = "ハブ";
                    break;
                default:
                    textGroup.Text = "すべて";
                    break;
            }
        }
    }
}
