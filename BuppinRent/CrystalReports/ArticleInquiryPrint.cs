﻿using BuppinRent.CrystalReports;
using BuppinRent.Util;
using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuppinRent
{
    public partial class ArticleInquiryPrint : IFormDesign
    {
        private DataSet param;
        private string[] keywords;
        private ArticleInquiryReports reports;

        public ArticleInquiryPrint(DataSet param, string[] keywords)
        {
            if (param is DataSet)
            {
                this.param = param;
                this.keywords = keywords;
            }

            InitializeComponent();

            FormSettings.EnabledFunction(f9, true, "印刷");
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    BtnF9_Click(null, null);
                    break;
            }
            base.OnKeyDown(e);
        }

        protected override void BtnF9_Click(object sender, EventArgs e)
        {
            crystalReportViewer1.PrintReport();
        }

        private void Crs_Load(object sender, EventArgs e)
        {
            reports = new ArticleInquiryReports();
            reports.SetDataSource(param);
            SetText();
            crystalReportViewer1.ReportSource = reports;
            crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
        }

        private void SetText()
        {
            TextObject textKind = (TextObject)reports.ReportDefinition.ReportObjects["txKind"];
            switch (keywords[0])
            {
                case "1":
                    textKind.Text = "書籍";
                    break;
                case "2":
                    textKind.Text = "機器";
                    break;
                default:
                    textKind.Text = "全て";
                    break;
            }

            TextObject textRent = (TextObject)reports.ReportDefinition.ReportObjects["txRent"];
            switch (keywords[1])
            {
                case "2":
                    textRent.Text = "貸出中";
                    break;
                case "3":
                    textRent.Text = "未貸出";
                    break;
                default:
                    textRent.Text = "全て";
                    break;
            }

            TextObject textDelete = (TextObject)reports.ReportDefinition.ReportObjects["txDel"];
            switch (keywords[2])
            {
                case "2":
                    textDelete.Text = "含む";
                    break;
                default:
                    textDelete.Text = "含まない";
                    break;
            }
        }
    }
}
