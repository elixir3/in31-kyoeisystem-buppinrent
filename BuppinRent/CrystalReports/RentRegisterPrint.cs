﻿using BuppinRent.Util;
using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuppinRent.CrystalReports
{
    public partial class RentRegisterPrint : IFormDesign
    {

        private DataSet param;
        private string[] keywords;
        private RentRegisterReport reports;


        public RentRegisterPrint(DataSet param, string[] keywords)
        {
            if (param is DataSet)
            {
                this.param = param;
                this.keywords = keywords;
            }

            InitializeComponent();

            FormSettings.EnabledFunction(f9, true, "印刷");
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    BtnF9_Click(null, null);
                    break;
            }
            base.OnKeyDown(e);
        }

        protected override void BtnF9_Click(object sender, EventArgs e)
        {
            crystalReportViewer1.PrintReport();
        }

        private void CrystalReportViewer1_Load(object sender, EventArgs e)
        {
            reports = new RentRegisterReport();
            reports.SetDataSource(param);
            SetText();
            crystalReportViewer1.ReportSource = reports;
            crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
        }

        private void SetText()
        {
            TextObject textNo = (TextObject)reports.ReportDefinition.ReportObjects["txRentNo"];
            TextObject txRentDate = (TextObject)reports.ReportDefinition.ReportObjects["txRentDate"];
            TextObject txCont = (TextObject)reports.ReportDefinition.ReportObjects["txCont"];
            TextObject txUser = (TextObject)reports.ReportDefinition.ReportObjects["txUser"];
            TextObject txRentComment = (TextObject)reports.ReportDefinition.ReportObjects["txRentComment"];
            textNo.Text = keywords[0];
            txRentDate.Text = keywords[1];
            txCont.Text = keywords[2];
            txUser.Text = keywords[3];
            txRentComment.Text = keywords[4];
        }
    }
}
