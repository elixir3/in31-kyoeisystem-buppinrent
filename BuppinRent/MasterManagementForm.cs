﻿using BuppinRent.ColumnsObj;
using BuppinRent.Database;
using BuppinRent.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuppinRent

/*  
*  Created By HAL OSAKA Katsuya Akamatsu on 2017/10/3 TUE
*  F001_BUPPIN_MNT 貸し出す物品を登録変更削除するためのフォーム
*/

{
    public partial class MasterManagementForm : IFormDesign
    {
        private string DefaultTitle = "物品マスタ登録";
        private string DefaultMessageHeader = "[メッセージ]  ";
        private string DefaultMessage = "物品マスタの登録・変更及び削除が行えます";

        private int dbAction = 0; //0 insert 1 update
        private string  del =  "9";
      //  private int index = -1;
        //private List<M001Buppin> list = null;

        public MasterManagementForm()
        {
            OnCreate();
            InitializeComponent();
        }

        private void OnCreate()
        {
            title.Text = DefaultTitle;
            message.Text = DefaultMessageHeader + DefaultMessage;

            //add oginoshota 2017/10/06 FRI
            FormSettings.EnabledFunction(f1, true, "検索");
            FormSettings.EnabledFunction(f2, true, "表示");
            FormSettings.EnabledFunction(f8, true, "登録");
            FormSettings.EnabledFunction(f10, true, "取消");
            FormSettings.EnabledFunction(f12, true, "削除");
        }

        protected override void FoundKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    BtnF1_Click(null, null);
                    break;
                case Keys.F2:
                    BtnF2_Click(null, null);
                    break;
                case Keys.F8:
                    f8_Click(null, null);
                    return;
                case Keys.F10:
                    BtnF10_Click(null, null);
                    return;
                case Keys.F12:
                    BtnF12_Click(null, null);
                    return;
            }

            base.FoundKeyDown(sender, e);
        }

        //add oginoshota 2017/10/06 F1の検索ボタンが押された時の処理
        protected override void BtnF1_Click(object sender, EventArgs e)
        {
            using (ModalStyle modal = new ModalStyle(this, null, ModalStyle.Mode.ModeSearch))
            {
                modal.Owner = this;
                modal.ShowDialog();
            }
        }

        protected override void BtnF2_Click(object sender, EventArgs e)
        {
          
           Display_Click(null,null);
        }
        //add oginoshota 2017/10/6

        protected override void BtnF10_Click(object sender, EventArgs e)
        {
            FormSettings.ModeCancel(this, DefaultColor);
            message.Text = DefaultMessageHeader + DefaultMessage;
            title.Text = DefaultTitle;
            dbAction = 0;
            tbBuppinCode.ReadOnly = false;
            SetCustomTabStopOption();
        }
        protected override void BtnF12_Click(object sender, EventArgs e)
        {

            if (dbAction == 1)
            {
                string messageBox = "削除しますか？";
                string titleHed = "削除";
                DialogResult result = MessageBox.Show(
                    messageBox, titleHed, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    Delete();
                    tbDeleteFlag.Text = "9";
                    FormSettings.ModeCancel(this, DefaultColor);
                    dbAction = 0;
                    tbBuppinCode.ReadOnly = false;
                    message.Text = DefaultMessageHeader + "削除しました。";
                    return;
                }
                tbBuppinCode.ReadOnly = true;

            }
        }

        private void Submit()
        {
            if (tbBuppinKind.Text == "1")
            {
                tbBuppinGroup.Text = "0";
            }

            M001Buppin buppin = new M001Buppin();
            buppin = GetBuppin();

            if (buppin == null)
            {
                MessageBox.Show(
                    "入力された値が不正です", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                message.Text = DefaultMessageHeader + "入力された値が不正です";
                return;
            }

            if (Execute(buppin))
            {
                ClearTextBox();
            }


        }

        private M001Buppin GetBuppin()
        {        
            M001Buppin buppin = new M001Buppin();

            if (!CheckValues.IsEmpty(tbBuppinName.Text))
            {
                buppin.M001BupnNm = tbBuppinName.Text;
            }
            else
            {
                return null;
            }
            

            if (CheckValues.IsM001Kind(tbBuppinKind.Text))
            {
                buppin.M001BupnKnd = Convert.ToInt32(tbBuppinKind.Text);
            }
            else
            {
                return null;
            }

            if (CheckValues.IsNumber(tbBuppinGroup.Text))
            {
                buppin.M001BupnGrp = Convert.ToInt32(tbBuppinGroup.Text);
            }
            else
            {
                return null;
            }

            if (!CheckValues.IsEmpty(tbBuppinLocation1.Text))
            {
                buppin.M001BupnLcat1 = tbBuppinLocation1.Text;
            }
            else
            {
                return null;
            }

            if (!CheckValues.IsEmpty(tbLcat2.Text))
            {
                buppin.M001BupnLcat2 = tbLcat2.Text;
            }
            else
            {
                return null;
            }

            if (CheckValues.IsNumber(tbBuppinRentFlag.Text))
            {
                buppin.M001BupnRent = Convert.ToInt32(tbBuppinRentFlag.Text);
            }
            else
            {
                return null;
            }

            if (CheckValues.IsNumber(tbDeleteFlag.Text))
            {
                buppin.M001BupnDel = Convert.ToInt32(tbDeleteFlag.Text);
            }
            else
            {

                return null;
            }

           
            if (!CheckValues.IsEmpty(tbCondition.Text))
            {
                buppin.M001BupnCnd = tbCondition.Text;
            }

            if (!CheckValues.IsEmpty(tbDeleteReason.Text))
            {
                buppin.M001BupnRrsn = tbDeleteReason.Text;
            }

            return buppin;
        }

        private void FoundKeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox box = (TextBox)sender;
            if (e.KeyChar != (Char) Keys.Back && e.KeyChar != (Char) Keys.Enter && e.KeyChar != (Char) Keys.Escape)   
            {
                CheckTypeValues(box, e);
            }

        }

        private void CheckTypeValues(TextBox box, KeyPressEventArgs e)
        {
           
            switch (box.Name)
            {
                case "tbBuppinCode":
                    ValueCheck(box, e , "物品コードは数値のみ入力可能です", "code");
                    break;
                case "tbBuppinKind":
                    ValueCheck(box, e, "物品種別は１または２のみ入力可能です", "kind");
                    break;
                case "tbBuppinGroup":
                    ValueCheck(box, e , "分類コードは０から６のみ入力可能です", "group");
                    break;
                case "tbBuppinRentFlag":
                    ValueCheck(box, e, "貸出フラグは０または９入力可能です", "flag");
                    break;
                case "tbDeleteFlag":
                    ValueCheck(box, e, "削除フラグは０または９可能です", "flag");
                    break;
                default:
                    break;
            }
        }

        private void ValueCheck(TextBox box, KeyPressEventArgs e, string message, string mode)
        {
            switch (mode)
            {
                case "kind":

                    if (CheckValues.IsM001Kind(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;

                case "group":

                    if (CheckValues.IsM001Group(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;

                case "flag":

                    if (CheckValues.IsM001Flag(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;
                    
                case "code":

                    if (CheckValues.IsNumber(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;

            }
        }

        private void SetCustomTabStopOption()
        {
            //btnLeft.TabStop = false;
            //btnRight.TabStop = false;
            f8.TabStop = true;
            f10.TabStop = true;
            f11.TabStop = true;
        }
        private void SetDefaultTheme(TextBox box)
        {
            box.BackColor = DefaultColor;
            message.Text = DefaultMessageHeader + DefaultMessage;
            FormSettings.TabStopTrigger(this, true);
            SetCustomTabStopOption();
        }

        private void SetErrorTheme(TextBox box, KeyPressEventArgs e, String errorMessage)
        {
            e.Handled = true;
            box.BackColor = ErrorColor;
            message.Text = DefaultMessageHeader + errorMessage;
            FormSettings.TabStopTrigger(this, false);
        }
        
        private bool Execute (M001Buppin buppin)
        {

            if (!CheckValues.IsNumber(tbBuppinCode.Text))
            {
                MessageBox.Show(
                    "物品コードは数値のみ入力可能です", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                message.Text = DefaultMessageHeader + "物品コードは数値のみ入力可能です";
                return false;
            }

            switch (dbAction)
            {
                case 0:
                    if (CodeIsExist())
                    {
                        MessageBox.Show(
                   "既に存在するコードです", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        message.Text = DefaultMessageHeader + "既に存在するコードです";
                        return false;
                    }
                    else
                    {
                        buppin.M001BupnCd = Convert.ToInt32(tbBuppinCode.Text);
                    }

                    if (tbBuppinKind.Text == "1" && tbBuppinGroup.Text == "0")
                    {
                        Insert(buppin);
                    }
                    else if(tbBuppinKind.Text == "2" && tbBuppinGroup.Text != "0")
                    {
                        Insert(buppin);
                    }
                   else {
                        MessageBox.Show(
              "分類コードを間違えています。", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        message.Text = DefaultMessageHeader + "分類コードを間違えています。";
                        return false;
                    }

                    break;

                case 1:
                    if (tbBuppinKind.Text == "1" && tbBuppinGroup.Text == "0")
                    {
                        Update(buppin);
                    }
                    else if(tbBuppinKind.Text == "2" && tbBuppinGroup.Text != "0")
                    {
                        Update(buppin);
                    }

               else
                    { 
                        MessageBox.Show(
              "分類コードを間違えています。", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        message.Text = DefaultMessageHeader + "分類コードを間違えています。";
                        return false;
                    }

                    break;
               
            }

            

            return true;
        }

        private bool CodeIsExist()
        {
            return false;
        }

        private void Insert(M001Buppin buppin)
        {
            
            StringBuilder sql = new StringBuilder();
            sql.Append("INSERT INTO M001_BUPPIN ");
            sql.Append("(`M001_BUPNCD`,`M001_BUPNNM`, `M001_BUPNKND`, `M001_BUPNGRP`, `M001_BUPNCND`, `M001_BUPNLCAT1`, `M001_BUPNLCAT2`, `M001_BUPNRENT`, `M001_BUPNDEL`, `M001_BUPNRESN`, `M001_INSDATE`, `M001_INSTIME`) VALUES( ");
            sql.Append(" " + buppin.M001BupnCd + ", ");
            sql.Append("'" + buppin.M001BupnNm + "', ");
            sql.Append(buppin.M001BupnKnd + ", ");
            sql.Append(buppin.M001BupnGrp + ", ");
            sql.Append("'" + buppin.M001BupnCnd + "', ");
            sql.Append("'" + buppin.M001BupnLcat1 + "', ");
            sql.Append("'" + buppin.M001BupnLcat2 + "', ");
            sql.Append(buppin.M001BupnRent + ", ");
            sql.Append(buppin.M001BupnDel + ", ");
            sql.Append("'" + buppin.M001BupnRrsn + "', ");
            sql.Append("now(), now()) ");

            DatabaseConnector conn = new DatabaseConnector();
            conn.ExecuteNonQuery(sql.ToString());
        }

        private void Update(M001Buppin buppin)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("UPdATE M001_BUPPIN SET ");
            sql.Append("M001_BUPNNM = '" + buppin.M001BupnNm + "', ");
            sql.Append("M001_BUPNKND = " + buppin.M001BupnKnd + ", ");
            sql.Append("M001_BUPNGRP = " + buppin.M001BupnGrp + ", ");
            sql.Append("M001_BUPNCND = '" + buppin.M001BupnCnd + "', ");
            sql.Append("M001_BUPNLCAT1 = '" + buppin.M001BupnLcat1 + "', ");
            sql.Append("M001_BUPNLCAT2 = '" + buppin.M001BupnLcat2 + "', ");
            sql.Append("M001_BUPNRENT = " + buppin.M001BupnRent + ", ");
            sql.Append("M001_BUPNDEL = " + buppin.M001BupnDel + ", ");
            sql.Append("M001_BUPNRESN = '" + buppin.M001BupnRrsn + "', ");
            sql.Append("M001_UPDDATE = now(), ");
            sql.Append("M001_UPDTIME =  now() ");
            sql.Append("WHERE M001_BUPNCD = " + tbBuppinCode.Text);
            DatabaseConnector conn = new DatabaseConnector();
            conn.ExecuteNonQuery(sql.ToString());
        }

        protected virtual void Delete()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("UPdATE M001_BUPPIN SET ");
            sql.Append("M001_BUPNDEL = '" + del + "', ");
            sql.Append("M001_UPDDATE = now(), ");
            sql.Append("M001_UPDTIME =  now() ");
            sql.Append("WHERE M001_BUPNCD = " + tbBuppinCode.Text);
            DatabaseConnector conn = new DatabaseConnector();
            conn.ExecuteNonQuery(sql.ToString());
        }

        private M001Buppin SetDataRow(DataRow row)
        {
            M001Buppin buppin = new M001Buppin();

            buppin.M001BupnCd = System.Convert.ToInt32(row[0].ToString());
            buppin.M001BupnNm = row[1].ToString();
            buppin.M001BupnKnd = System.Convert.ToInt32(row[2].ToString());
            buppin.M001BupnGrp = System.Convert.ToInt32(row[3].ToString());
            buppin.M001BupnCnd = row[4].ToString();
            buppin.M001BupnLcat1 = row[5].ToString();
            buppin.M001BupnLcat2 = row[6].ToString();
            buppin.M001BupnRent = System.Convert.ToInt32(row[7].ToString());
            buppin.M001BupnDel = System.Convert.ToInt32(row[8].ToString());
            buppin.M001BupnRrsn = row[9].ToString();

            return buppin;
        }

        private void Display_Click(object sender, EventArgs e)
        {

            int BuppinCode;
            if (!Int32.TryParse(tbBuppinCode.Text, out BuppinCode))
            {
                message.Text = "物品コードが未入力です。";
                return;
            }
            StringBuilder sql = new StringBuilder();
            string table = "M001_BUPPIN";
            sql.Append("SELECT `M001_BUPNCD`, `M001_BUPNNM`, `M001_BUPNKND`, `M001_BUPNGRP`, `M001_BUPNCND`, ");
            sql.Append("`M001_BUPNLCAT1`, `M001_BUPNLCAT2`, `M001_BUPNRENT`, `M001_BUPNDEL`, `M001_BUPNRESN` FROM M001_BUPPIN WHERE M001_BUPNCD = " + BuppinCode);
            DatabaseConnector conn = new DatabaseConnector();
            DataSet detaSet = conn.Result(table, sql.ToString(), table);

            if (CheckValues.IsEmpty(detaSet, table))
            {
                message.Text = "物品コードがありませんでした。";
                return;
            }

            string buppinName = detaSet.Tables[table].Rows[0]["M001_BUPNNM"].ToString();
            tbBuppinName.Text = buppinName;

            string buppinKind = detaSet.Tables[table].Rows[0]["M001_BUPNKND"].ToString();
            tbBuppinKind.Text = buppinKind;

            string buppinGroup = detaSet.Tables[table].Rows[0]["M001_BUPNGRP"].ToString();
            tbBuppinGroup.Text = buppinGroup;

            string condition = detaSet.Tables[table].Rows[0]["M001_BUPNCND"].ToString();
            tbCondition.Text = condition;

            string buppinLocation1 = detaSet.Tables[table].Rows[0]["M001_BUPNLCAT1"].ToString();
            tbBuppinLocation1.Text = buppinLocation1;

            string buppinLocation2 = detaSet.Tables[table].Rows[0]["M001_BUPNLCAT2"].ToString();
            tbLcat2.Text = buppinLocation2;

            string buppinRentFlag = detaSet.Tables[table].Rows[0]["M001_BUPNRENT"].ToString();
            tbBuppinRentFlag.Text = buppinRentFlag;

            string deleteFlag = detaSet.Tables[table].Rows[0]["M001_BUPNDEL"].ToString();
            tbDeleteFlag.Text = deleteFlag;

            string deleteReason = detaSet.Tables[table].Rows[0]["M001_BUPNRESN"].ToString();
            tbDeleteReason.Text = deleteReason;
            message.Text = "検索結果を表示しました。";
            dbAction = 1;
            tbBuppinCode.BackColor = SystemColors.Control;
            tbBuppinCode.ReadOnly = true;
        }

        public void SetModeUpdate()
        {
            dbAction = 1;
            tbBuppinCode.BackColor = SystemColors.Control;
            tbBuppinCode.ReadOnly = true;
        }

        public void SetBuppinCode(String buppinCode)
        {
            tbBuppinCode.Text = buppinCode;
        }

        public void SetBuppinName(String buppinName)
        {
            tbBuppinName.Text = buppinName;
        }

        public void SetBuppinKind(String buppinKind)
        {
            tbBuppinKind.Text = buppinKind;
        }

        public void SetBuppinGroup(String buppinGroup)
        {
            tbBuppinGroup.Text = buppinGroup;
        }

        public void SetBuppinLcat1(String buppinLcat1)
        {
            tbBuppinLocation1.Text = buppinLcat1;
        }

        public void SetBuppinLcat2(String buppinLcat2)
        {
            tbLcat2.Text = buppinLcat2;
        }

        public void SetRentFlag(String buppinRentFlag)
        {
            tbBuppinRentFlag.Text = buppinRentFlag;
        }

        public void SetDelFlag(String buppinDelFlag)
        {
            tbDeleteFlag.Text = buppinDelFlag;
        }

        public void SetBuppinCondition(String buppinCondition)
        {
            tbCondition.Text = buppinCondition;
        }

        public void SetBuppinReason(String buppinReason)
        {
            tbDeleteReason.Text = buppinReason;
        }

        private void ClearTextBox()
        {
            switch (dbAction)
            {
                case 0:
                    message.Text = DefaultMessageHeader + "登録しました";
                    FormSettings.ModeCancel(this, DefaultColor);
                    break;
                case 1:
                    message.Text = DefaultMessageHeader + "変更しました";
                    dbAction = 0;
                    tbBuppinCode.ReadOnly = false;
                    FormSettings.ModeCancel(this, DefaultColor);
                    break;

            }
        }

        private void f8_Click(object sender, EventArgs e)
        {
            Submit();
        }
    }
}
