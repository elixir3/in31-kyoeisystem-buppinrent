﻿using BuppinRent.ColumnsObj;
using BuppinRent.Database;
using BuppinRent.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuppinRent
{
    public partial class SearchForm : IFormDesign
    {
        private Form parent;
        private string param;

        private string DefaultTitle = "種別分類別物品検索";
        private string DefaultMessageHeader = "[メッセージ] ";
        private string DefaultMessage = "検索できます";
        private string[] headerText;
        private DataGridView list;

        public SearchForm(Form parent, string param)
        {
            InitializeComponent(); //これを実行していないとフォームの中身はインターフェイスのものしか存在しない

            list = searchResultList;
            this.parent = parent;
            this.param = param;
            OnCreate();

        }
        private void OnCreate()
        {
            title.Text = DefaultTitle;
            message.Text = DefaultMessageHeader + DefaultMessage;
            FormSettings.EnabledFunction(f3, true, "前へ");
            FormSettings.EnabledFunction(f4, true, "次へ");
            FormSettings.EnabledFunction(f8, true, "表示");
            FormSettings.EnabledFunction(f10, true, "取消");
            FormBorderStyle = FormBorderStyle.None;

            switch (parent.Name)
            {
                case "RentRegisterForm":

                    tbBuppinKind.Text = param;
                    tbBuppinKind.ReadOnly = true;
                    break;
            }
        }

        protected override void FoundKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F3:
                    MoveSelectRow(Keys.F3);
                    return;
                case Keys.F4:
                    MoveSelectRow(Keys.F4);
                    return;
                case Keys.F8:
                    Display_Click(null, null);
                    break;
                case Keys.F9:
                    return;

                case Keys.F10:
                    BtnF10_Click(null, null);
                    return;
                case Keys.Enter:
                    int currentRow = 0;
                    try
                    {
                        currentRow = list.CurrentRow.Index;
                    }
                    catch (Exception)
                    {
                        return;
                    }

                    DataGridViewCellEventArgs args = new DataGridViewCellEventArgs(0, currentRow);
                    SearchResultList_CellDoubleClick(null, args);

                    return;
            }

            base.FoundKeyDown(sender, e);

        }

        protected override void BtnF10_Click(object sender, EventArgs e)
        {

            string kind = tbBuppinKind.Text;
            FormSettings.ModeCancel(this, DefaultColor);
            if (parent is RentRegisterForm)
            {
                tbBuppinKind.Text = kind;
                tbBuppinKind.BackColor = SystemColors.Control;
            }
            list.DataSource = null;
            message.Text = DefaultMessageHeader + DefaultMessage;
        }

        protected override void BtnF11_Click(object sender, EventArgs e)
        {
            base.BtnF11_Click(sender, e);
        }


        private void FoundKeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox box = (TextBox)sender;
            if (e.KeyChar != (Char)Keys.Back && e.KeyChar != (Char)Keys.Enter && e.KeyChar != (Char)Keys.Escape)
            {
                CheckTypeValues(box, e);
            }

        }

        private void CheckTypeValues(TextBox box, KeyPressEventArgs e)
        {

            switch (box.Name)
            {
                case "tbBuppinKind":
                    ValueCheck(box, e, "物品種別は１または２のみ入力可能です", "kind");
                    break;
                case "tbBuppinGroup":
                    ValueCheck(box, e, "分類コードは０から６のみ入力可能です", "group");
                    break;
                default:
                    break;
            }
        }

        private void ValueCheck(TextBox box, KeyPressEventArgs e, string message, string mode)
        {
            switch (mode)
            {
                case "kind":

                    if (CheckValues.IsM001Kind(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;

                case "group":

                    if (CheckValues.IsM001Group(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;
                default:
                    break;

            }
        }

        private void SetDefaultTheme(TextBox box)
        {
            box.BackColor = DefaultColor;
            message.Text = DefaultMessageHeader + DefaultMessage;
            FormSettings.TabStopTrigger(this, true);
        }

        protected void SetErrorTheme(TextBox box, KeyPressEventArgs e, String errorMessage)
        {
            e.Handled = true;
            box.BackColor = ErrorColor;
            message.Text = DefaultMessageHeader + errorMessage;
            FormSettings.TabStopTrigger(this, false);
        }


        private void Display_Click(object sender, EventArgs e)
        {
            switch (parent.Name)
            {
                case "MasterManagementForm":
                    SearchFromMasterManagement();
                    break;
                case "RentRegisterForm":
                    SearchFromRentRegister();
                    break;
            }

            if (list.Rows.Count != 0)
            {
                list.Rows[0].Selected = true;
            }

        }

        private void SearchFromMasterManagement()
        {
            StringBuilder sql = new StringBuilder();
            //sql.Append("SELECT `M001_BUPNCD`, `M001_BUPNNM`, `M001_BUPNKND`, `M001_BUPNGRP`, `M001_BUPNCND`, ");
            //sql.Append("`M001_BUPNLCAT1`, `M001_BUPNLCAT2`, `M001_BUPNRENT`, `M001_BUPNDEL`, `M001_BUPNRESN` FROM M001_BUPPIN WHERE 1=1 ");
            sql.Append("SELECT M001_BUPNCD,");
            sql.Append("M001_BUPNNM,cast(M001_BUPNKND as CHAR) as M001_BUPNKND ,cast(REPLACE(M001_BUPNGRP,0,' ')as CHAR) as M001_BUPNGRP,");
            sql.Append("REPLACE(M001_BUPNCND,null,' ') as M001_BUPNCND,M001_BUPNLCAT1,M001_BUPNLCAT2,");
            sql.Append("REPLACE (REPLACE (M001_BUPNRENT,'0','未'),'9','貸') as M001_BUPNRENT,");
            sql.Append("REPLACE (REPLACE (M001_BUPNDEL,'0','未'),'9','削除') as M001_BUPNDEL,");
            sql.Append("M001_BUPNRESN FROM M001_BUPPIN");
            sql.Append(" WHERE 1=1");
            //物品名称が空白なら処理を出る。
            if (!CheckValues.IsEmpty(tbBuppinName.Text))
            {
                string stBuppinName = tbBuppinName.Text;


                sql.Append(" and M001_BUPNNM like '%" + stBuppinName + "%' ");
            }

            //物品種別が空白なら処理を出る。

            if (!CheckValues.IsEmpty(tbBuppinKind.Text))
            {
                string stBuppinKind = tbBuppinKind.Text;

                if (stBuppinKind.Equals("1") || stBuppinKind.Equals("2"))
                {
                    sql.Append(" and M001_BUPNKND = " + stBuppinKind);
                }
            }
            //分類コードが空白なら処理を出る。
            if (!CheckValues.IsEmpty(tbBuppinGroup.Text))
            {
                string stBuppinGroup = tbBuppinGroup.Text;
                //分類コードが押された時のSQLの追加構文list
                if (stBuppinGroup.Equals("1") || stBuppinGroup.Equals("2") || stBuppinGroup.Equals("3") ||
                     stBuppinGroup.Equals("4") || stBuppinGroup.Equals("5") || stBuppinGroup.Equals("6"))
                {
                    sql.Append(" and M001_BUPNGRP =" + stBuppinGroup);
                }
            }

            string table = "M001_BUPPIN";
            DatabaseConnector conn = new DatabaseConnector();

            DataSet dataSet = conn.Result(table, sql.ToString(), table);

            if (dataSet.Tables[table].Rows.Count != 0)
            {
                SetCellValues(dataSet, table);

            }

            list.DataSource = dataSet.Tables[table];
            SetMaterColumnProperty();

            headerText = new string[] { "物品名", "物品名", "種別", "類", "状態(付属品)", "基本保管場所", "現在保管場所", "貸出", "削", "削除理由", };

            for (int count = 0; count < headerText.Length; count++)
            {
                list.Columns[count].HeaderText = headerText[count];
            }





            if (CheckValues.IsEmpty(dataSet, table))
            {
                MessageBox.Show(
                    "該当する物品はありませんでした", "メッセージ", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        private void SearchFromRentRegister()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT M001_BUPNCD,");
            sql.Append("M001_BUPNNM,cast(M001_BUPNKND as CHAR) as M001_BUPNKND ,cast(M001_BUPNGRP as CHAR) as M001_BUPNGRP,");
            sql.Append("REPLACE(M001_BUPNCND,null,' ') as M001_BUPNCND, M001_BUPNLCAT1, M001_BUPNLCAT2 ");
            sql.Append("FROM M001_BUPPIN");
            sql.Append(" WHERE M001_BUPNRENT = 0 AND M001_BUPNDEL = 0 ");
            //物品名称が空白なら処理を出る。
            if (!CheckValues.IsEmpty(tbBuppinName.Text))
            {
                string stBuppinName = tbBuppinName.Text;


                sql.Append(" and M001_BUPNNM like '%" + stBuppinName + "%' ");
            }

            //物品種別が空白なら処理を出る。

            if (!CheckValues.IsEmpty(tbBuppinKind.Text))
            {
                string stBuppinKind = tbBuppinKind.Text;

                if (stBuppinKind.Equals("1") || stBuppinKind.Equals("2"))
                {
                    sql.Append(" and M001_BUPNKND = " + stBuppinKind);
                }
            }
            //分類コードが空白なら処理を出る。
            if (!CheckValues.IsEmpty(tbBuppinGroup.Text))
            {
                string stBuppinGroup = tbBuppinGroup.Text;
                //分類コードが押された時のSQLの追加構文list
                if (stBuppinGroup.Equals("1") || stBuppinGroup.Equals("2") || stBuppinGroup.Equals("3") ||
                     stBuppinGroup.Equals("4") || stBuppinGroup.Equals("5") || stBuppinGroup.Equals("6"))
                {
                    sql.Append(" and M001_BUPNGRP =" + stBuppinGroup);
                }
            }

            string table = "M001_BUPPIN";
            DatabaseConnector conn = new DatabaseConnector();


            DataSet dataSet = conn.Result(table, sql.ToString(), table);

            if (dataSet.Tables[table].Rows.Count != 0)
            {
                SetCellValues(dataSet, table);

            }

            list.DataSource = dataSet.Tables[table];


            SetRentColumnProperty();
            headerText = new string[] { "物品名", "物品名", "種別", "類", "状態(付属品)", "基本保管場所", "現在保管場所" };

            for (int count = 0; count < headerText.Length; count++)
            {
                list.Columns[count].HeaderText = headerText[count];
            }

            if (CheckValues.IsEmpty(dataSet, table))
            {
                MessageBox.Show(
                    "該当する物品はありませんでした", "メッセージ", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }

        }

        /*
         * ヘッダーセルのスタイル変更
         */
        protected void PaintHeaderCells(object sender, DataGridViewCellPaintingEventArgs e)
        {
            FormSettings.PaintHeaderCells(sender, e, list, 0, headerText);
        }

        /*
       *列の幅調整
       */
        protected void SetRentColumnProperty()
        {
            double width = list.Size.Width;

            if (list.Rows.Count > 21)
            {
                width = width - 17;
            }
            int small = 50;
            int midle = 80;
            int big = 160;

            var center = DataGridViewContentAlignment.MiddleCenter; //中央揃え
            //var right = DataGridViewContentAlignment.MiddleRight; //右揃え

            //RowHeder
            list.RowHeadersWidth = small;

            //Column0 As 物品コード
            list.Columns[0].Width = midle;
            list.Columns[0].HeaderCell.Style.Alignment = center;


            //Column1 As 物品名
            list.Columns[1].Width = big;
            list.Columns[1].HeaderCell.Style.Alignment = center;

            //Column2 As Code
            list.Columns[2].Width = small;

            //Column3 As Name
            list.Columns[3].Width = small;

            //Column4 As 状態付属
            list.Columns[4].Width = (int)width - (small * 3 + midle * 1 + big * 3) - 2;
            list.Columns[4].HeaderCell.Style.Alignment = center;
            list.Columns[4].DefaultCellStyle.Alignment = center;

            //Column5 As 基本保管場所
            list.Columns[5].Width = big;
            list.Columns[5].HeaderCell.Style.Alignment = center;

            //Column6 As 現在保管場所
            list.Columns[6].Width = big;
            list.Columns[6].HeaderCell.Style.Alignment = center;
        }


        protected void SetMaterColumnProperty()
        {
            double width = list.Size.Width;

            if (list.Rows.Count > 21)
            {
                width = width - 17;
            }
            int small = 50;
            int midle = 80;
            int big = 160;

            var center = DataGridViewContentAlignment.MiddleCenter; //中央揃え
            var right = DataGridViewContentAlignment.MiddleRight; //右揃え

            //RowHeder
            list.RowHeadersWidth = small;

            //Column0 As 物品コード
            list.Columns[0].Width = small;
            list.Columns[0].HeaderCell.Style.Alignment = center;
            list.Columns[0].DefaultCellStyle.Alignment = right;
        
            //Column1 As 物品名
            list.Columns[1].Width = big;
            list.Columns[1].HeaderCell.Style.Alignment = center;

            //Column2 As Code
            list.Columns[2].Width = small;
            list.Columns[2].DefaultCellStyle.Alignment = center;

            //Column3 As Name
            list.Columns[3].Width = small;
            list.Columns[3].DefaultCellStyle.Alignment = center;

            //Column4 As 状態付属
            list.Columns[4].Width = (int)width - (small * 6 + midle * 2 + big * 2) - 2;
            list.Columns[4].HeaderCell.Style.Alignment = center;
            list.Columns[4].DefaultCellStyle.Alignment = center;

            //Column5 As 基本保管場所
            list.Columns[5].Width = midle;
            list.Columns[5].HeaderCell.Style.Alignment = center;

            //Column6 As 現在保管場所
            list.Columns[6].Width = midle;
            list.Columns[6].HeaderCell.Style.Alignment = center;

            //Column7 As Rent
            list.Columns[7].Width = small;
            list.Columns[7].HeaderCell.Style.Alignment = center;
            list.Columns[7].DefaultCellStyle.Alignment = center;
            //Column8 As Del
            list.Columns[8].Width = small;
            list.Columns[8].HeaderCell.Style.Alignment = center;
            list.Columns[8].DefaultCellStyle.Alignment = center;
            //Column9 As Reasaon
            list.Columns[9].Width = big;
            list.Columns[9].HeaderCell.Style.Alignment = center;
            list.Columns[9].DefaultCellStyle.Alignment = center;
        }

        /*
        * SQLだとREPLACEが複雑になるのでC#でレコードの内容を変更します
        */
        protected void SetCellValues(DataSet dataSet, string table)
        {

            string cell; //セルの内容を格納する

            //DataGridViewの内容を全件調べる
            for (int rowIndex = 0; rowIndex < dataSet.Tables[table].Rows.Count; rowIndex++)
            {
                cell = dataSet.Tables[table].Rows[rowIndex][2].ToString();
                switch (cell)
                {
                    case "1":
                        cell = "書籍";
                        break;
                    case "2":
                        cell = "機器";
                        break;
                }
                dataSet.Tables[table].Rows[rowIndex][2] = cell;

                //２列目、分類コードを可視性向上のため文字に置き換え開始
                cell = "";
                cell = dataSet.Tables[table].Rows[rowIndex][3].ToString(); //二列目 rowindex行目の文字を格納

                switch (cell)
                {
                    case "1":
                        cell = "本体";
                        break;
                    case "2":
                        cell = "モニター";
                        break;
                    case "3":
                        cell = "プリンタ";
                        break;
                    case "4":
                        cell = "モデム";
                        break;
                    case "5":
                        cell = "ルータ";
                        break;
                    case "6":
                        cell = "ハブ";
                        break;
                    case "0":
                        cell = " ";
                        break;
                }

                dataSet.Tables[table].Rows[rowIndex][3] = cell; //変換した文字をセルに設定する、置き換え終了

            }

        }


        private void SearchResultList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1 || e.ColumnIndex == -1)
            {
                return;
            }

            DataGridViewRow row = list.Rows[e.RowIndex];

            switch (parent.Name)
            {
                case "MasterManagementForm":
                    SetToMasterManagement(row);
                    break;
                case "RentRegisterForm":
                    SetToRentRegister(row);
                    break;
            }
        }

        private void SetToMasterManagement(DataGridViewRow row)
        {
            ((MasterManagementForm)this.Owner).SetModeUpdate();
            ((MasterManagementForm)this.Owner).SetBuppinCode(row.Cells[0].Value.ToString());
            ((MasterManagementForm)this.Owner).SetBuppinName(row.Cells[1].Value.ToString());

            string kind = "1";
            if (row.Cells[2].Value.ToString() is "機器")
            {
                kind = "2";
            }

            string group = "";
            string groupNm = row.Cells[3].Value.ToString();
            switch (groupNm)
            {
                case "本体":
                    group = "1";
                    break;
                case "モニター":
                    group = "2";
                    break;
                case "プリンタ":
                    group = "3";
                    break;
                case "モデム":
                    group = "4";
                    break;
                case "ルータ":
                    group = "5";
                    break;
                case "ハブ":
                    group = "6";
                    break;
            }

            ((MasterManagementForm)this.Owner).SetBuppinKind(kind);
            ((MasterManagementForm)this.Owner).SetBuppinGroup(group);
            ((MasterManagementForm)this.Owner).SetBuppinLcat1(row.Cells[5].Value.ToString());
            ((MasterManagementForm)this.Owner).SetBuppinLcat2(row.Cells[6].Value.ToString());

            string rent = "0";
            if (row.Cells[7].Value.ToString().Equals("貸"))
            {
                rent = "9";
            }

            string del = "0";
            if (row.Cells[8].Value.ToString().Equals("削除"))
            {
                del = "9";
            }

            ((MasterManagementForm)this.Owner).SetRentFlag(rent);
            ((MasterManagementForm)this.Owner).SetDelFlag(del);
            ((MasterManagementForm)this.Owner).SetBuppinCondition(row.Cells[4].Value.ToString());
            ((MasterManagementForm)this.Owner).SetBuppinReason(row.Cells[9].Value.ToString());
            BtnF11_Click(null, null);
        }

        private void SetToRentRegister(DataGridViewRow row)
        {
            ((RentRegisterForm)this.Owner).SetKindReadOnly();
            ((RentRegisterForm)this.Owner).SetBuppinCode(row.Cells[0].Value.ToString());
            ((RentRegisterForm)this.Owner).SetBuppinName(row.Cells[1].Value.ToString());
            ((RentRegisterForm)this.Owner).SetTbBuppinCnd(row.Cells[4].Value.ToString());
            ((RentRegisterForm)this.Owner).SetBuppinLcat1(row.Cells[5].Value.ToString());
            ((RentRegisterForm)this.Owner).SetBuppinLcat2(row.Cells[6].Value.ToString());
            BtnF11_Click(null, null);
        }

        private void MoveSelectRow(Keys key)
        {
            if (list.Rows.Count is 0)
            {
                return;
            }

            int rowIndex = 0;
            try
            {
                rowIndex = list.CurrentRow.Index;

            }
            catch
            {
                list.Rows[rowIndex].Selected = true;
                list.CurrentCell = list[0, rowIndex];
            }

            switch (key)
            {
                case Keys.F3:
                    if (rowIndex != 0)
                    {
                        list.Rows[rowIndex - 1].Selected = true;
                        list.Rows[rowIndex].Selected = false;
                        list.CurrentCell = list[0, rowIndex - 1];
                    }
                    return;
                case Keys.F4:
                    if (rowIndex + 1 != list.Rows.Count)
                    {
                        list.Rows[rowIndex].Selected = false;
                        list.Rows[rowIndex + 1].Selected = true;
                        list.CurrentCell = list[0, rowIndex + 1];
                    }
                    return;
            }
        }
    }
}
