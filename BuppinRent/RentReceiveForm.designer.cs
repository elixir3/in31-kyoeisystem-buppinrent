﻿namespace BuppinRent
{
    partial class RentReceiveForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbRentNo = new System.Windows.Forms.TextBox();
            this.tbBuppinKind = new System.Windows.Forms.TextBox();
            this.tbRentCont = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbRentUser = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.display = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.tbRentDate = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.RentcMnt = new System.Windows.Forms.TextBox();
            this.btnLeft = new System.Windows.Forms.Button();
            this.btnRight = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbBuppinCode = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbRentcMnt = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tbRetrnNm = new System.Windows.Forms.TextBox();
            this.tbLcat2 = new System.Windows.Forms.TextBox();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.bg = new System.Windows.Forms.Label();
            this.tbBuppinName = new System.Windows.Forms.Label();
            this.lbRentSeq = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.tbDay = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.tbMonth = new System.Windows.Forms.TextBox();
            this.tbYear = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // f7
            // 
            this.f7.Click += new System.EventHandler(this.BtnF7_Click);
            // 
            // f11
            // 
            this.f11.TabIndex = 18;
            // 
            // tbRentNo
            // 
            this.tbRentNo.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbRentNo.Location = new System.Drawing.Point(226, 117);
            this.tbRentNo.MaxLength = 5;
            this.tbRentNo.Name = "tbRentNo";
            this.tbRentNo.Size = new System.Drawing.Size(112, 36);
            this.tbRentNo.TabIndex = 1;
            this.tbRentNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // tbBuppinKind
            // 
            this.tbBuppinKind.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbBuppinKind.Location = new System.Drawing.Point(227, 191);
            this.tbBuppinKind.Name = "tbBuppinKind";
            this.tbBuppinKind.ReadOnly = true;
            this.tbBuppinKind.Size = new System.Drawing.Size(26, 36);
            this.tbBuppinKind.TabIndex = 80;
            // 
            // tbRentCont
            // 
            this.tbRentCont.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbRentCont.Location = new System.Drawing.Point(226, 231);
            this.tbRentCont.Name = "tbRentCont";
            this.tbRentCont.ReadOnly = true;
            this.tbRentCont.Size = new System.Drawing.Size(204, 36);
            this.tbRentCont.TabIndex = 81;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label1.Location = new System.Drawing.Point(139, 120);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 28);
            this.label1.TabIndex = 23;
            this.label1.Text = "貸出No.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label2.Location = new System.Drawing.Point(136, 194);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 28);
            this.label2.TabIndex = 23;
            this.label2.Text = "物品種別";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label3.Location = new System.Drawing.Point(135, 235);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 28);
            this.label3.TabIndex = 23;
            this.label3.Text = "借受者名";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.label6.Location = new System.Drawing.Point(273, 194);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(134, 28);
            this.label6.TabIndex = 23;
            this.label6.Text = "1:書籍 2:機器";
            // 
            // tbRentUser
            // 
            this.tbRentUser.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbRentUser.Location = new System.Drawing.Point(225, 273);
            this.tbRentUser.Name = "tbRentUser";
            this.tbRentUser.ReadOnly = true;
            this.tbRentUser.Size = new System.Drawing.Size(204, 36);
            this.tbRentUser.TabIndex = 82;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label11.Location = new System.Drawing.Point(135, 275);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 28);
            this.label11.TabIndex = 23;
            this.label11.Text = "利用者名";
            // 
            // display
            // 
            this.display.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.display.Location = new System.Drawing.Point(381, 93);
            this.display.Name = "display";
            this.display.Size = new System.Drawing.Size(124, 80);
            this.display.TabIndex = 2;
            this.display.Text = "表示";
            this.display.UseVisualStyleBackColor = true;
            this.display.Click += new System.EventHandler(this.BtnF1_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label5.Location = new System.Drawing.Point(844, 191);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 28);
            this.label5.TabIndex = 23;
            this.label5.Text = "貸出日";
            // 
            // tbRentDate
            // 
            this.tbRentDate.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbRentDate.Location = new System.Drawing.Point(912, 186);
            this.tbRentDate.Name = "tbRentDate";
            this.tbRentDate.ReadOnly = true;
            this.tbRentDate.Size = new System.Drawing.Size(150, 36);
            this.tbRentDate.TabIndex = 83;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label4.Location = new System.Drawing.Point(859, 231);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 28);
            this.label4.TabIndex = 23;
            this.label4.Text = "備考";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // RentcMnt
            // 
            this.RentcMnt.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.RentcMnt.Location = new System.Drawing.Point(912, 231);
            this.RentcMnt.MaxLength = 50;
            this.RentcMnt.Multiline = true;
            this.RentcMnt.Name = "RentcMnt";
            this.RentcMnt.ReadOnly = true;
            this.RentcMnt.Size = new System.Drawing.Size(220, 78);
            this.RentcMnt.TabIndex = 84;
            // 
            // btnLeft
            // 
            this.btnLeft.Enabled = false;
            this.btnLeft.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnLeft.Location = new System.Drawing.Point(587, 121);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(100, 40);
            this.btnLeft.TabIndex = 100;
            this.btnLeft.Text = "←";
            this.btnLeft.UseVisualStyleBackColor = true;
            this.btnLeft.Visible = false;
            this.btnLeft.Click += new System.EventHandler(this.BtnLeft_Click);
            // 
            // btnRight
            // 
            this.btnRight.Enabled = false;
            this.btnRight.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnRight.Location = new System.Drawing.Point(689, 121);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(100, 40);
            this.btnRight.TabIndex = 101;
            this.btnRight.Text = "→";
            this.btnRight.UseVisualStyleBackColor = true;
            this.btnRight.Visible = false;
            this.btnRight.Click += new System.EventHandler(this.BtnRight_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label7.Location = new System.Drawing.Point(32, 350);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 28);
            this.label7.TabIndex = 23;
            this.label7.Text = "No.";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label8.Location = new System.Drawing.Point(116, 351);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(107, 28);
            this.label8.TabIndex = 23;
            this.label8.Text = "物品コード";
            // 
            // tbBuppinCode
            // 
            this.tbBuppinCode.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbBuppinCode.Location = new System.Drawing.Point(225, 347);
            this.tbBuppinCode.MaxLength = 5;
            this.tbBuppinCode.Name = "tbBuppinCode";
            this.tbBuppinCode.ReadOnly = true;
            this.tbBuppinCode.Size = new System.Drawing.Size(112, 36);
            this.tbBuppinCode.TabIndex = 3;
            this.tbBuppinCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label10.Location = new System.Drawing.Point(149, 407);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 28);
            this.label10.TabIndex = 23;
            this.label10.Text = "返却日";
            // 
            // tbRentcMnt
            // 
            this.tbRentcMnt.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbRentcMnt.Location = new System.Drawing.Point(912, 342);
            this.tbRentcMnt.MaxLength = 50;
            this.tbRentcMnt.Multiline = true;
            this.tbRentcMnt.Name = "tbRentcMnt";
            this.tbRentcMnt.Size = new System.Drawing.Size(220, 71);
            this.tbRentcMnt.TabIndex = 9;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label15.Location = new System.Drawing.Point(825, 347);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(88, 28);
            this.label15.TabIndex = 23;
            this.label15.Text = "返却備考";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label16.Location = new System.Drawing.Point(135, 457);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(88, 28);
            this.label16.TabIndex = 23;
            this.label16.Text = "返却者名";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label17.Location = new System.Drawing.Point(130, 510);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(88, 28);
            this.label17.TabIndex = 23;
            this.label17.Text = "返却場所";
            // 
            // tbRetrnNm
            // 
            this.tbRetrnNm.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbRetrnNm.Location = new System.Drawing.Point(222, 454);
            this.tbRetrnNm.MaxLength = 20;
            this.tbRetrnNm.Name = "tbRetrnNm";
            this.tbRetrnNm.Size = new System.Drawing.Size(207, 36);
            this.tbRetrnNm.TabIndex = 7;
            // 
            // tbLcat2
            // 
            this.tbLcat2.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbLcat2.Location = new System.Drawing.Point(225, 507);
            this.tbLcat2.MaxLength = 20;
            this.tbLcat2.Name = "tbLcat2";
            this.tbLcat2.Size = new System.Drawing.Size(207, 36);
            this.tbLcat2.TabIndex = 8;
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Location = new System.Drawing.Point(21, 578);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.RowTemplate.Height = 21;
            this.dataGridView.Size = new System.Drawing.Size(1229, 292);
            this.dataGridView.TabIndex = 25;
            this.dataGridView.TabStop = false;
            this.dataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellDoubleClick);
            this.dataGridView.Sorted += new System.EventHandler(this.dataGridView_Sorted);
            // 
            // bg
            // 
            this.bg.BackColor = System.Drawing.SystemColors.ControlDark;
            this.bg.Location = new System.Drawing.Point(30, 327);
            this.bg.Name = "bg";
            this.bg.Size = new System.Drawing.Size(1220, 1);
            this.bg.TabIndex = 26;
            this.bg.Text = " ";
            this.bg.UseMnemonic = false;
            // 
            // tbBuppinName
            // 
            this.tbBuppinName.BackColor = System.Drawing.Color.Transparent;
            this.tbBuppinName.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.tbBuppinName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.tbBuppinName.Location = new System.Drawing.Point(343, 351);
            this.tbBuppinName.Name = "tbBuppinName";
            this.tbBuppinName.Size = new System.Drawing.Size(344, 38);
            this.tbBuppinName.TabIndex = 23;
            // 
            // lbRentSeq
            // 
            this.lbRentSeq.AutoSize = true;
            this.lbRentSeq.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.lbRentSeq.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.lbRentSeq.Location = new System.Drawing.Point(75, 350);
            this.lbRentSeq.Name = "lbRentSeq";
            this.lbRentSeq.Size = new System.Drawing.Size(25, 28);
            this.lbRentSeq.TabIndex = 27;
            this.lbRentSeq.Text = "1";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label22.Location = new System.Drawing.Point(221, 407);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(38, 28);
            this.label22.TabIndex = 48;
            this.label22.Text = "20";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label23.Location = new System.Drawing.Point(522, 407);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(0, 28);
            this.label23.TabIndex = 47;
            // 
            // tbDay
            // 
            this.tbDay.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbDay.Location = new System.Drawing.Point(430, 404);
            this.tbDay.MaxLength = 2;
            this.tbDay.Name = "tbDay";
            this.tbDay.Size = new System.Drawing.Size(52, 36);
            this.tbDay.TabIndex = 6;
            this.tbDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbDay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label24.Location = new System.Drawing.Point(401, 407);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(23, 28);
            this.label24.TabIndex = 45;
            this.label24.Text = "/";
            // 
            // tbMonth
            // 
            this.tbMonth.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbMonth.Location = new System.Drawing.Point(343, 404);
            this.tbMonth.MaxLength = 2;
            this.tbMonth.Name = "tbMonth";
            this.tbMonth.Size = new System.Drawing.Size(52, 36);
            this.tbMonth.TabIndex = 5;
            this.tbMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbMonth.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // tbYear
            // 
            this.tbYear.Font = new System.Drawing.Font("メイリオ", 14.25F);
            this.tbYear.Location = new System.Drawing.Point(262, 404);
            this.tbYear.MaxLength = 2;
            this.tbYear.Name = "tbYear";
            this.tbYear.Size = new System.Drawing.Size(52, 36);
            this.tbYear.TabIndex = 4;
            this.tbYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbYear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.label25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label25.Location = new System.Drawing.Point(314, 407);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(23, 28);
            this.label25.TabIndex = 42;
            this.label25.Text = "/";
            // 
            // RentReceiveForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 1024);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.tbDay);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.tbMonth);
            this.Controls.Add(this.tbYear);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.lbRentSeq);
            this.Controls.Add(this.bg);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.btnRight);
            this.Controls.Add(this.btnLeft);
            this.Controls.Add(this.display);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tbBuppinName);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbBuppinKind);
            this.Controls.Add(this.tbRentUser);
            this.Controls.Add(this.tbRentcMnt);
            this.Controls.Add(this.RentcMnt);
            this.Controls.Add(this.tbRentDate);
            this.Controls.Add(this.tbRentCont);
            this.Controls.Add(this.tbLcat2);
            this.Controls.Add(this.tbRetrnNm);
            this.Controls.Add(this.tbBuppinCode);
            this.Controls.Add(this.tbRentNo);
            this.Name = "RentReceiveForm";
            this.Text = "LendingRecordRegistrationForm";
            this.Controls.SetChildIndex(this.tbRentNo, 0);
            this.Controls.SetChildIndex(this.tbBuppinCode, 0);
            this.Controls.SetChildIndex(this.tbRetrnNm, 0);
            this.Controls.SetChildIndex(this.tbLcat2, 0);
            this.Controls.SetChildIndex(this.tbRentCont, 0);
            this.Controls.SetChildIndex(this.tbRentDate, 0);
            this.Controls.SetChildIndex(this.RentcMnt, 0);
            this.Controls.SetChildIndex(this.tbRentcMnt, 0);
            this.Controls.SetChildIndex(this.tbRentUser, 0);
            this.Controls.SetChildIndex(this.tbBuppinKind, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.tbBuppinName, 0);
            this.Controls.SetChildIndex(this.label10, 0);
            this.Controls.SetChildIndex(this.label16, 0);
            this.Controls.SetChildIndex(this.label17, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.label15, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.label11, 0);
            this.Controls.SetChildIndex(this.display, 0);
            this.Controls.SetChildIndex(this.btnLeft, 0);
            this.Controls.SetChildIndex(this.btnRight, 0);
            this.Controls.SetChildIndex(this.dataGridView, 0);
            this.Controls.SetChildIndex(this.bg, 0);
            this.Controls.SetChildIndex(this.lbRentSeq, 0);
            this.Controls.SetChildIndex(this.title, 0);
            this.Controls.SetChildIndex(this.message, 0);
            this.Controls.SetChildIndex(this.f11, 0);
            this.Controls.SetChildIndex(this.f1, 0);
            this.Controls.SetChildIndex(this.f2, 0);
            this.Controls.SetChildIndex(this.f3, 0);
            this.Controls.SetChildIndex(this.f4, 0);
            this.Controls.SetChildIndex(this.f5, 0);
            this.Controls.SetChildIndex(this.f6, 0);
            this.Controls.SetChildIndex(this.f7, 0);
            this.Controls.SetChildIndex(this.f8, 0);
            this.Controls.SetChildIndex(this.f9, 0);
            this.Controls.SetChildIndex(this.f10, 0);
            this.Controls.SetChildIndex(this.f12, 0);
            this.Controls.SetChildIndex(this.label25, 0);
            this.Controls.SetChildIndex(this.tbYear, 0);
            this.Controls.SetChildIndex(this.tbMonth, 0);
            this.Controls.SetChildIndex(this.label24, 0);
            this.Controls.SetChildIndex(this.tbDay, 0);
            this.Controls.SetChildIndex(this.label23, 0);
            this.Controls.SetChildIndex(this.label22, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbRentNo;
        private System.Windows.Forms.TextBox tbBuppinKind;
        private System.Windows.Forms.TextBox tbRentCont;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbRentUser;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button display;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbRentDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox RentcMnt;
        private System.Windows.Forms.Button btnLeft;
        private System.Windows.Forms.Button btnRight;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbBuppinCode;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbRentcMnt;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tbRetrnNm;
        private System.Windows.Forms.TextBox tbLcat2;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Label bg;
        private System.Windows.Forms.Label tbBuppinName;
        private System.Windows.Forms.Label lbRentSeq;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox tbDay;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox tbMonth;
        private System.Windows.Forms.TextBox tbYear;
        private System.Windows.Forms.Label label25;
    }
}