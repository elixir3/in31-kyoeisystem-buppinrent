﻿using BuppinRent.Database;
using BuppinRent.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuppinRent
{
    public partial class NonReceivedListForm : IListForm
    {
        public NonReceivedListForm()
        {
            InitializeComponent();
            OnCreate();
        }

        protected override void OnCreate()
        {
            defaultTitle = "未返却照会";
            defaultMessageHeader = "[メッセージ]  ";
            defaultMessage = "未返却の一覧を確認できます";
            base.OnCreate();
        }

        protected override void FoundKeyDown(object sender, KeyEventArgs e)
        {
            base.FoundKeyDown(sender, e);
        }

        protected override void BtnF10_Click(object sender, EventArgs e)
        {
            base.BtnF10_Click(sender, e);
        }

        protected override void BtnF11_Click(object sender, EventArgs e)
        {
            base.BtnF11_Click(sender, e);
        }

        protected override void CheckTypeValues(TextBox box, KeyPressEventArgs e)
        {

            switch (box.Name)
            {
                case "tbBuppinGroup":
                    ValueCheck(box, e, "分類コードは０から６のみ入力可能です", "group");
                    break;
                case "tbYear":
                    ValueCheck(box, e, "日付は数値のみ入力可能です。", "year");
                    break;
                case "tbMonth":
                    ValueCheck(box, e, "日付は数値のみ入力可能です。", "month");
                    break;
                case "tbDay":
                    ValueCheck(box, e, "日付は数値のみ入力可能です。", "day");
                    break;
                case "tbPlanYear":
                    ValueCheck(box, e, "日付は数値のみ入力可能です。", "planYear");
                    break;
                case "tbPlanMonth":
                    ValueCheck(box, e, "日付は数値のみ入力可能です。", "planMonth");
                    break;
                case "tbPlanDay":
                    ValueCheck(box, e, "日付は数値のみ入力可能です。", "planDay");
                    break;
                default:
                    base.CheckTypeValues(box, e);
                    break;
            }


        }

        protected void ValueCheck(TextBox box, KeyPressEventArgs e, string message, string mode)
        {
            switch (mode)
            {

                case "group":

                    if (CheckValues.IsM001Group(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;

                case "year":
                    if (CheckValues.IsNumber(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;

                case "month":
                    if (CheckValues.IsNumber(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;

                case "day":
                    if (CheckValues.IsNumber(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;

                case "planYear":
                    if (CheckValues.IsNumber(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;

                case "planMonth":
                    if (CheckValues.IsNumber(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;

                case "planDay":
                    if (CheckValues.IsNumber(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;

                default:
                    break;

            }

        }

        protected override void FoundKeyPress(object sender, KeyPressEventArgs e)
        {
            base.FoundKeyPress(sender, e);
        }

        protected override void SetDefaultTheme(TextBox box)
        {
            box.BackColor = DefaultColor;
            message.Text = defaultMessageHeader + defaultMessage;
            FormSettings.TabStopTrigger(this, true);
        }

        protected override void SetErrorTheme(TextBox box, KeyPressEventArgs e, String errorMessage)
        {
            e.Handled = true;
            box.BackColor = ErrorColor;
            message.Text = defaultMessageHeader + errorMessage;
            FormSettings.TabStopTrigger(this, false);
        }




        protected override void Display_Click(object sender, EventArgs e)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT CAST(T001_BUPNKND as CHAR) as T001_BUPNKND,M001_BUPNNM,T002_RETRNPLAN,T001_RENTCONT,T001_RENTUSER,T001_RENTNO FROM by_loan WHERE 1=1");
            if (!CheckValues.IsM001Kind(tbKind.Text))
            {
                tbKind.Text = "0";
                tbKind.BackColor = DefaultColor;

            }

            DateTime? oldDateTime = CheckValues.IsDateTime("20" + tbYear.Text, tbMonth.Text, tbDay.Text);
            DateTime dateTime;
            if (oldDateTime == null)
            {
                return;
            }
            else
            {
                dateTime = (DateTime)oldDateTime;
            } 
      //      DateTime oldDate;
    //        DateTime newDate;
    //        DateTime? newDateTime = CheckValues.IsDateTime("20" + tbPlanYear.Text, tbPlanMonth.Text, tbPlanDay.Text);
    //        if (oldDateTime != null && newDateTime != null)
    //        {
    //            oldDate = (DateTime)oldDateTime;
    //            newDate = (DateTime)newDateTime;
    //            if (oldDate.CompareTo(newDate) is 1)
    //            {
    //                MessageBox.Show(
    // "左側の日付には右側より過去の日付を入力してください。", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
    //                message.Text = defaultMessageHeader + "日付を変更してください。";
    //                return;
    //            }
    //        }
    //        else
    //        {
    //            MessageBox.Show(
    //"日付が空白です。", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
    //            message.Text = defaultMessageHeader + "日付が空白です。";
    //            return;
    //        }


            if (!CheckValues.IsM001Group(tbBuppinGroup.Text))
            {
                tbBuppinGroup.Text = "0";
                tbBuppinGroup.BackColor = DefaultColor;

            }
            string stAricleType = tbKind.Text;
            if (stAricleType == "1")
            {
                sql.Append(" and T001_BUPNKND = 1");
            }
            else if (stAricleType is "2")
            {
                sql.Append(" and T001_BUPNKND = 2");
            }
            sql.Append(" and T002_RETRNPLAN between 19871005 and " + dateTime.Date.ToString("yyyyMMdd") );
            //sql.Append(" and T002_RETRNPLAN BETWEEN " + oldDate.Date.ToString("yyyyMMdd") + " and " + newDate.ToString("yyyyMMdd"));
            string stBuppinGroup = tbBuppinGroup.Text;
            if (stBuppinGroup.Equals("1") || stBuppinGroup.Equals("2") || stBuppinGroup.Equals("3") ||
                     stBuppinGroup.Equals("4") || stBuppinGroup.Equals("5") || stBuppinGroup.Equals("6"))
            {
                sql.Append(" and M001_BUPNGRP =" + stBuppinGroup);
            }



            sql.Append(";");
            string table = "by_loan";

            DatabaseConnector conn = new DatabaseConnector();

            dataSet = conn.Result(table, sql.ToString(), table);
            if (dataSet.Tables[table].Rows.Count != 0)
            {
                SetCellValues(table);
            }

            list.DataSource = dataSet.Tables[table];

            keywords[0] = tbKind.Text;
            //keywords[1] = oldDate.Date.ToString("yyyy/MM/dd");
          //  keywords[2] = newDate.Date.ToString("yyyy/MM/dd");
            keywords[3] = tbKind.Text;

            if (list.Rows.Count != 0)
            {
                list.Rows[0].Selected = true;
                SetRowColor();
            }

            //ヘッダーのカラムの幅
            list.Columns[0].Width = 70;//分類コード
            list.Columns[0].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //list.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            list.Columns[1].Width = 272;//物品名
            list.Columns[1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            list.Columns[2].Width = 90;//返却予定日
            list.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            list.Columns[3].Width = 150;//借受者名
            list.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            list.Columns[4].Width = 270;//利用者名
            list.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            list.Columns[5].Width = 70;//貸出No
            list.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            //Add by By HAL OSAKA Katsuya Akamatsu on 2017/10/6 FRI



            headerText = new string[] { "類", "物品名", "返却予定日", "借受者名", "利用者名", "貸出No" };

            for (int count = 0; count < headerText.Length; count++)
            {
                list.Columns[count].HeaderText = headerText[count];
            }



        }

        /*
 * SQLだとREPLACEが複雑になるのでC#でレコードの内容を変更します
 */
        protected override void SetCellValues(string table)
        {
            string cell; //セルの内容を格納する

            //DataGridViewの内容を全件調べる
            for (int rowIndex = 0; rowIndex < dataSet.Tables[table].Rows.Count; rowIndex++)
            {
                //２列目、分類コードを可視性向上のため文字に置き換え開始

                cell = dataSet.Tables[table].Rows[rowIndex][0].ToString(); //二列目 rowindex行目の文字を格納

                switch (cell)
                {
                    case "1":
                        cell = "書籍";
                        break;
                    case "2":
                        cell = "機器";
                        break;
                    case "0":
                        cell = "すべて";
                        break;
                }

                dataSet.Tables[table].Rows[rowIndex][0] = cell; //変換した文字をセルに設定する、置き換え終了

            }
        }

        private void SetRowColor()
        {
            DateTime today = DateTime.Today;
            DateTime target;
            for (int rowIndex = 0; rowIndex < list.Rows.Count; rowIndex++)
            {
                DateTime.TryParse(list.Rows[rowIndex].Cells[2].Value.ToString(), out target);

                if (target.CompareTo(today) == -1)
                {
                    list.Rows[rowIndex].DefaultCellStyle.BackColor = Color.Pink;
                }
            }
        }

        protected override void ArticleInquiryList_Sorted(object sender, EventArgs e)
        {
            if (list.Rows.Count != 0)
            {
                SetRowColor();
            }
        }

    }
}
