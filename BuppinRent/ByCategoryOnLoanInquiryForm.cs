﻿using BuppinRent.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BuppinRent.Database;

namespace BuppinRent
{
    public partial class ByCategoryOnLoanInquiryForm : IListForm
    {
        public ByCategoryOnLoanInquiryForm()
        {
            InitializeComponent();
            OnCreate();
        }

        protected override void OnCreate()
        {
            defaultTitle = "分類別貸出中照会";
            defaultMessageHeader = "[メッセージ]  ";
            defaultMessage = "保管場所別の一覧を確認できます";
            base.OnCreate();
        }

        protected override void FoundKeyDown(object sender, KeyEventArgs e)
        {
            base.FoundKeyDown(sender, e);
        }

        protected override void BtnF10_Click(object sender, EventArgs e)
        {
            base.BtnF10_Click(sender, e);
        }

        protected override void BtnF11_Click(object sender, EventArgs e)
        {
            base.BtnF11_Click(sender, e);
        }

        protected override void FoundKeyPress(object sender, KeyPressEventArgs e)
        {
            base.FoundKeyPress(sender, e);
        }

        protected override void CheckTypeValues(TextBox box, KeyPressEventArgs e)
        {

            switch (box.Name)
            {
                case "tbBuppinGroup":
                    ValueCheck(box, e, "分類コードは０から６のみ入力可能です", "group");
                    break;
                case "oldYear":
                    ValueCheck(box, e, "日付は数値のみ入力可能です。", "year");
                    break;
                case "oldMonth":
                    ValueCheck(box, e, "日付は数値のみ入力可能です。", "month");
                    break;
                case "oldDay":
                    ValueCheck(box, e, "日付は数値のみ入力可能です。", "day");
                    break;
                case "newYear":
                    ValueCheck(box, e, "日付は数値のみ入力可能です。", "newYear");
                    break;
                case "newMonth":
                    ValueCheck(box, e, "日付は数値のみ入力可能です。", "newMonth");
                    break;
                case "newDay":
                    ValueCheck(box, e, "日付は数値のみ入力可能です。", "newDay");
                    break;
                default:
                    base.CheckTypeValues(box, e);
                    break;

                    
            }


        }

        protected void ValueCheck(TextBox box, KeyPressEventArgs e, string message, string mode)
        {
            switch (mode)
            {

                case "group":

                    if (CheckValues.IsM001Group(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;

                case "year":
                    if (CheckValues.IsNumber(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;

                case "month":
                    if (CheckValues.IsNumber(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;

                case "day":
                    if (CheckValues.IsNumber(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;

                case "newYear":
                    if (CheckValues.IsNumber(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;

                case "newMonth":
                    if (CheckValues.IsNumber(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;

                case "newDay":
                    if (CheckValues.IsNumber(e))
                    {
                        SetDefaultTheme(box);
                    }
                    else
                    {
                        SetErrorTheme(box, e, message);
                    }
                    break;
                default:
                    break;

            }
            
        }

        protected override void SetDefaultTheme(TextBox box)
        {
            box.BackColor = DefaultColor;
            message.Text = defaultMessageHeader + defaultMessage;
            FormSettings.TabStopTrigger(this, true);
        }

        protected  override void SetErrorTheme(TextBox box, KeyPressEventArgs e, String errorMessage)
        {
            e.Handled = true;
            box.BackColor = ErrorColor;
            message.Text = defaultMessageHeader + errorMessage;
            FormSettings.TabStopTrigger(this, false);
        }






        protected override void Display_Click(object sender, EventArgs e)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT `M001_BUPNGRP`, `M001_BUPNNM`, `T001_RENTDATE`, `T001_RENTCONT`, `T001_RENTUSER`, `T002_RETRNPLAN`, `T001_RENTCMNT`, `T001_RENTNO`");
            sql.Append(" FROM `by_loan` WHERE 1");

            if (CheckValues.IsEmpty(tbKind.Text))
            {
                tbKind.Text = "0";
            }

            if (!CheckValues.IsM001Kind(tbKind.Text) && tbKind.Text != "0")
            {
                MessageBox.Show("種別は０から２のみ入力可能です");
                return;
            }

            DateTime? oldDateTime = CheckValues.IsDateTime("20" +oldYear.Text, oldMonth.Text, oldDay.Text);
            DateTime dtOldDate;
            if (oldDateTime!= null)
            {
                 dtOldDate = (DateTime)oldDateTime;

            }
            else
            {
                MessageBox.Show(
                   "左側の日付に空白が入っています", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                message.Text = defaultMessageHeader + "日付を変更してください。";
                return;
            }


            DateTime? newDateTime = CheckValues.IsDateTime("20" + newYear.Text, newMonth.Text, newDay.Text);
            DateTime dtNewDate;
            if (newDateTime != null)
            {
                dtNewDate = (DateTime)newDateTime;

            }
            else
            {
                MessageBox.Show(
                   "右側の日付に空白が入っています。", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                message.Text = defaultMessageHeader + "日付を変更してください。";
                return ;
            }

            if (oldDateTime > newDateTime)
            {
                MessageBox.Show(
                    "左側の日付には右側より過去の日付を入力してください。", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                message.Text = defaultMessageHeader + "日付を変更してください。";
                return;
            }

            if (CheckValues.IsEmpty(tbBuppinGroup.Text))
            {
                tbBuppinGroup.Text = "0";
            }
           
            string stAricleType = tbKind.Text;
            if (stAricleType == "1")
            {
                sql.Append(" and T001_BUPNKND = 1");
            }
            else if(stAricleType is "2")
            {
                sql.Append(" and T001_BUPNKND = 2");
            }

            sql.Append(" and T001_RENTDATE BETWEEN " + dtOldDate.Date.ToString("yyyyMMdd")+ " and " + dtNewDate.Date.ToString("yyyyMMdd"));
            string stBuppinGroup = tbBuppinGroup.Text;

            if (stBuppinGroup.Equals("1") || stBuppinGroup.Equals("2") || stBuppinGroup.Equals("3") ||
                     stBuppinGroup.Equals("4") || stBuppinGroup.Equals("5") || stBuppinGroup.Equals("6"))
            {
                sql.Append(" and M001_BUPNGRP =" + stBuppinGroup);
            }



            sql.Append(";");
            string table = "by_loan";

            DatabaseConnector conn = new DatabaseConnector();
            dataSet = conn.Result(table, sql.ToString(), table);

            keywords[0] = tbKind.Text;
            keywords[1] = dtOldDate.Date.ToString("yyyy/MM/dd");
            keywords[2] = dtNewDate.Date.ToString("yyyy/MM/dd");
            keywords[3] = tbBuppinGroup.Text;

            /*
            //ヘッダーのカラムの幅
            list.Columns[0].Width = 197;
            list.Columns[0].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            list.Columns[1].Width = 30;
            list.Columns[2].Width = 60;
            list.Columns[3].Width = 150;
            list.Columns[4].Width = 55;
            list.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            list.Columns[5].Width = 400;
            list.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            */



            //Add By HAL OSAKA Katsuya Akamatsu on 2017/10/19 THR

            if (dataSet.Tables[table].Rows.Count != 0)
            {
                SetCellValues(table);
                
            }

            DataTable tb = dataSet.Tables[table].Copy();
            tb.Columns.Remove("T001_RENTCMNT");
            list.DataSource = tb;
            if (list.Rows.Count != 0)
            {
                list.Rows[0].Selected = true;
            }
            SetColumnProperty();
            //Add By HAL OSAKA Katsuya Akamatsu on 2017/10/6 FRI


            headerText = new string[] { "類", "物品名", "貸出日", "借受者名", "利用者名", "返却予定日", "貸出No" };

            for (int count = 0; count < headerText.Length; count++)
            {
                list.Columns[count].HeaderText = headerText[count];
            }

        }

        //Add By HAL OSAKA Katsuya Akamatsu on 2017/10/19 THR

        protected override void SetColumnProperty()
        {
            double width = list.Size.Width;
            int small = 50;
            int midle = 80;
            int big = 160;

            var center = DataGridViewContentAlignment.MiddleCenter; //中央揃え
            var right = DataGridViewContentAlignment.MiddleRight; //右揃え

            //RowHeder
            list.RowHeadersWidth = small;

            //Column0 As Group
            list.Columns[0].Width = midle;
            list.Columns[0].HeaderCell.Style.Alignment = center;

            //Column1 As Name
            list.Columns[1].Width = (int)width - (small * 1 + midle * 4 + big * 2) - 2;
            list.Columns[1].HeaderCell.Style.Alignment = center;

            //Column2 As RentDate
            list.Columns[2].Width = midle;
            list.Columns[2].HeaderCell.Style.Alignment = center;
            list.Columns[2].DefaultCellStyle.Alignment = right;

            //Column3 As Cont
            list.Columns[3].Width = big;
            list.Columns[3].HeaderCell.Style.Alignment = center;

            //Column4 As User
            list.Columns[4].Width = big;
            list.Columns[4].HeaderCell.Style.Alignment = center;

            //Column4 As ReturnPlan
            list.Columns[5].Width = midle;
            list.Columns[5].HeaderCell.Style.Alignment = center;

            //Column5 As RentNo
            list.Columns[6].Width = midle;
            list.Columns[6].HeaderCell.Style.Alignment = center;
            list.Columns[6].DefaultCellStyle.Alignment = right;
        }

        /*
         * SQLだとREPLACEが複雑になるのでC#でレコードの内容を変更します
         */
        protected override void SetCellValues(string table)
        {
            string cell; //セルの内容を格納する

            //DataGridViewの内容を全件調べる
            for (int rowIndex = 0; rowIndex < dataSet.Tables[table].Rows.Count; rowIndex++)
            {
                //２列目、分類コードを可視性向上のため文字に置き換え開始

                cell = dataSet.Tables[table].Rows[rowIndex][0].ToString(); //二列目 rowindex行目の文字を格納

                switch (cell)
                {
                    case "1":
                        cell = "本体";
                        break;
                    case "2":
                        cell = "モニター";
                        break;
                    case "3":
                        cell = "プリンタ";
                        break;
                    case "4":
                        cell = "モデム";
                        break;
                    case "5":
                        cell = "ルータ";
                        break;
                    case "6":
                        cell = "ハブ";
                        break;
                    case "0":
                        cell = "書籍";
                        break;
                }

                dataSet.Tables[table].Rows[rowIndex][0] = cell; //変換した文字をセルに設定する、置き換え終了

            }
        }

    }
}
