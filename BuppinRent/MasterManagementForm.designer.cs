﻿namespace BuppinRent
{
    partial class MasterManagementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbBpnCode = new System.Windows.Forms.Label();
            this.tbBuppinCode = new System.Windows.Forms.TextBox();
            this.tbBuppinName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbBuppinKind = new System.Windows.Forms.TextBox();
            this.tbBuppinGroup = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbBuppinLocation1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbBuppinRentFlag = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbDeleteFlag = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tbCondition = new System.Windows.Forms.TextBox();
            this.tbDeleteReason = new System.Windows.Forms.TextBox();
            this.tbLcat2 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.display = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // f1
            // 
            this.f1.Click += new System.EventHandler(this.BtnF1_Click);
            // 
            // f2
            // 
            this.f2.Click += new System.EventHandler(this.BtnF2_Click);
            // 
            // f8
            // 
            this.f8.TabIndex = 11;
            this.f8.Click += new System.EventHandler(this.f8_Click);
            // 
            // f10
            // 
            this.f10.TabIndex = 12;
            this.f10.Click += new System.EventHandler(this.BtnF10_Click);
            // 
            // f11
            // 
            this.f11.TabIndex = 13;
            // 
            // f12
            // 
            this.f12.Click += new System.EventHandler(this.BtnF12_Click);
            // 
            // lbBpnCode
            // 
            this.lbBpnCode.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbBpnCode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.lbBpnCode.Location = new System.Drawing.Point(238, 110);
            this.lbBpnCode.Name = "lbBpnCode";
            this.lbBpnCode.Size = new System.Drawing.Size(107, 84);
            this.lbBpnCode.TabIndex = 6;
            this.lbBpnCode.Text = "物品コード";
            this.lbBpnCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbBuppinCode
            // 
            this.tbBuppinCode.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tbBuppinCode.Location = new System.Drawing.Point(351, 135);
            this.tbBuppinCode.MaxLength = 5;
            this.tbBuppinCode.Name = "tbBuppinCode";
            this.tbBuppinCode.Size = new System.Drawing.Size(100, 36);
            this.tbBuppinCode.TabIndex = 1;
            this.tbBuppinCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbBuppinCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // tbBuppinName
            // 
            this.tbBuppinName.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tbBuppinName.Location = new System.Drawing.Point(351, 210);
            this.tbBuppinName.MaxLength = 20;
            this.tbBuppinName.Name = "tbBuppinName";
            this.tbBuppinName.Size = new System.Drawing.Size(396, 36);
            this.tbBuppinName.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label2.Location = new System.Drawing.Point(257, 199);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 56);
            this.label2.TabIndex = 10;
            this.label2.Text = "物品名称";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label1.Location = new System.Drawing.Point(257, 255);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 56);
            this.label1.TabIndex = 11;
            this.label1.Text = "物品種別";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbBuppinKind
            // 
            this.tbBuppinKind.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tbBuppinKind.Location = new System.Drawing.Point(351, 266);
            this.tbBuppinKind.MaxLength = 1;
            this.tbBuppinKind.Name = "tbBuppinKind";
            this.tbBuppinKind.Size = new System.Drawing.Size(48, 36);
            this.tbBuppinKind.TabIndex = 3;
            this.tbBuppinKind.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbBuppinKind.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // tbBuppinGroup
            // 
            this.tbBuppinGroup.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tbBuppinGroup.Location = new System.Drawing.Point(351, 322);
            this.tbBuppinGroup.MaxLength = 1;
            this.tbBuppinGroup.Name = "tbBuppinGroup";
            this.tbBuppinGroup.Size = new System.Drawing.Size(48, 36);
            this.tbBuppinGroup.TabIndex = 4;
            this.tbBuppinGroup.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbBuppinGroup.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label3.Location = new System.Drawing.Point(234, 311);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 56);
            this.label3.TabIndex = 13;
            this.label3.Text = "分類コード";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label4.Location = new System.Drawing.Point(218, 367);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(127, 56);
            this.label4.TabIndex = 15;
            this.label4.Text = "基本保管場所";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbBuppinLocation1
            // 
            this.tbBuppinLocation1.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tbBuppinLocation1.Location = new System.Drawing.Point(351, 378);
            this.tbBuppinLocation1.MaxLength = 20;
            this.tbBuppinLocation1.Name = "tbBuppinLocation1";
            this.tbBuppinLocation1.Size = new System.Drawing.Size(220, 36);
            this.tbBuppinLocation1.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label5.Location = new System.Drawing.Point(577, 367);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(127, 56);
            this.label5.TabIndex = 17;
            this.label5.Text = "現在保管場所";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbBuppinRentFlag
            // 
            this.tbBuppinRentFlag.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tbBuppinRentFlag.Location = new System.Drawing.Point(351, 434);
            this.tbBuppinRentFlag.MaxLength = 1;
            this.tbBuppinRentFlag.Name = "tbBuppinRentFlag";
            this.tbBuppinRentFlag.Size = new System.Drawing.Size(48, 36);
            this.tbBuppinRentFlag.TabIndex = 7;
            this.tbBuppinRentFlag.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbBuppinRentFlag.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label6.Location = new System.Drawing.Point(216, 423);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(129, 56);
            this.label6.TabIndex = 19;
            this.label6.Text = "貸出フラグ";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbDeleteFlag
            // 
            this.tbDeleteFlag.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tbDeleteFlag.Location = new System.Drawing.Point(710, 434);
            this.tbDeleteFlag.MaxLength = 1;
            this.tbDeleteFlag.Name = "tbDeleteFlag";
            this.tbDeleteFlag.Size = new System.Drawing.Size(48, 36);
            this.tbDeleteFlag.TabIndex = 8;
            this.tbDeleteFlag.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbDeleteFlag.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label7.Location = new System.Drawing.Point(582, 423);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 56);
            this.label7.TabIndex = 21;
            this.label7.Text = "削除フラグ";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label8.Location = new System.Drawing.Point(193, 479);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(152, 56);
            this.label8.TabIndex = 23;
            this.label8.Text = "状態（付属品）";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label9.Location = new System.Drawing.Point(552, 479);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(152, 56);
            this.label9.TabIndex = 24;
            this.label9.Text = "削除理由";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbCondition
            // 
            this.tbCondition.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tbCondition.Location = new System.Drawing.Point(351, 490);
            this.tbCondition.MaxLength = 20;
            this.tbCondition.Multiline = true;
            this.tbCondition.Name = "tbCondition";
            this.tbCondition.Size = new System.Drawing.Size(220, 223);
            this.tbCondition.TabIndex = 9;
            // 
            // tbDeleteReason
            // 
            this.tbDeleteReason.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tbDeleteReason.Location = new System.Drawing.Point(710, 490);
            this.tbDeleteReason.MaxLength = 20;
            this.tbDeleteReason.Multiline = true;
            this.tbDeleteReason.Name = "tbDeleteReason";
            this.tbDeleteReason.Size = new System.Drawing.Size(220, 223);
            this.tbDeleteReason.TabIndex = 10;
            // 
            // tbLcat2
            // 
            this.tbLcat2.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tbLcat2.Location = new System.Drawing.Point(710, 378);
            this.tbLcat2.MaxLength = 20;
            this.tbLcat2.Name = "tbLcat2";
            this.tbLcat2.Size = new System.Drawing.Size(220, 36);
            this.tbLcat2.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.label10.Location = new System.Drawing.Point(413, 269);
            this.label10.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(134, 28);
            this.label10.TabIndex = 28;
            this.label10.Text = "1:書籍 2:機器";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.label12.Location = new System.Drawing.Point(413, 437);
            this.label12.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(172, 28);
            this.label12.TabIndex = 30;
            this.label12.Text = "0:未貸出 9:貸出中";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.label13.Location = new System.Drawing.Point(767, 437);
            this.label13.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(115, 28);
            this.label13.TabIndex = 31;
            this.label13.Text = "0:未 9:削除";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.label11.Location = new System.Drawing.Point(413, 325);
            this.label11.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(586, 28);
            this.label11.TabIndex = 29;
            this.label11.Text = "0:書籍 1:本体 2:モニタ 3:プリンター 4:モデム 5:ルータ 6:ハブ   ";
            // 
            // display
            // 
            this.display.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.display.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.display.Font = new System.Drawing.Font("メイリオ", 14.25F, System.Drawing.FontStyle.Bold);
            this.display.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.display.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.display.Location = new System.Drawing.Point(456, 131);
            this.display.Name = "display";
            this.display.Size = new System.Drawing.Size(124, 40);
            this.display.TabIndex = 34;
            this.display.Text = "表示";
            this.display.UseVisualStyleBackColor = false;
            this.display.Click += new System.EventHandler(this.Display_Click);
            // 
            // MasterManagementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1280, 1024);
            this.Controls.Add(this.display);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tbLcat2);
            this.Controls.Add(this.tbDeleteReason);
            this.Controls.Add(this.tbCondition);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbDeleteFlag);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbBuppinRentFlag);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbBuppinLocation1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbBuppinGroup);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbBuppinKind);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbBuppinName);
            this.Controls.Add(this.tbBuppinCode);
            this.Controls.Add(this.lbBpnCode);
            this.Name = "MasterManagementForm";
            this.Text = "MasterRegisterForm";
            this.Controls.SetChildIndex(this.lbBpnCode, 0);
            this.Controls.SetChildIndex(this.tbBuppinCode, 0);
            this.Controls.SetChildIndex(this.tbBuppinName, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.tbBuppinKind, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.tbBuppinGroup, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.tbBuppinLocation1, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.tbBuppinRentFlag, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.tbDeleteFlag, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.label9, 0);
            this.Controls.SetChildIndex(this.tbCondition, 0);
            this.Controls.SetChildIndex(this.tbDeleteReason, 0);
            this.Controls.SetChildIndex(this.tbLcat2, 0);
            this.Controls.SetChildIndex(this.label10, 0);
            this.Controls.SetChildIndex(this.label11, 0);
            this.Controls.SetChildIndex(this.label12, 0);
            this.Controls.SetChildIndex(this.label13, 0);
            this.Controls.SetChildIndex(this.f11, 0);
            this.Controls.SetChildIndex(this.f1, 0);
            this.Controls.SetChildIndex(this.f2, 0);
            this.Controls.SetChildIndex(this.f3, 0);
            this.Controls.SetChildIndex(this.f4, 0);
            this.Controls.SetChildIndex(this.f5, 0);
            this.Controls.SetChildIndex(this.f6, 0);
            this.Controls.SetChildIndex(this.f7, 0);
            this.Controls.SetChildIndex(this.f8, 0);
            this.Controls.SetChildIndex(this.f9, 0);
            this.Controls.SetChildIndex(this.f10, 0);
            this.Controls.SetChildIndex(this.f12, 0);
            this.Controls.SetChildIndex(this.title, 0);
            this.Controls.SetChildIndex(this.message, 0);
            this.Controls.SetChildIndex(this.display, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbBpnCode;
        private System.Windows.Forms.TextBox tbBuppinCode;
        private System.Windows.Forms.TextBox tbBuppinName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbBuppinKind;
        private System.Windows.Forms.TextBox tbBuppinGroup;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbBuppinLocation1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbBuppinRentFlag;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbDeleteFlag;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbCondition;
        private System.Windows.Forms.TextBox tbDeleteReason;
        private System.Windows.Forms.TextBox tbLcat2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button display;
    }
}