﻿using BuppinRent.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuppinRent

/*
*  Created By HAL OSAKA Katsuya Akamatsu on 2017/10/3 TUE
*  アプリケーションを起動したときに最初に表示されるメニューフォーム
*/

{
    public partial class WelcomeForm : IFormDesign
    {

        public WelcomeForm()
        {
            InitializeComponent();

            btnRent.BackColor = SystemColors.Control;
            btnReceive.BackColor = SystemColors.Control;
            btnCat.BackColor = SystemColors.Control;
            btnMaster.BackColor = SystemColors.Control;
            btnArticle.BackColor = SystemColors.Control;
            btnNon.BackColor = SystemColors.Control;

        
        }

        protected override void BtnF1_Click(object sender, EventArgs e)
        {
            using (RentRegisterForm form = new RentRegisterForm())
            {
                form.ShowDialog();
            }
        }

        protected override void BtnF2_Click(object sender, EventArgs e)
        {
            using (RentReceiveForm form = new RentReceiveForm())
            {
                form.ShowDialog();
            }
        }

        protected override void BtnF3_Click(object sender, EventArgs e)
        {
            using (ByCategoryOnLoanInquiryForm form = new ByCategoryOnLoanInquiryForm())
            {
                form.ShowDialog();
            }
        }

        protected override void BtnF4_Click(object sender, EventArgs e)
        {
            using (MasterManagementForm form = new MasterManagementForm())
            {
                form.ShowDialog();
            }
        }

        protected override void BtnF5_Click(object sender, EventArgs e)
        {
            using (ArticleInquiryForm form = new ArticleInquiryForm())
            {
                form.ShowDialog();
            }
        }

        protected override void BtnF6_Click(object sender, EventArgs e)
        {
            using (NonReceivedListForm form = new NonReceivedListForm())
            {
                form.ShowDialog();
            }
        }

        /*
         * Override インターフェイスの終了ボタンの処理の前にダイアログの処理を追加する
         */
        protected override void BtnF11_Click(object sender, EventArgs e)
        {
            string message = "終了しますか？";
            string title = "終了";
            DialogResult result = MessageBox.Show(
                message, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if(result == DialogResult.Yes)
            {
                base.BtnF11_Click(null, null);
            }
        }

        /*
         * キー入力された時の処理をカスタマイズする
         */
        protected override void FoundKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    BtnF1_Click(null, null);
                    break;
                case Keys.F2:
                    BtnF2_Click(null, null);
                    break;
                case Keys.F3:
                    BtnF3_Click(null, null);
                    break;
                case Keys.F4:
                    BtnF4_Click(null, null);
                    break;
                case Keys.F5:
                    BtnF5_Click(null, null);
                    break;
                case Keys.F6:
                    BtnF6_Click(null, null);
                    break;
                case Keys.F11:
                    BtnF11_Click(null, null);
                    break;
            }
        }

    }
}
